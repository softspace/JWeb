/**
 * 
 */
package com.hm.common.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import com.hm.common.enums.Authority;

/**
 * @author hwg
 *
 *         2021年3月2日
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface Function {
	String module();
	String code();
	String description();
	Authority authority() default Authority.REQUIRE_AUTH;
}
