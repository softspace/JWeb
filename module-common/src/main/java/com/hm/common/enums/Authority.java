/**
 * 
 */
package com.hm.common.enums;

/**
 * @author hwg
 *
 *         2021年3月3日
 */
public enum Authority {
	REQUIRE_LOGIN, //需要登录
	UNREQUIRE_LOGIN, //不需要登录
	REQUIRE_AUTH;
}
