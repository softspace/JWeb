package com.hm.common;

import com.hm.common.utils.DateUtils;
import com.hm.common.utils.RSAEncryptUtils;

public class CreateAuthCode {

	public static void main(String[] args) throws Exception {
		String expirationDate = "2021-03-07 00:00:00";

		try {
			DateUtils.parse(expirationDate);
		} catch (Exception e) {
			System.out.println("日期格式不正确，格式为：yyyy-MM-dd HH:mm:ss， 如：" + DateUtils.fnow());
			return;
		}

		//KeyPair keyPair = RSAEncrypt.genKeyPair();
		
		String publicKey = "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCfMBYZmVJmTtr1fbUObEv1auALiL81+NSxNSO3S4GC4dh3rmU13JlD5L7AXPW0xYH1ornPUnaQoekTgi9bRyccQzHYpb32LDX25OHgrQ/hFb1xoDNF0zwJwMsc6vqSpJJ2POrgaEIjD3eldCUO5fSS6ft7H0/9UEq10U9wdBcL/wIDAQAB";
		String authCode = RSAEncryptUtils.encrypt(expirationDate, publicKey);
		System.out.println("到期时间:" + RSAEncryptUtils.decrypt(authCode, Constant.AUTH_CODE_PRIVATE_KEY));
		System.out.println("创建成功, Auth Code：" + authCode);
	}
}
