/**
 * 
 */
package com.hm.common.exception;

/**
 * @author hwg
 *
 * 2021年3月3日
 */
public class NoAuthException extends Exception{
	private static final long serialVersionUID = 1L;

	/**
	 * @param message
	 */
	public NoAuthException(String message) {
		super(message);
	}
}
