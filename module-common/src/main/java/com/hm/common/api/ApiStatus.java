package com.hm.common.api;

/**
 * 
 * @author hwg
 *
 *         2020年12月18日
 */
public enum ApiStatus {
	/** 通用部分 Code: 100 - 999 */
	STATUS_200(200, "成功"), //
	STATUS_401(401, "请求要求用户的身份认证"), //
	STATUS_403(403, "没有权限"), //
	STATUS_404(404, "你请求的资源不存在"), //
	STATUS_405(405, "客户端请求中的方法被禁止"), //
	STATUS_500(500, "服务器内部错误，无法完成请求"), //
	STATUS_503(503, "服务不可用"), //
	STATUS_600(600, "无效的token"), //
	STATUS_800(800, "缺少必要参数"), //
	STATUS_801(801, "参数格式错误"), //
	STATUS_900(900, "授权码验证失败"), //
	STATUS_999(999, "未知错误"), //
	/** 统一用户管理模块 1000 - 1999 */
	STATUS_1000(1000, "账号或密码错误"), //
	STATUS_1001(1001, "账号被禁用"), //
	STATUS_1002(1002, "用户名或密码不能为空"), //
	STATUS_1003(1003, "两次输入密码不一致"), //
	STATUS_1004(1004, "该账号已被注册"), //
	STATUS_1100(1100, "不能禁用，存在未禁用的子部门"), //
	STATUS_1101(1101, "不能删除，存在子部门"), //
	STATUS_1120(1120, "不能禁用，存在未禁用的子功能"), //
	STATUS_1121(1121, "不能删除，存在子功能"), //
	STATUS_1200(1200, "未配置默认密码"), //
	STATUS_1201(1201, "原密码验证失败");//

	private final int status;
	private final String msg;

	ApiStatus(final int status, final String msg) {
		this.status = status;
		this.msg = msg;
	}

	public static ApiStatus getApiStatus(int status) {
		ApiStatus[] ecs = ApiStatus.values();
		for (ApiStatus ec : ecs) {
			if (ec.getStatus() == status) {
				return ec;
			}
		}
		return null;
	}

	public int getStatus() {
		return status;
	}

	public String getMsg() {
		return msg;
	}
}
