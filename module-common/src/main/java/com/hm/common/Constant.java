/**
 * 
 */
package com.hm.common;

/**
 * 常量
 * 
 * @author hwg
 *
 *         2020年12月18日 常量类
 */
public class Constant {
	//授权码 私有key
	public static final String AUTH_CODE_PRIVATE_KEY = "MIICdQIBADANBgkqhkiG9w0BAQEFAASCAl8wggJbAgEAAoGBAJ8wFhmZUmZO2vV9tQ5sS/Vq4AuIvzX41LE1I7dLgYLh2HeuZTXcmUPkvsBc9bTFgfWiuc9SdpCh6ROCL1tHJxxDMdilvfYsNfbk4eCtD+EVvXGgM0XTPAnAyxzq+pKkknY86uBoQiMPd6V0JQ7l9JLp+3sfT/1QSrXRT3B0Fwv/AgMBAAECgYBlX5MDVE9UFa18nkQZ3j91F018KuZRsZeJDiUjifL87WQTuzx603PK97W9/LUAHLMJQUqAy6vzqRgrs0EDKMmvRQnZB3OjUB9B06R2TVb0iW05li97CBIC931puMknPNtGkM8P648WsxaIfSXufudia+ll2MYP6vaRIPXCLYzs8QJBANmOEhENpowDJSoCkxP750u/0WCEasyLGd9iNRKojkUHFEDhZXe7xnHBD9SwLFpukAEMgiQZPuxlpj9wmtnYGBcCQQC7UY5TGbyMMTZlCcylNZpORtjDrcsGg1S5vUCNhhzMazOWPY2LzjUAgtwK3x7vRjwbaSmDEaQyuN1+xrPevjRZAkA907tcr1Vymv431y13U3KjO2tEk71KTgnXSzCsEc0E3wMpgST2Bx5Ze7WFC2A/fwZb1fMyUR5pzmWVx+mqGViXAkAjfwHsZ20eV9kJ7oB0MiMqgCxlcFKPymVMyTspVmewqQPejY18F7hdf265NKzFBNcxDo2KQ9aMshFvC7JcHEkJAkA9FHybI6nu+eOzyaGHMxG5VbixizefDXQKPZFHqSgFcry6wsHQvbNF4MY1bdrybXCLfD/JOHNceSEvXcpGUXeZ";

	//请求消息头Token参数名
	public static final String token = "Hm-Token";
	//请求消息头请求标识参数名
	public static final String REQUEST_ID = "Hm-Request-Id";
	//请求消息头用户标识参数名
	public static final String USER_ID = "Hm-User-Id";

	public static final String CONTENT_TYPE = "Content-Type";

	public static final String CONTENT_TYPE_HTML = "text/html;charset=UTF-8";

	public static final String CONTENT_TYPE_JSON = "application/json;charset=UTF-8";
	//响应头功能标识参数名
	public static final String REQUEST_FUNCTION_CODE = "Hm-Request-Function-Code";
	//响应头功能说明参数名
	public static final String REQUEST_FUNCTION_DESC = "Hm-Request-Function-Desc";
	//响应头功能所属模块参数名
	public static final String REQUEST_FUNCTION_MODULE = "Hm-Request-Function-Module";
}
