/**
 * 
 */
package com.hm.common.utils;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author hwg
 *
 *         2020年12月18日
 */
public class StringUtils {

	public static String toString(Object o) {
		return null == o ? "" : o.toString();
	}

	public static boolean isNull(Object o) {
		return null == o;
	}

	public static boolean isNotNull(Object o) {
		return !isNull(o);
	}

	public static boolean isEmpty(Object o) {
		return isNull(o) || o.toString().length() == 0;
	}

	public static boolean isNotEmpty(Object o) {
		return !isEmpty(o);
	}

	public static boolean isEmpty(Map m) {
		return isNull(m) || m.size() == 0;
	}

	public static boolean isNotEmpty(Map m) {
		return !isEmpty(m);
	}

	public static boolean isEmpty(List l) {
		return isNull(l) || l.size() == 0;
	}

	public static boolean isNotEmpty(List l) {
		return !isEmpty(l);
	}

	public static boolean isEmpty(Set s) {
		return isNull(s) || s.size() == 0;
	}

	public static boolean isNotEmpty(Set s) {
		return !isEmpty(s);
	}

	public static boolean isEmpty(String s) {
		return isNull(s) || s.length() == 0;
	}

	public static boolean isNotEmpty(String s) {
		return !isEmpty(s);
	}

	public static Object ifEmptyReturn(Object o, Object v) {
		return isEmpty(o) ? v : o;
	}

	public static int toInt(String v, int defaultValue) {
		if (StringUtils.isEmpty(v)) {
			return defaultValue;
		}
		return Integer.parseInt(v);
	}

	public static String encodeUrl(String s) throws UnsupportedEncodingException {
		return encodeUrl(s, "UTF-8");
	}

	public static String decodeUrl(String s) throws UnsupportedEncodingException {
		return decodeUrl(s, "UTF-8");
	}

	public static String encodeUrl(String s, String enc) throws UnsupportedEncodingException {
		if (StringUtils.isEmpty(s)) {
			return s;
		}
		return URLEncoder.encode(s, enc);
	}

	public static String decodeUrl(String s, String enc) throws UnsupportedEncodingException {
		if (StringUtils.isEmpty(s)) {
			return s;
		}
		return URLDecoder.decode(s, enc);
	}

}
