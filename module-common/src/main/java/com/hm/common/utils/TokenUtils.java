/**
 * 
 */
package com.hm.common.utils;

import java.util.Random;

/**
 * @author hwg
 *
 *         2020年12月28日 生成token工具类，
 */
public class TokenUtils {
	/**
	 * 根据当前时间和随机数生成token
	 * 
	 * @return
	 */
	public static String generateToken() {
		StringBuilder sb = new StringBuilder();
		sb.append(System.currentTimeMillis());
		sb.append(new Random().nextInt(999999999));
		return MD5Utils.sign(sb.toString());
	}
}
