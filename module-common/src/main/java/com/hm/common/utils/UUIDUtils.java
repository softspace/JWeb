/**
 * 
 */
package com.hm.common.utils;

import java.util.UUID;

/**
 * @author hwg
 *
 *         2020年12月28日
 */
public class UUIDUtils {
	public static String uuid() {
		return UUID.randomUUID().toString().replaceAll("-", "");
	}
	
}
