package com.hm.common.utils;

import org.springframework.util.DigestUtils;

/**
 * 
 * @author admin
 * 
 */
public class MD5Utils {

	/**
	 * 
	 * @param s
	 * @return
	 */
	public static String encode(String s) {
		return DigestUtils.md5DigestAsHex(s.getBytes());
	}

	/**
	 * 
	 * @param s
	 * @return
	 */
	public static String sign(String s) {
		return encode(s);
	}

	/**
	 * 
	 * @param s
	 * @param sign
	 * @return
	 */
	public static boolean verity(String s, String sign) {
		String sign_ = encode(s);
		return sign.equals(sign_);
	}

	public static void main(String[] args) {
		//e10adc3949ba59abbe56e057f20f883e
		//e10adc3949ba59abbe56e057f20f883e
		System.out.println(MD5Utils.encode("123456"));
	}
}
