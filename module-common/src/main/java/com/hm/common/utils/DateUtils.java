package com.hm.common.utils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * 日期根据类
 * 
 * @author hwg
 *
 *         2020年12月18日
 */
public class DateUtils {
	public static final String YYYY_MM_DD_HH_MM_SS = "yyyy-MM-dd HH:mm:ss";
	private static final SimpleDateFormat SIMPLE_DATE_FORAMT = new SimpleDateFormat(YYYY_MM_DD_HH_MM_SS);

	/**
	 * 当前时间
	 * 
	 * @return
	 */
	public static Date now() {
		return new Date();
	}

	/**
	 * 格式化的当前时间
	 * 
	 * @return
	 */
	public static String fnow() {
		return SIMPLE_DATE_FORAMT.format(now());
	}

	public static Date parse(String date) throws Exception {
		return parse(date, SIMPLE_DATE_FORAMT);
	}

	public static Date parse(String date, String format) throws Exception {
		return parse(date, new SimpleDateFormat(format));
	}

	public static Date parse(String date, DateFormat sdf) throws Exception {
		return sdf.parse(date);
	}

	public static String parse(long millSec) throws Exception {
		return format(new Date(millSec), SIMPLE_DATE_FORAMT);
	}
	
	public static String parse(long millSec, String format) throws Exception {
		return format(new Date(millSec), format);
	}

	public static String format(Date date) throws Exception {
		return format(date, SIMPLE_DATE_FORAMT);
	}

	public static String format(Date date, String format) throws Exception {
		return format(date, new SimpleDateFormat(format));
	}

	public static String format(Date date, DateFormat sdf) throws Exception {
		return sdf.format(date);
	}

	public static Date addHours(Date date, int hours) {
		Calendar cal = Calendar.getInstance();
		cal.setTime((Date) date.clone());
		cal.add(Calendar.HOUR_OF_DAY, hours);
		return cal.getTime();
	}

	public static Date addMinutes(Date date, int minutes) {
		Calendar cal = Calendar.getInstance();
		cal.setTime((Date) date.clone());
		cal.add(Calendar.MINUTE, minutes);
		return cal.getTime();
	}

	public static Date addDay(Date date, int days) {
		Calendar cal = Calendar.getInstance();
		cal.setTime((Date) date.clone());
		cal.add(Calendar.DAY_OF_MONTH, days);
		return cal.getTime();
	}

	public static void main(String[] args) throws Exception {
		System.out.println(DateUtils.format(DateUtils.addMinutes(DateUtils.now(), 30)));
	}
}
