/**
 * 
 */
package com.hm.common.utils;

import java.util.regex.Pattern;

/**
 * @author hwg
 *
 *         2020年12月30日
 */
public class NumberUtils {
	public static boolean isInteger(String str) {
		if(StringUtils.isEmpty(str)) {
			return false;
		}
		Pattern pattern = Pattern.compile("^[-\\+]?[\\d]*$");
		return pattern.matcher(str).matches();
	}

	public static boolean isNotInteger(String str) {
		return !isInteger(str);
	}

}
