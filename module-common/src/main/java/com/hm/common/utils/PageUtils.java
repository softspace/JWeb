/**
 * 
 */
package com.hm.common.utils;

/**
 * @author hwg
 *
 *         2020年12月30日
 *         分页工具类
 */
public class PageUtils {
	public static final int MAX_PAGE_SIZE = 2000;//每页最大记录数，不能超过这个数，超过将赋值为次数
	public static final int DEFAULT_PAGE_SIZE = 100;//每页默认记录数

	/**
	 * 校验每页记录数是否合法，合法转换为int，不合法则使用每页默认记录数
	 * @param pSize
	 * @return
	 */
	public static int getPageSize(String pSize) {
		return NumberUtils.isInteger(pSize) ? Integer.parseInt(pSize) : PageUtils.DEFAULT_PAGE_SIZE;
	}

	/**
	 * 计算分页（limit offset， pageSize）中的offset
	 * @param cPage
	 * @param pSize
	 * @return
	 */
	public static int getOffset(String cPage, String pSize) {
		int currentPage = NumberUtils.isInteger(cPage) ? Integer.parseInt(cPage) : 1;
		int pageSize = NumberUtils.isInteger(pSize) ? Integer.parseInt(pSize) : PageUtils.DEFAULT_PAGE_SIZE;
		return getOffset(currentPage, pageSize);
	}

	/**
	 * 计算分页（limit offset， pageSize）中的offset
	 * @param cPage
	 * @param pSize
	 * @return
	 */
	public static int getOffset(int cPage, int pSize) {
		if (pSize > MAX_PAGE_SIZE) {
			pSize = MAX_PAGE_SIZE;
		}
		if (cPage < 0) {
			cPage = 1;
		}
		return (cPage - 1) * pSize;
	}

}
