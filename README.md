# JWeb

#### 介绍
JWeb后台管理系统，基于Springboot、springCloud（Gateway）、Consul、Mybatis开发，采用分模块的方式便于开发和维护，支持前后台模块分别部署，目前支持的功能有：用户管理、功能菜单管理、角色管理、权限管理、部门管理、字典管理、参数配置、日志和在线用户等，界面简洁美观大方，使用舒适，易于扩展改造，适合新项目的快速搭建开发。

#### 软件架构
![输入图片说明](https://images.gitee.com/uploads/images/2021/0323/101927_bb6586b6_8850763.png "QQ图片20210323101807.png")


#### 在线演示
在线演示地址： [http://139.227.254.194:18081/](http://139.227.254.194:18081/) 用户名：admin 密码：123456
(注意：请使用较新版本浏览器（支持localstorage，css3等），推荐使用google浏览器。)

#### 部分页面截图
1.  登录页面
![输入图片说明](https://images.gitee.com/uploads/images/2021/0323/111700_e1238724_8850763.png "登录页面.png")
2.  首页
![输入图片说明](https://images.gitee.com/uploads/images/2021/0323/112132_03216b7a_8850763.png "首页.png")
3.  统一用户管理模块
![输入图片说明](https://images.gitee.com/uploads/images/2021/0323/112549_d4fc0b94_8850763.png "统一用户管理模块.png")
4.  访问日志
![输入图片说明](https://images.gitee.com/uploads/images/2021/0323/112957_20dec489_8850763.png "访问日志.png")
4.  在线文档（Swagger3）
![输入图片说明](https://images.gitee.com/uploads/images/2021/0323/130440_377eb331_8850763.png "在线文档.png")
#### 项目模块介绍
1.  module-root。此模块配置基础公共依赖，以及spring等相关Maven仓库信息，此模块本身不包含代码。
2.  module-common。主要提供一些通用的工具类。
3.  module-api-gateway。网关模块，基于springCloud中的Gateway实现，主要完成路由、统一鉴权和日志记录。
4.  module-uums。统一用户管理模块实现用户、角色、部门、权限、参数配置、字典等功能。
- 另外注册中心使用Consul实现，module-uums模块注册到注册中心，module-api-gateway模块从注册中心发现服务。

#### 技术栈（后台）
1. Springboot（版本：2.4.1）
2. springCloud（版本：2020.0.0-RC1）
3. mybatis（版本：2.1.4）
4. Consul（版本：1.9.4）
5. Mysql

#### 技术栈（前端）
1. jquery（版本：3.5.1）
2. layui （版本：2.5.7，使用其中的按钮、表单、弹出等，没有使用表格）
3. easyui（版本：1.9.11，使用其中的datagrid和treegrid）
4. font-awesome（版本：4.7.0）
5. jQuery-slimScroll（版本：1.3.8）
另外浏览器需要支持HTML5、css3和ES6

#### 开发环境搭建
本项目使用Eclipse作为开发工具基于springboot、Maven构建的微服务项目，具体环境和下载如下：
1. JDK。 安装JDK8及以上版本，JDK8下载路径：https://www.oracle.com/java/technologies/javase/javase-jdk8-downloads.html。
2. Mysql。Mysql下载路径：https://dev.mysql.com/downloads/mysql/，建议安装Mysql5.5以上。
3. IDE。本项目使用Eclipse作为开发工具，下载路径：https://www.eclipse.org/downloads/，比较新版本的Eclipse一般都自带了SVN，GIT、Maven等插件，如果没有可以自己安装。
4. Consul。Consul下载地址:https://www.consul.io/downloads，本项目使用的是1.9.4版本，下载最新版本安装即可。
5. Navicat。数据库管理工具，根据自己的喜好，可以使用其他数控管理工具，下载地址：http://www.navicat.com.cn/products，需要破解哦，可以根据网上的破解流程破解。
6. 创建数据库和表结构，初始化数据。创建一个数据库，本项目中数据库名为：module-uums，创建好数据库后，下载sql.sql脚本，执行脚本创建表结构和初始化数据。（注意：查看定时策略是否开启：show variables like '%event_sche%'，若未开启执行set global event_scheduler=1开启）

Eclipse安装好后，要把JDK环境配置好，相关文章网上都很多的，基本上不会有问题。都安装好后，启动Mysql和Consul服务，使用默认端口即可。
克隆项目源代码到本地解压，使用Eclipse导入module-root、module-common、module-api-gateway和module-uums四个模块到工程中，这4个模块中，module-root是基础的Maven相关依赖仓库配置模块，module-common是公共的工具类模块，只要运行module-api-gateway和module-uums模块即可。module-api-gateway模块启动类：com.hm.gateway.ApiGatewayApplication，点击运行即可；module-uums模块启动类：com.hm.uums.UUMSApplication，点击运行即可。运行后访问：http://127.0.0.1:8080/如果能正常访问，恭喜你环境搭建OK了！

#### 关于部署
Mysql和Consul和开发环境类似安装，而module-uums和module-api-gateway模块需要使用Maven编译部署到本地仓库，然后把jar拿到需要部署的服务器上运行即可。当然如果你要使用tomcat，也可以把module-uums和module-api-gateway模块打包为相应的war包，部署到tomcat中即可。

#### 关于一些设计想法
本项目总体上是比较简单的，看软件架构基本上能总体掌握，这里想聊一聊token。本来一开始想用JWT TOKEN这样的框架来处理token的，但觉得不太符合自己的需要，而且用不上很多功能，故没用。以下是此项目中token的处理：
- token的生成和保存：用户登录成功后，后台会生成一个MD5的token，token会被保存到Mysql数据库表中；
- token传输：用户登录成功后，token会被携带在响应消息头中，客户端保存token，后续请求需在请求消息头中携带token。
- token超时处理：用户每次调用微服务API都会刷新token的访问时间，在参数配置中有一个参数是设置token超时时间的，默认为30分钟，mysql中设置了一个定时任务，每隔1分钟对token进行检查，如果用户最后访问的时间到目前已经超过了设置的超时时间，则删除token，用户再次访问需要重新登录认证。

本项目实现的是基础功能，实际开发中，可能需要根据具体需求搭配不同组件，如缓存数据库（如Redis）、负载均衡、消息队列、熔断处理等，项目本身就比较简单，很方便扩展改造。本人能力有限，大家若发现项目中存在的缺陷或者错误，恳请大家批评指正，谢谢！




