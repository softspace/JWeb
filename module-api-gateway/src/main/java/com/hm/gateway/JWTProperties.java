/**
 * 
 */
package com.hm.gateway;

import java.util.Set;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

/**
 * @author hwg
 *
 *         2020年12月28日
 */
@PropertySource("classpath:application.properties")
@ConfigurationProperties(prefix = "jwt")
@Configuration
public class JWTProperties {
	private Set<String> ignoreUrls;
	private Integer expire;
	private String redirectUrl;
	public Set<String> getIgnoreUrls() {
		return ignoreUrls;
	}
	public void setIgnoreUrls(Set<String> ignoreUrls) {
		this.ignoreUrls = ignoreUrls;
	}
	public Integer getExpire() {
		return expire;
	}
	public void setExpire(Integer expire) {
		this.expire = expire;
	}
	public String getRedirectUrl() {
		return redirectUrl;
	}
	public void setRedirectUrl(String redirectUrl) {
		this.redirectUrl = redirectUrl;
	}

}
