package com.hm.gateway.mapper;

import java.util.Map;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.ResultType;
import org.apache.ibatis.annotations.Select;

/**
 * @author hwg
 *
 *         2020年12月18日
 */
public interface AuthMapper {
	@Insert("update module_uums_token set refresh_time = #{refreshTime} where token = #{token}")
	public void updateToken(String token, long refreshTime);

	@Select("select `user_id` as userId, `refresh_time` as refreshTime, `create_time` as createTime from module_uums_token where token = #{token}")
	@ResultType(value = Map.class)
	public Map<String, Object> findToken(String token);
}
