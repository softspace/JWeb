package com.hm.gateway.mapper;

import java.util.Map;

import org.apache.ibatis.annotations.Insert;

/**
 * @author hwg
 *
 *         2020年12月18日
 */
public interface LogMapper {
	@Insert("insert into module_gateway_log(`req_id`,`user_id`,`token`,`req_url`,`req_time`,`client_ip`,`client_name`,`resp_time`,`status_code`,`method`,`resp_duration`,`function_module`,`function_code`,`function_desc`) values (#{reqId},#{userId},#{token},#{reqUrl},#{reqTime},#{clientIp},#{clientName},#{respTime},#{statusCode},#{method},#{respDuration},#{functionModule},#{functionCode},#{functionDesc}) ")
	public void addLog(Map<String, Object> dMap);
}
