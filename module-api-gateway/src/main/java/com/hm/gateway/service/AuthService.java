package com.hm.gateway.service;

import java.math.BigInteger;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.hm.common.utils.DateUtils;
import com.hm.gateway.mapper.AuthMapper;

/**
 * @author hwg
 *
 *         2020年12月18日
 */
@Service
public class AuthService {

	@Autowired
	private AuthMapper authMapper;

	@Transactional
	public void refreshToken(String token) {
		authMapper.updateToken(token, DateUtils.now().getTime());
	}

	public Map<String, Object> findToken(String token) {
		return  authMapper.findToken(token);
	}
}
