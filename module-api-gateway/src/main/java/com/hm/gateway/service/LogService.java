package com.hm.gateway.service;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.hm.gateway.mapper.LogMapper;

/**
 * @author hwg
 *
 *         2020年12月18日
 */
@Service
public class LogService {

	@Autowired
	private LogMapper logMapper;

	@Transactional
	public void addLog(Map<String, Object> dMap) {
		logMapper.addLog(dMap);
	}

}
