/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 80017
 Source Host           : localhost:3306
 Source Schema         : module-uums

 Target Server Type    : MySQL
 Target Server Version : 80017
 File Encoding         : 65001

 Date: 23/03/2021 15:16:21
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;


-- ----------------------------
-- Table structure for module_gateway_log
-- ----------------------------
DROP TABLE IF EXISTS `module_gateway_log`;
CREATE TABLE `module_gateway_log`  (
  `req_id` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '请求Id',
  `user_id` int(11) NULL DEFAULT NULL COMMENT '用户Id',
  `token` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT 'JWT TOKEN',
  `req_url` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '请求URL',
  `req_time` bigint(20) NULL DEFAULT NULL COMMENT '请求时间',
  `client_ip` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '客户端IP',
  `client_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '客户端名称',
  `resp_time` bigint(20) NULL DEFAULT NULL COMMENT '响应时间',
  `status_code` int(11) NULL DEFAULT NULL COMMENT '状态码',
  `method` varchar(32) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '请求方法',
  `resp_duration` int(11) NULL DEFAULT NULL COMMENT '响应时长',
  `function_module` varchar(32) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '功能归属模块',
  `function_code` varchar(32) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '功能标识',
  `function_desc` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '功能描述',
  INDEX `index_userId`(`user_id`) USING BTREE,
  INDEX `index_reqTime`(`req_time`) USING BTREE,
  INDEX `index_respDuration`(`resp_duration`) USING BTREE,
  INDEX `index_statusCode`(`status_code`) USING BTREE,
  INDEX `index_clientIp`(`client_ip`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin COMMENT = 'jwt-token表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of module_gateway_log
-- ----------------------------

-- ----------------------------
-- Table structure for module_sys_info
-- ----------------------------
DROP TABLE IF EXISTS `module_sys_info`;
CREATE TABLE `module_sys_info`  (
  `sys_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `sys_code` int(11) NULL DEFAULT NULL COMMENT '系统编号',
  `sys_name` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '系统名称',
  `sys_version` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '系统版本号',
  `sys_publish_date` varchar(32) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '系统发布时间 格式：yyyy-MM-dd HH:mm:ss',
  `sys_manager_moblie_phone` varchar(32) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '系统管理员联系电话',
  `sys_manager_email` varchar(32) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '系统管理员电子邮箱',
  `sys_authorization_code` text CHARACTER SET utf8 COLLATE utf8_bin NULL COMMENT '授权码',
  PRIMARY KEY (`sys_id`) USING BTREE,
  INDEX `index_sys_code`(`sys_code`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_bin COMMENT = 'jwt-token表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of module_sys_info
-- ----------------------------
INSERT INTO `module_sys_info` VALUES (1, 10000, 'JWeb管理系统', 'V1.0.0', '2021-02-01 00:00:00', '13501670000', '66666666@qq.com', 'R5luTgEO0MvCL8bjEBsmgGTxMPIaCL+jTgLZFotRIQ53/0vzt4YNdc+L6y9HHikXzNZwDdhQafuGJ63TZTlPe5mMDvcf0imEvcbLAYs7UssTX3EjckUZrM2z3y6wmwtxG7H/bshI6BFLdjoqMNf5jUv4FyrUd99oMP/26eY5PbY=');

-- ----------------------------
-- Table structure for module_uums_dict
-- ----------------------------
DROP TABLE IF EXISTS `module_uums_dict`;
CREATE TABLE `module_uums_dict`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键',
  `dict_code` varchar(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '字典编号',
  `dict_name` varchar(256) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '字典名称',
  `dict_desc` varchar(256) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '描述',
  `create_time` varchar(32) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` varchar(32) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `index_parm_key`(`dict_code`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 110 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci COMMENT = '字典表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of module_uums_dict
-- ----------------------------
INSERT INTO `module_uums_dict` VALUES (107, 'uums-parm-type', '参数类型', NULL, '2021-02-08 10:43:48', '2021-03-18 17:12:20');
INSERT INTO `module_uums_dict` VALUES (109, 'test', 'test1', NULL, '2021-03-02 10:31:45', '2021-03-22 14:09:01');

-- ----------------------------
-- Table structure for module_uums_dict_item
-- ----------------------------
DROP TABLE IF EXISTS `module_uums_dict_item`;
CREATE TABLE `module_uums_dict_item`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键',
  `dict_id` varchar(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '字典编号',
  `dict_item_code` varchar(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '字典值',
  `dict_item_name` varchar(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '字典值名称',
  `dict_item_desc` varchar(256) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '描述',
  `order` int(11) NULL DEFAULT NULL COMMENT '排序',
  `create_time` varchar(32) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` varchar(32) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `index_dict_id`(`dict_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 123 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci COMMENT = '字典表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of module_uums_dict_item
-- ----------------------------
INSERT INTO `module_uums_dict_item` VALUES (110, '107', '0', '系统参数', NULL, 80, '2021-02-08 13:33:33', '2021-03-22 14:27:29');
INSERT INTO `module_uums_dict_item` VALUES (119, '107', '1', '其他', NULL, 80, '2021-02-08 14:41:09', '2021-02-22 17:17:31');
INSERT INTO `module_uums_dict_item` VALUES (120, '109', '1', 'test1', NULL, 80, '2021-03-02 11:42:04', '2021-03-03 15:10:03');
INSERT INTO `module_uums_dict_item` VALUES (121, '109', '2', 'test2', NULL, 80, '2021-03-02 11:42:13', '2021-03-02 11:42:13');

-- ----------------------------
-- Table structure for module_uums_function
-- ----------------------------
DROP TABLE IF EXISTS `module_uums_function`;
CREATE TABLE `module_uums_function`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '功能编号',
  `p_id` bigint(20) UNSIGNED NULL DEFAULT NULL COMMENT '上级功能编号',
  `full_p_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '所以父级id',
  `name` varchar(32) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '功能名称',
  `order` int(11) NULL DEFAULT NULL COMMENT '排序',
  `status` int(11) NOT NULL DEFAULT 0 COMMENT '状态：0 正常 1：禁用',
  `type` int(11) NOT NULL COMMENT '类型：0菜单，1权限',
  `code` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '功能标识',
  `url` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '功能url',
  `icon` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '功能图标',
  `desc` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '备注信息',
  `create_time` varchar(32) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` varchar(32) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `index_status`(`status`) USING BTREE,
  INDEX `index_pId`(`p_id`) USING BTREE,
  INDEX `index_type`(`type`) USING BTREE,
  INDEX `index_code`(`code`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 71 CHARACTER SET = utf8 COLLATE = utf8_bin COMMENT = '菜单功能表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of module_uums_function
-- ----------------------------
INSERT INTO `module_uums_function` VALUES (20, NULL, NULL, '首页', 10, 0, 0, 'uums.home', '/uums/views/home.html', 'fa-home', '', '2021-01-18 16:15:03', '2021-03-02 17:42:43');
INSERT INTO `module_uums_function` VALUES (21, NULL, NULL, '统一用户管理', 20, 0, 0, 'uums.sys.manager', '', 'fa-cog', '', '2021-01-18 16:24:58', '2021-03-23 11:22:51');
INSERT INTO `module_uums_function` VALUES (22, NULL, NULL, '模块2', 30, 0, 0, '', 'https://www.baidu.com/', 'fa-archive', '', '2021-01-18 16:29:45', '2021-03-23 11:18:59');
INSERT INTO `module_uums_function` VALUES (23, NULL, NULL, '模块3', 40, 0, 0, '', 'https://www.baidu.com/', 'fa-area-chart', '', '2021-01-18 16:30:10', '2021-03-23 11:18:37');
INSERT INTO `module_uums_function` VALUES (24, 21, '-21', '用户与权限', 10, 0, 0, '', '', 'fa-paper-plane-o', '', '2021-01-18 16:31:53', '2021-03-02 17:44:28');
INSERT INTO `module_uums_function` VALUES (25, 24, '-21-24', '用户管理', 10, 0, 0, 'uums.user', '/uums/views/user/list.html', 'fa-user-circle', '', '2021-01-18 16:32:47', '2021-02-22 17:14:31');
INSERT INTO `module_uums_function` VALUES (26, 24, '-21-24', '部门管理', 20, 0, 0, '', '/uums/views/org/list.html', 'fa-sitemap', '', '2021-01-18 16:33:27', '2021-02-22 17:14:25');
INSERT INTO `module_uums_function` VALUES (27, 24, '-21-24', '角色管理', 30, 0, 0, '', '/uums/views/role/list.html', 'fa-users', '', '2021-01-18 16:36:36', '2021-02-22 17:14:17');
INSERT INTO `module_uums_function` VALUES (28, 24, '-21-24', '功能管理', 40, 0, 0, '', '/uums/views/function/list.html', 'fa-navicon', '', '2021-01-18 16:37:04', '2021-02-22 17:14:10');
INSERT INTO `module_uums_function` VALUES (29, 21, '-21', '参数配置', 20, 0, 0, '', '', 'fa-cog', '', '2021-01-18 16:37:46', '2021-01-27 15:18:44');
INSERT INTO `module_uums_function` VALUES (30, 29, '-21-29', '字典管理', 10, 0, 0, '', '/uums/views/dict/list.html', 'fa-book', '', '2021-01-18 16:38:17', '2021-02-22 17:14:01');
INSERT INTO `module_uums_function` VALUES (31, 29, '-21-29', '参数设置', 20, 0, 0, '', '/uums/views/parm/list.html', 'fa-puzzle-piece', '', '2021-01-18 16:38:51', '2021-02-22 17:13:55');
INSERT INTO `module_uums_function` VALUES (32, 21, '-21', '系统监控', 30, 0, 0, '', '', 'fa-heartbeat', '', '2021-01-18 16:39:33', '2021-01-18 16:43:39');
INSERT INTO `module_uums_function` VALUES (33, 32, '-21-32', '访问日志', 10, 0, 0, '', '/uums/views/log/list.html', 'fa-bug', '', '2021-01-18 16:40:56', '2021-02-22 17:13:44');
INSERT INTO `module_uums_function` VALUES (34, 32, '-21-32', '在线用户', 20, 0, 0, '', '/uums/views/online/list.html', 'fa-user-circle', '', '2021-01-18 16:41:40', '2021-02-22 17:13:14');
INSERT INTO `module_uums_function` VALUES (35, 25, '-21-24-25', '新增', 10, 0, 1, 'uums.user.add', '', '', '', '2021-01-19 09:03:09', '2021-02-20 16:29:33');
INSERT INTO `module_uums_function` VALUES (36, 25, '-21-24-25', '删除', 30, 0, 1, 'uums.user.del', '', '', '', '2021-01-19 10:02:51', '2021-02-01 17:34:59');
INSERT INTO `module_uums_function` VALUES (37, 25, '-21-24-25', '修改', 20, 0, 1, 'uums.user.edit', '', '', '', '2021-01-19 10:03:25', '2021-02-20 16:29:33');
INSERT INTO `module_uums_function` VALUES (38, 25, '-21-24-25', '重置密码', 40, 0, 1, 'uums.user.reset.password', '', '', '', '2021-01-28 10:23:03', '2021-02-07 13:37:50');
INSERT INTO `module_uums_function` VALUES (39, 26, '-21-24-26', '新增', 10, 0, 1, 'uums.org.add', '', '', '', '2021-02-06 17:41:04', '2021-02-22 15:38:34');
INSERT INTO `module_uums_function` VALUES (40, 26, '-21-24-26', '编辑', 20, 0, 1, 'uums.org.edit', '', '', '', '2021-02-06 17:41:23', '2021-03-03 14:39:26');
INSERT INTO `module_uums_function` VALUES (41, 26, '-21-24-26', '删除', 30, 0, 1, 'uums.org.del', '', '', '', '2021-02-06 17:41:44', '2021-02-06 17:41:44');
INSERT INTO `module_uums_function` VALUES (42, 27, '-21-24-27', '新增', 10, 0, 1, 'uums.role.add', '', '', '', '2021-02-06 17:42:21', '2021-02-06 17:42:21');
INSERT INTO `module_uums_function` VALUES (43, 27, '-21-24-27', '编辑', 20, 0, 1, 'uums.role.edit', '', '', '', '2021-02-06 17:42:32', '2021-02-06 17:42:32');
INSERT INTO `module_uums_function` VALUES (44, 27, '-21-24-27', '删除', 30, 0, 1, 'uums.role.del', '', '', '', '2021-02-06 17:42:54', '2021-02-06 17:42:54');
INSERT INTO `module_uums_function` VALUES (45, 27, '-21-24-27', '配置功能权限', 80, 0, 1, 'uums.role.cfg', '', '', '', '2021-02-06 17:43:22', '2021-02-06 17:43:22');
INSERT INTO `module_uums_function` VALUES (46, 28, '-21-24-28', '新增', 10, 0, 1, 'uums.func.add', '', '', '', '2021-02-06 17:49:27', '2021-02-06 17:49:27');
INSERT INTO `module_uums_function` VALUES (47, 28, '-21-24-28', '编辑', 20, 0, 1, 'uums.func.edit', '', '', '', '2021-02-06 17:49:49', '2021-02-06 17:49:49');
INSERT INTO `module_uums_function` VALUES (48, 28, '-21-24-28', '删除', 30, 0, 1, 'uums.func.del', '', '', '', '2021-02-06 17:50:04', '2021-02-06 17:50:04');
INSERT INTO `module_uums_function` VALUES (49, 31, '-21-29-31', '新增', 10, 0, 1, 'uums.parm.add', '', '', '', '2021-02-07 14:50:29', '2021-02-09 11:33:35');
INSERT INTO `module_uums_function` VALUES (50, 31, '-21-29-31', '编辑', 20, 0, 1, 'uums.parm.edit', '', '', '', '2021-02-07 14:50:49', '2021-02-09 11:33:42');
INSERT INTO `module_uums_function` VALUES (51, 31, '-21-29-31', '删除', 30, 0, 1, 'uums.parm.del', '', '', '', '2021-02-07 14:51:07', '2021-02-09 11:33:49');
INSERT INTO `module_uums_function` VALUES (52, 25, '-21-24-25', '查询', 5, 0, 1, 'uums.user.view', '', '', '', '2021-02-20 15:42:51', '2021-03-03 14:39:33');
INSERT INTO `module_uums_function` VALUES (53, 26, '-21-24-26', '查询', 5, 0, 1, 'uums.org.view', '', '', '', '2021-02-20 15:43:23', '2021-02-22 15:38:33');
INSERT INTO `module_uums_function` VALUES (54, 27, '-21-24-27', '查看', 5, 0, 1, 'uums.role.view', '', '', '', '2021-02-20 15:43:49', '2021-02-20 15:43:49');
INSERT INTO `module_uums_function` VALUES (55, 28, '-21-24-28', '查看', 5, 0, 1, 'uums.func.view', '', '', '', '2021-02-20 15:44:15', '2021-02-20 15:44:15');
INSERT INTO `module_uums_function` VALUES (56, 31, '-21-29-31', '查看', 5, 0, 1, 'uums.parm.view', '', '', '', '2021-02-20 15:45:07', '2021-02-20 15:45:07');
INSERT INTO `module_uums_function` VALUES (57, 30, '-21-29-30', '查看', 10, 0, 1, 'uums.dict.view', '', '', '', '2021-02-20 15:47:47', '2021-02-20 15:47:47');
INSERT INTO `module_uums_function` VALUES (58, 30, '-21-29-30', '新增', 20, 0, 1, 'uums.dict.add', '', '', '', '2021-02-20 15:48:04', '2021-02-20 15:48:04');
INSERT INTO `module_uums_function` VALUES (59, 30, '-21-29-30', '编辑', 30, 0, 1, 'uums.dict.edit', '', '', '', '2021-02-20 15:48:24', '2021-02-20 15:48:24');
INSERT INTO `module_uums_function` VALUES (60, 30, '-21-29-30', '删除', 40, 0, 1, 'uums.dict.del', '', '', '', '2021-02-20 15:48:42', '2021-02-20 15:48:42');
INSERT INTO `module_uums_function` VALUES (61, 30, '-21-29-30', '新增字典值', 50, 0, 1, 'uums.dict.add.item', '', '', '', '2021-03-02 10:32:40', '2021-03-02 10:32:40');
INSERT INTO `module_uums_function` VALUES (62, 33, '-21-32-33', '日志查询', 10, 0, 1, 'uums.log.view', '', '', '', '2021-03-02 17:04:34', '2021-03-02 17:04:34');
INSERT INTO `module_uums_function` VALUES (63, 34, '-21-32-34', '在线用户查询', 10, 0, 1, 'uums.online.user.view', '', '', '', '2021-03-02 17:05:19', '2021-03-02 17:05:19');
INSERT INTO `module_uums_function` VALUES (64, 32, '-21-32', '在线文档', 80, 0, 0, '', '/uums/swagger-ui/index.html', 'fa-paper-plane-o', '', '2021-03-04 10:52:08', '2021-03-04 10:53:54');

-- ----------------------------
-- Table structure for module_uums_org
-- ----------------------------
DROP TABLE IF EXISTS `module_uums_org`;
CREATE TABLE `module_uums_org`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '组织机构编号',
  `p_id` bigint(20) UNSIGNED NULL DEFAULT NULL COMMENT '上级组织机构编号',
  `full_p_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '所以父级id',
  `name` varchar(32) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '组织机构名称',
  `order` int(11) NULL DEFAULT NULL COMMENT '排序',
  `status` int(11) NOT NULL DEFAULT 0 COMMENT '状态：0 正常 1：禁用',
  `desc` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '备注信息',
  `create_time` varchar(32) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` varchar(32) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `index_status`(`status`) USING BTREE,
  INDEX `index_code`(`p_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 70 CHARACTER SET = utf8 COLLATE = utf8_bin COMMENT = '组织机构表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of module_uums_org
-- ----------------------------
INSERT INTO `module_uums_org` VALUES (1, NULL, NULL, '上海分部', 10, 0, '', '2021-01-04 12:12:12', '2021-03-22 14:09:14');
INSERT INTO `module_uums_org` VALUES (2, 1, '-1', '开发部', 60, 0, '', '2021-01-04 12:12:12', '2021-03-22 14:08:40');
INSERT INTO `module_uums_org` VALUES (3, 1, '-1', '运维部', 70, 0, '', '2021-01-04 12:12:12', '2021-03-16 10:25:19');
INSERT INTO `module_uums_org` VALUES (6, NULL, NULL, '深圳分部', 20, 0, '', '2021-01-04 12:12:12', '2021-01-18 09:40:35');
INSERT INTO `module_uums_org` VALUES (29, 6, '-6', '运维部', 50, 0, '', '2021-01-13 13:43:40', '2021-01-18 09:40:37');
INSERT INTO `module_uums_org` VALUES (59, 1, '-1', '测试部', 80, 0, '', '2021-03-02 09:56:09', '2021-03-16 10:25:20');

-- ----------------------------
-- Table structure for module_uums_parm
-- ----------------------------
DROP TABLE IF EXISTS `module_uums_parm`;
CREATE TABLE `module_uums_parm`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键',
  `parm_type` int(11) NOT NULL DEFAULT 0 COMMENT '参数类型',
  `parm_key` varchar(36) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '参数key',
  `parm_value` varchar(256) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '参数value',
  `parm_desc` varchar(256) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '描述',
  `create_time` varchar(32) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` varchar(32) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `index_parm_key`(`parm_key`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 110 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci COMMENT = '参数配置表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of module_uums_parm
-- ----------------------------
INSERT INTO `module_uums_parm` VALUES (1, 0, 'uums-defualt-password', '123456', '新增用户或重置密码时，使用这个密码作为默认密码', NULL, '2021-03-22 14:28:30');
INSERT INTO `module_uums_parm` VALUES (107, 0, 'token-timeout-minutes', '30', '用户token超时时间(单位：分钟)。用户超过指定时间未次访问系统，则token失效，再次访问需要重新认证！未配置或配置错误（非数字、0或负数等），则默认为30分钟', '2021-02-20 11:19:39', '2021-03-02 10:57:17');
INSERT INTO `module_uums_parm` VALUES (108, 0, 'allow-multiple-client-online', '0', '同一个用户是否允许多个客户端同时登录在线操作，0：不允许，1：允许！未配置或配置错误（非0和1），则默认为0。建议配置为0', '2021-02-22 08:33:16', '2021-02-24 19:01:05');

-- ----------------------------
-- Table structure for module_uums_role
-- ----------------------------
DROP TABLE IF EXISTS `module_uums_role`;
CREATE TABLE `module_uums_role`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '角色编号',
  `code` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '角色编号',
  `name` varchar(32) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '角色名称',
  `status` int(11) NOT NULL DEFAULT 0 COMMENT '状态：0 正常 1：禁用',
  `order` int(11) NULL DEFAULT NULL COMMENT '排序',
  `desc` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '备注信息',
  `create_time` varchar(32) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` varchar(32) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `index_status`(`status`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8 COLLATE = utf8_bin COMMENT = '组织机构表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of module_uums_role
-- ----------------------------
INSERT INTO `module_uums_role` VALUES (2, 'super', '超级管理员', 0, 10, '拥有最高权限', '2021-01-19 11:10:19', '2021-02-24 18:54:51');
INSERT INTO `module_uums_role` VALUES (3, 'user', '普通用户', 0, 80, '', '2021-01-19 11:15:09', '2021-02-24 18:54:50');
INSERT INTO `module_uums_role` VALUES (4, 'admin', '普通管理员', 0, 20, '', '2021-01-20 10:42:55', '2021-02-24 18:54:51');
INSERT INTO `module_uums_role` VALUES (5, 'test', '测试1', 0, 80, '测试1', '2021-03-02 09:41:00', '2021-03-02 11:28:57');

-- ----------------------------
-- Table structure for module_uums_role_function
-- ----------------------------
DROP TABLE IF EXISTS `module_uums_role_function`;
CREATE TABLE `module_uums_role_function`  (
  `role_id` bigint(20) UNSIGNED NOT NULL COMMENT '角色编号',
  `function_id` bigint(20) UNSIGNED NOT NULL COMMENT '功能编号',
  INDEX `index_userId`(`function_id`) USING BTREE,
  INDEX `index_orgId`(`role_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin COMMENT = '角色功能关联表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of module_uums_role_function
-- ----------------------------
INSERT INTO `module_uums_role_function` VALUES (5, 20);
INSERT INTO `module_uums_role_function` VALUES (5, 25);
INSERT INTO `module_uums_role_function` VALUES (5, 52);
INSERT INTO `module_uums_role_function` VALUES (5, 35);
INSERT INTO `module_uums_role_function` VALUES (5, 37);
INSERT INTO `module_uums_role_function` VALUES (5, 36);
INSERT INTO `module_uums_role_function` VALUES (5, 38);
INSERT INTO `module_uums_role_function` VALUES (5, 26);
INSERT INTO `module_uums_role_function` VALUES (5, 53);
INSERT INTO `module_uums_role_function` VALUES (5, 39);
INSERT INTO `module_uums_role_function` VALUES (5, 40);
INSERT INTO `module_uums_role_function` VALUES (5, 41);
INSERT INTO `module_uums_role_function` VALUES (5, 27);
INSERT INTO `module_uums_role_function` VALUES (5, 54);
INSERT INTO `module_uums_role_function` VALUES (5, 42);
INSERT INTO `module_uums_role_function` VALUES (5, 43);
INSERT INTO `module_uums_role_function` VALUES (5, 44);
INSERT INTO `module_uums_role_function` VALUES (5, 45);
INSERT INTO `module_uums_role_function` VALUES (5, 29);
INSERT INTO `module_uums_role_function` VALUES (5, 30);
INSERT INTO `module_uums_role_function` VALUES (5, 57);
INSERT INTO `module_uums_role_function` VALUES (5, 58);
INSERT INTO `module_uums_role_function` VALUES (5, 59);
INSERT INTO `module_uums_role_function` VALUES (5, 60);
INSERT INTO `module_uums_role_function` VALUES (5, 61);
INSERT INTO `module_uums_role_function` VALUES (5, 31);
INSERT INTO `module_uums_role_function` VALUES (5, 56);
INSERT INTO `module_uums_role_function` VALUES (5, 49);
INSERT INTO `module_uums_role_function` VALUES (5, 50);
INSERT INTO `module_uums_role_function` VALUES (5, 51);
INSERT INTO `module_uums_role_function` VALUES (5, 32);
INSERT INTO `module_uums_role_function` VALUES (5, 33);
INSERT INTO `module_uums_role_function` VALUES (5, 62);
INSERT INTO `module_uums_role_function` VALUES (5, 34);
INSERT INTO `module_uums_role_function` VALUES (5, 63);
INSERT INTO `module_uums_role_function` VALUES (3, 20);
INSERT INTO `module_uums_role_function` VALUES (3, 52);
INSERT INTO `module_uums_role_function` VALUES (3, 25);
INSERT INTO `module_uums_role_function` VALUES (3, 24);
INSERT INTO `module_uums_role_function` VALUES (3, 21);
INSERT INTO `module_uums_role_function` VALUES (3, 35);
INSERT INTO `module_uums_role_function` VALUES (3, 37);
INSERT INTO `module_uums_role_function` VALUES (3, 53);
INSERT INTO `module_uums_role_function` VALUES (3, 26);
INSERT INTO `module_uums_role_function` VALUES (3, 39);
INSERT INTO `module_uums_role_function` VALUES (3, 40);
INSERT INTO `module_uums_role_function` VALUES (3, 54);
INSERT INTO `module_uums_role_function` VALUES (3, 27);
INSERT INTO `module_uums_role_function` VALUES (3, 42);
INSERT INTO `module_uums_role_function` VALUES (3, 43);
INSERT INTO `module_uums_role_function` VALUES (3, 55);
INSERT INTO `module_uums_role_function` VALUES (3, 28);
INSERT INTO `module_uums_role_function` VALUES (3, 46);
INSERT INTO `module_uums_role_function` VALUES (3, 47);
INSERT INTO `module_uums_role_function` VALUES (3, 57);
INSERT INTO `module_uums_role_function` VALUES (3, 30);
INSERT INTO `module_uums_role_function` VALUES (3, 29);
INSERT INTO `module_uums_role_function` VALUES (3, 58);
INSERT INTO `module_uums_role_function` VALUES (3, 56);
INSERT INTO `module_uums_role_function` VALUES (3, 31);
INSERT INTO `module_uums_role_function` VALUES (3, 32);
INSERT INTO `module_uums_role_function` VALUES (3, 33);
INSERT INTO `module_uums_role_function` VALUES (3, 62);
INSERT INTO `module_uums_role_function` VALUES (3, 34);
INSERT INTO `module_uums_role_function` VALUES (3, 63);
INSERT INTO `module_uums_role_function` VALUES (3, 64);
INSERT INTO `module_uums_role_function` VALUES (3, 36);
INSERT INTO `module_uums_role_function` VALUES (3, 38);
INSERT INTO `module_uums_role_function` VALUES (3, 41);
INSERT INTO `module_uums_role_function` VALUES (3, 44);
INSERT INTO `module_uums_role_function` VALUES (3, 45);
INSERT INTO `module_uums_role_function` VALUES (3, 48);
INSERT INTO `module_uums_role_function` VALUES (3, 59);
INSERT INTO `module_uums_role_function` VALUES (3, 60);
INSERT INTO `module_uums_role_function` VALUES (3, 61);
INSERT INTO `module_uums_role_function` VALUES (3, 49);
INSERT INTO `module_uums_role_function` VALUES (3, 50);
INSERT INTO `module_uums_role_function` VALUES (3, 51);
INSERT INTO `module_uums_role_function` VALUES (3, 22);
INSERT INTO `module_uums_role_function` VALUES (3, 23);
INSERT INTO `module_uums_role_function` VALUES (3, 65);
INSERT INTO `module_uums_role_function` VALUES (3, 66);
INSERT INTO `module_uums_role_function` VALUES (3, 67);
INSERT INTO `module_uums_role_function` VALUES (3, 68);
INSERT INTO `module_uums_role_function` VALUES (3, 69);
INSERT INTO `module_uums_role_function` VALUES (4, 20);
INSERT INTO `module_uums_role_function` VALUES (4, 21);
INSERT INTO `module_uums_role_function` VALUES (4, 24);
INSERT INTO `module_uums_role_function` VALUES (4, 25);
INSERT INTO `module_uums_role_function` VALUES (4, 52);
INSERT INTO `module_uums_role_function` VALUES (4, 35);
INSERT INTO `module_uums_role_function` VALUES (4, 37);
INSERT INTO `module_uums_role_function` VALUES (4, 36);
INSERT INTO `module_uums_role_function` VALUES (4, 38);
INSERT INTO `module_uums_role_function` VALUES (4, 26);
INSERT INTO `module_uums_role_function` VALUES (4, 53);
INSERT INTO `module_uums_role_function` VALUES (4, 39);
INSERT INTO `module_uums_role_function` VALUES (4, 40);
INSERT INTO `module_uums_role_function` VALUES (4, 41);
INSERT INTO `module_uums_role_function` VALUES (4, 27);
INSERT INTO `module_uums_role_function` VALUES (4, 54);
INSERT INTO `module_uums_role_function` VALUES (4, 42);
INSERT INTO `module_uums_role_function` VALUES (4, 43);
INSERT INTO `module_uums_role_function` VALUES (4, 44);
INSERT INTO `module_uums_role_function` VALUES (4, 45);
INSERT INTO `module_uums_role_function` VALUES (4, 28);
INSERT INTO `module_uums_role_function` VALUES (4, 55);
INSERT INTO `module_uums_role_function` VALUES (4, 46);
INSERT INTO `module_uums_role_function` VALUES (4, 47);
INSERT INTO `module_uums_role_function` VALUES (4, 48);
INSERT INTO `module_uums_role_function` VALUES (4, 29);
INSERT INTO `module_uums_role_function` VALUES (4, 30);
INSERT INTO `module_uums_role_function` VALUES (4, 57);
INSERT INTO `module_uums_role_function` VALUES (4, 58);
INSERT INTO `module_uums_role_function` VALUES (4, 59);
INSERT INTO `module_uums_role_function` VALUES (4, 60);
INSERT INTO `module_uums_role_function` VALUES (4, 61);
INSERT INTO `module_uums_role_function` VALUES (4, 31);
INSERT INTO `module_uums_role_function` VALUES (4, 56);
INSERT INTO `module_uums_role_function` VALUES (4, 49);
INSERT INTO `module_uums_role_function` VALUES (4, 50);
INSERT INTO `module_uums_role_function` VALUES (4, 51);
INSERT INTO `module_uums_role_function` VALUES (4, 32);
INSERT INTO `module_uums_role_function` VALUES (4, 33);
INSERT INTO `module_uums_role_function` VALUES (4, 62);
INSERT INTO `module_uums_role_function` VALUES (4, 34);
INSERT INTO `module_uums_role_function` VALUES (4, 63);
INSERT INTO `module_uums_role_function` VALUES (4, 64);
INSERT INTO `module_uums_role_function` VALUES (4, 22);
INSERT INTO `module_uums_role_function` VALUES (4, 23);
INSERT INTO `module_uums_role_function` VALUES (2, 20);
INSERT INTO `module_uums_role_function` VALUES (2, 25);
INSERT INTO `module_uums_role_function` VALUES (2, 52);
INSERT INTO `module_uums_role_function` VALUES (2, 35);
INSERT INTO `module_uums_role_function` VALUES (2, 37);
INSERT INTO `module_uums_role_function` VALUES (2, 36);
INSERT INTO `module_uums_role_function` VALUES (2, 38);
INSERT INTO `module_uums_role_function` VALUES (2, 24);
INSERT INTO `module_uums_role_function` VALUES (2, 21);
INSERT INTO `module_uums_role_function` VALUES (2, 26);
INSERT INTO `module_uums_role_function` VALUES (2, 53);
INSERT INTO `module_uums_role_function` VALUES (2, 39);
INSERT INTO `module_uums_role_function` VALUES (2, 40);
INSERT INTO `module_uums_role_function` VALUES (2, 41);
INSERT INTO `module_uums_role_function` VALUES (2, 27);
INSERT INTO `module_uums_role_function` VALUES (2, 54);
INSERT INTO `module_uums_role_function` VALUES (2, 42);
INSERT INTO `module_uums_role_function` VALUES (2, 43);
INSERT INTO `module_uums_role_function` VALUES (2, 44);
INSERT INTO `module_uums_role_function` VALUES (2, 45);
INSERT INTO `module_uums_role_function` VALUES (2, 55);
INSERT INTO `module_uums_role_function` VALUES (2, 28);
INSERT INTO `module_uums_role_function` VALUES (2, 29);
INSERT INTO `module_uums_role_function` VALUES (2, 30);
INSERT INTO `module_uums_role_function` VALUES (2, 57);
INSERT INTO `module_uums_role_function` VALUES (2, 58);
INSERT INTO `module_uums_role_function` VALUES (2, 59);
INSERT INTO `module_uums_role_function` VALUES (2, 60);
INSERT INTO `module_uums_role_function` VALUES (2, 61);
INSERT INTO `module_uums_role_function` VALUES (2, 31);
INSERT INTO `module_uums_role_function` VALUES (2, 56);
INSERT INTO `module_uums_role_function` VALUES (2, 49);
INSERT INTO `module_uums_role_function` VALUES (2, 50);
INSERT INTO `module_uums_role_function` VALUES (2, 51);
INSERT INTO `module_uums_role_function` VALUES (2, 32);
INSERT INTO `module_uums_role_function` VALUES (2, 33);
INSERT INTO `module_uums_role_function` VALUES (2, 62);
INSERT INTO `module_uums_role_function` VALUES (2, 34);
INSERT INTO `module_uums_role_function` VALUES (2, 63);
INSERT INTO `module_uums_role_function` VALUES (2, 64);
INSERT INTO `module_uums_role_function` VALUES (2, 22);
INSERT INTO `module_uums_role_function` VALUES (2, 23);
INSERT INTO `module_uums_role_function` VALUES (2, 46);
INSERT INTO `module_uums_role_function` VALUES (2, 47);
INSERT INTO `module_uums_role_function` VALUES (2, 48);
INSERT INTO `module_uums_role_function` VALUES (6, 24);
INSERT INTO `module_uums_role_function` VALUES (6, 25);
INSERT INTO `module_uums_role_function` VALUES (6, 52);
INSERT INTO `module_uums_role_function` VALUES (6, 35);
INSERT INTO `module_uums_role_function` VALUES (6, 37);
INSERT INTO `module_uums_role_function` VALUES (6, 36);
INSERT INTO `module_uums_role_function` VALUES (6, 38);
INSERT INTO `module_uums_role_function` VALUES (6, 26);
INSERT INTO `module_uums_role_function` VALUES (6, 53);
INSERT INTO `module_uums_role_function` VALUES (6, 39);
INSERT INTO `module_uums_role_function` VALUES (6, 40);
INSERT INTO `module_uums_role_function` VALUES (6, 41);
INSERT INTO `module_uums_role_function` VALUES (6, 27);
INSERT INTO `module_uums_role_function` VALUES (6, 54);
INSERT INTO `module_uums_role_function` VALUES (6, 42);
INSERT INTO `module_uums_role_function` VALUES (6, 43);
INSERT INTO `module_uums_role_function` VALUES (6, 44);
INSERT INTO `module_uums_role_function` VALUES (6, 45);
INSERT INTO `module_uums_role_function` VALUES (6, 28);
INSERT INTO `module_uums_role_function` VALUES (6, 55);
INSERT INTO `module_uums_role_function` VALUES (6, 46);
INSERT INTO `module_uums_role_function` VALUES (6, 47);
INSERT INTO `module_uums_role_function` VALUES (6, 48);
INSERT INTO `module_uums_role_function` VALUES (6, 29);
INSERT INTO `module_uums_role_function` VALUES (6, 30);
INSERT INTO `module_uums_role_function` VALUES (6, 57);
INSERT INTO `module_uums_role_function` VALUES (6, 58);
INSERT INTO `module_uums_role_function` VALUES (6, 59);
INSERT INTO `module_uums_role_function` VALUES (6, 60);
INSERT INTO `module_uums_role_function` VALUES (6, 61);
INSERT INTO `module_uums_role_function` VALUES (6, 31);
INSERT INTO `module_uums_role_function` VALUES (6, 56);
INSERT INTO `module_uums_role_function` VALUES (6, 49);
INSERT INTO `module_uums_role_function` VALUES (6, 50);
INSERT INTO `module_uums_role_function` VALUES (6, 51);
INSERT INTO `module_uums_role_function` VALUES (6, 33);
INSERT INTO `module_uums_role_function` VALUES (6, 62);
INSERT INTO `module_uums_role_function` VALUES (6, 34);
INSERT INTO `module_uums_role_function` VALUES (6, 63);
INSERT INTO `module_uums_role_function` VALUES (6, 22);
INSERT INTO `module_uums_role_function` VALUES (6, 23);

-- ----------------------------
-- Table structure for module_uums_token
-- ----------------------------
DROP TABLE IF EXISTS `module_uums_token`;
CREATE TABLE `module_uums_token`  (
  `token` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT 'TOKEN',
  `user_id` bigint(20) UNSIGNED NULL DEFAULT NULL COMMENT '用户Id',
  `create_time` varchar(32) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '创建时间',
  `refresh_time` bigint(20) NULL DEFAULT NULL COMMENT '刷新时间',
  `token_timeout_minutes` int(11) NULL DEFAULT NULL COMMENT 'token超时时间，默认30分钟',
  `client_ip` varchar(125) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '客户端IP',
  `client_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '客户端设备名称',
  PRIMARY KEY (`token`) USING BTREE,
  INDEX `index_userId`(`user_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin COMMENT = '用户表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of module_uums_token
-- ----------------------------

-- ----------------------------
-- Table structure for module_uums_token_his
-- ----------------------------
DROP TABLE IF EXISTS `module_uums_token_his`;
CREATE TABLE `module_uums_token_his`  (
  `token` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT 'TOKEN',
  `user_id` bigint(20) UNSIGNED NULL DEFAULT NULL COMMENT '用户Id',
  `create_time` varchar(32) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '创建时间',
  `refresh_time` bigint(20) NULL DEFAULT NULL COMMENT '刷新时间',
  `delete_time` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `token_timeout_minutes` int(11) NULL DEFAULT NULL COMMENT 'token超时时间，默认30分钟',
  `client_ip` varchar(125) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '客户端IP',
  `client_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '客户端设备名称',
  `del_type` int(11) NULL DEFAULT NULL COMMENT '删除类型：0正常退出删除, 1超时删除，2不允许不允许多个客户端同时在线删除',
  PRIMARY KEY (`token`) USING BTREE,
  INDEX `index_userId`(`user_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin COMMENT = '用户表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of module_uums_token_his
-- ----------------------------

-- ----------------------------
-- Table structure for module_uums_user
-- ----------------------------
DROP TABLE IF EXISTS `module_uums_user`;
CREATE TABLE `module_uums_user`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `username` varchar(32) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '登录名',
  `password` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '密码',
  `full_name` varchar(32) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '姓名',
  `email` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '邮箱',
  `mobile_number` varchar(32) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '手机号码',
  `office_phone` varchar(32) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '办公电话',
  `status` int(11) NULL DEFAULT 0 COMMENT '用户状态：0 正常 1：禁用',
  `desc` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '备注信息',
  `create_time` varchar(32) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` varchar(32) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `index_login_name`(`username`) USING BTREE,
  INDEX `index_status`(`status`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1014 CHARACTER SET = utf8 COLLATE = utf8_bin COMMENT = '用户表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of module_uums_user
-- ----------------------------
INSERT INTO `module_uums_user` VALUES (1007, 'admin', 'e10adc3949ba59abbe56e057f20f883e', '管理员', '000000@qq.com', '13700000000', '021-000000', 0, '', '2021-01-21 13:52:30', '2021-03-23 11:24:24');
INSERT INTO `module_uums_user` VALUES (1008, '李四', 'e10adc3949ba59abbe56e057f20f883e', '李四', '000000@qq.com', '13700000000', '021-000000', 0, '测试', '2021-01-21 13:58:14', '2021-03-23 11:24:21');
INSERT INTO `module_uums_user` VALUES (1011, 'lkw', 'e10adc3949ba59abbe56e057f20f883e', '张小三', '', '13700000000', '65445580', 0, '测试1', '2021-03-02 09:43:55', '2021-03-23 11:24:30');

-- ----------------------------
-- Table structure for module_uums_user_org
-- ----------------------------
DROP TABLE IF EXISTS `module_uums_user_org`;
CREATE TABLE `module_uums_user_org`  (
  `org_id` bigint(20) UNSIGNED NOT NULL COMMENT '组织机构编号',
  `user_id` bigint(20) UNSIGNED NOT NULL COMMENT '用户编号',
  INDEX `index_userId`(`user_id`) USING BTREE,
  INDEX `index_orgId`(`org_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin COMMENT = '用户组织机构关联表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of module_uums_user_org
-- ----------------------------
INSERT INTO `module_uums_user_org` VALUES (2, 1008);
INSERT INTO `module_uums_user_org` VALUES (6, 1008);
INSERT INTO `module_uums_user_org` VALUES (2, 1007);
INSERT INTO `module_uums_user_org` VALUES (1, 1011);
INSERT INTO `module_uums_user_org` VALUES (59, 1011);

-- ----------------------------
-- Table structure for module_uums_user_role
-- ----------------------------
DROP TABLE IF EXISTS `module_uums_user_role`;
CREATE TABLE `module_uums_user_role`  (
  `role_id` bigint(20) UNSIGNED NOT NULL COMMENT '角色编号',
  `user_id` bigint(20) UNSIGNED NOT NULL COMMENT '用户编号',
  INDEX `index_userId`(`user_id`) USING BTREE,
  INDEX `index_orgId`(`role_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin COMMENT = '用户角色关联表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of module_uums_user_role
-- ----------------------------
INSERT INTO `module_uums_user_role` VALUES (3, 1008);
INSERT INTO `module_uums_user_role` VALUES (2, 1007);
INSERT INTO `module_uums_user_role` VALUES (5, 1011);

-- ----------------------------
-- Procedure structure for del_expire_token_task
-- ----------------------------
DROP PROCEDURE IF EXISTS `del_expire_token_task`;
delimiter ;;
CREATE PROCEDURE `del_expire_token_task`()
BEGIN
	insert into module_uums_token_his(`token`,`user_id`,`create_time`,`refresh_time`,`delete_time`,`token_timeout_minutes`,`client_ip`,`client_name`,`del_type`) 
	SELECT `token`,`user_id`,`create_time`,`refresh_time`, date_format(now(),'%Y-%m-%d %H:%i:%s') as `delete_time`,`token_timeout_minutes`,`client_ip`,`client_name`,1 as `del_type`
	from module_uums_token where `refresh_time` < (unix_timestamp() * 1000) - (`token_timeout_minutes` * 60 * 1000);
	delete from module_uums_token where `refresh_time` < (unix_timestamp() * 1000) - (`token_timeout_minutes` * 60 * 1000);
END
;;
delimiter ;

-- ----------------------------
-- Event structure for del_expire_jwt_token_task_per_5m
-- ----------------------------
DROP EVENT IF EXISTS `del_expire_jwt_token_task_per_5m`;
delimiter ;;
CREATE EVENT `del_expire_jwt_token_task_per_5m`
ON SCHEDULE
EVERY '1' MINUTE STARTS '2020-12-29 09:52:37'
ON COMPLETION PRESERVE
DO call del_expire_jwt_token_task()
;;
delimiter ;

SET FOREIGN_KEY_CHECKS = 1;
