package com.hm.uums.controller;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hm.common.annotation.Function;
import com.hm.common.api.ApiResult;
import com.hm.common.api.ApiStatus;
import com.hm.common.utils.DateUtils;
import com.hm.uums.service.OnlineUserService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * @author hwg
 *
 *         2020年12月18日
 */
@Api(tags = "8.在线用户接口")
@RestController
@RequestMapping("/online/user")
public class OnlineUserController extends BaseController {

	Logger logger = LoggerFactory.getLogger(OnlineUserController.class);

	@Autowired
	private OnlineUserService onlineUserService;

	@ApiOperation("在线用户查询")
	@GetMapping("/list")
	@Function(module = "uums", code = "uums.online.user.view", description = "在线用户查询")
	public ApiResult list(HttpServletRequest request) throws Exception {
		String reqId = getReqId(request);
		Map<String, Object> params = getPageParam(request);
		params.put("username", request.getParameter("username"));
		params.put("fullname", request.getParameter("fullname"));

		List<Map<String, Object>> dataList = onlineUserService.list(params);
		for (Map<String, Object> m : dataList) {
			m.put("refreshTime", DateUtils.parse((Long) m.get("refreshTime")));
		}
		Map<String, Object> mData = toMapResult(dataList, dataList.size());
		return ApiResult.of(reqId, ApiStatus.STATUS_200, "查询成功", request.getRequestURI(), mData);
	}

}
