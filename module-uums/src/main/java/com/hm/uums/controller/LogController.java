package com.hm.uums.controller;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hm.common.annotation.Function;
import com.hm.common.api.ApiResult;
import com.hm.common.api.ApiStatus;
import com.hm.common.utils.DateUtils;
import com.hm.common.utils.StringUtils;
import com.hm.uums.service.LogService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * @author hwg
 *
 *         2020年12月18日
 */
@Api(tags = "5.日志接口")
@RestController
@RequestMapping("/log")
public class LogController extends BaseController {

	Logger logger = LoggerFactory.getLogger(LogController.class);

	@Autowired
	private LogService logService;

	@ApiOperation("日志查询")
	@GetMapping("/list")
	@Function(module = "uums", code = "uums.log.view", description = "日志查询")
	public ApiResult list(HttpServletRequest request) throws Exception {
		String reqId = getReqId(request);
		Map<String, Object> params = getPageParam(request);

		String reqStartDate = request.getParameter("reqStartDate");
		String reqEndDate = request.getParameter("reqEndDate");

		params.put("userId", request.getParameter("userId"));
		params.put("clientIp", request.getParameter("clientIp"));

		params.put("reqStartDate", StringUtils.isEmpty(reqStartDate) ? null : DateUtils.parse(reqStartDate + " 00:00:00").getTime());
		params.put("reqEndDate", StringUtils.isEmpty(reqEndDate) ? null : DateUtils.parse(reqEndDate + " 23:59:59").getTime());

		List<Map<String, Object>> dataList = logService.list(params);
		for (Map<String, Object> m : dataList) {
			m.put("reqTime", DateUtils.parse((Long) m.get("reqTime")));
		}
		Map<String, Object> mData = toMapResult(dataList, -1);
		return ApiResult.of(reqId, ApiStatus.STATUS_200, "查询成功", request.getRequestURI(), mData);
	}

}
