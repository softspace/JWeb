package com.hm.uums.controller;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hm.common.annotation.Function;
import com.hm.common.api.ApiResult;
import com.hm.common.api.ApiStatus;
import com.hm.common.utils.StringUtils;
import com.hm.uums.service.DictService;
import com.hm.uums.service.ParmService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;

/**
 * @author hwg
 *
 *         2020年12月18日
 */
@Api(tags = "7.参数接口")
@RestController
@RequestMapping("/parm")
public class ParmController extends BaseController {

	Logger logger = LoggerFactory.getLogger(ParmController.class);

	@Autowired
	private ParmService parmService;

	@Autowired
	private DictService dictService;

	@ApiOperation("参数查询")
	@ApiImplicitParams({@ApiImplicitParam(name = "name", value = "参数名称", required = false, dataType = "String", paramType = "query"), @ApiImplicitParam(name = "status", value = "状态", required = false, dataType = "Integer", paramType = "query")})
	@GetMapping("/list")
	@Function(module = "uums", code = "uums.parm.view", description = "参数查询")
	public ApiResult list(HttpServletRequest request) {
		String reqId = getReqId(request);
		Map<String, Object> params = getPageParam(request);
		params.put("parmKey", request.getParameter("parmKey"));

		List<Map<String, Object>> dataList = parmService.list(params);

		Map<String, Object> dMap = dictService.getDictByCode("uums-parm-type");

		if (StringUtils.isEmpty(dMap)) {
			dMap = new HashMap<String, Object>();
		}

		if (StringUtils.isEmpty(dMap.get("itemList"))) {
			dMap.put("itemList", new ArrayList<Map<String, Object>>());
		}

		Map<String, String> parmTypeMap = new HashMap<String, String>();

		for (Map<String, Object> m : (List<Map<String, Object>>) dMap.get("itemList")) {
			parmTypeMap.put(StringUtils.toString(m.get("dictItemCode")), StringUtils.toString(m.get("dictItemName")));
		}

		for (Map<String, Object> m : dataList) {
			m.put("parmTypeCn", parmTypeMap.get(StringUtils.toString(m.get("parmType"))));
		}

		Map<String, Object> mData = toMapResult(dataList, dataList.size());
		return ApiResult.of(reqId, ApiStatus.STATUS_200, "查询成功", request.getRequestURI(), mData);
	}

	@ApiOperation("删除参数")
	@ApiImplicitParams({@ApiImplicitParam(name = "parmId", value = "参数Id", required = true, dataType = "Long", paramType = "path")})
	@DeleteMapping("/{parmId}")
	@Function(module = "uums", code = "uums.parm.del", description = "删除参数")
	public ApiResult delParm(HttpServletRequest request, @PathVariable(required = false) BigInteger parmId) {
		String reqId = getReqId(request);
		if (StringUtils.isEmpty(parmId)) {
			return ApiResult.of(reqId, ApiStatus.STATUS_800, "参数Id不能为空", request.getRequestURI(), null);
		}

		parmService.delParm(parmId);
		return ApiResult.of(reqId, ApiStatus.STATUS_200, "删除成功", request.getRequestURI(), null);
	}

	@ApiOperation("获取参数")
	@GetMapping("/{parmId}")
	@Function(module = "uums", code = "uums.parm.view", description = "获取参数")
	public ApiResult getParm(HttpServletRequest request, @PathVariable(required = false) BigInteger parmId) {
		String reqId = getReqId(request);
		if (StringUtils.isEmpty(parmId)) {
			return ApiResult.of(reqId, ApiStatus.STATUS_800, "参数Id不能为空", request.getRequestURI(), null);
		}

		Map<String, Object> dMap = parmService.getParm(parmId);
		Map<String, Object> dictMap = dictService.getDictByCode("uums-parm-type");
		dMap.put("itemList", dictMap.get("itemList"));
		return ApiResult.of(reqId, ApiStatus.STATUS_200, "成功", request.getRequestURI(), dMap);
	}

	private Map<String, Object> parseParm(HttpServletRequest request) {
		String parmId = request.getParameter("parmId");
		String parmType = request.getParameter("parmType");
		String parmKey = request.getParameter("parmKey");
		String parmValue = request.getParameter("parmValue");
		String parmDesc = request.getParameter("parmDesc");

		Map<String, Object> dMap = new HashMap<String, Object>();
		dMap.put("parmId", StringUtils.isEmpty(parmId) ? null : new BigInteger(parmId));
		dMap.put("parmType", parmType);
		dMap.put("parmKey", parmKey);
		dMap.put("parmValue", parmValue);
		dMap.put("parmDesc", parmDesc);
		return dMap;
	}

	@ApiOperation("新增参数")
	@PostMapping
	@Function(module = "uums", code = "uums.parm.add", description = "新增参数")
	public ApiResult addParm(HttpServletRequest request) {
		String reqId = getReqId(request);

		Map<String, Object> dMap = parseParm(request);

		if (StringUtils.isEmpty(dMap.get("parmType"))) {
			return ApiResult.of(reqId, ApiStatus.STATUS_800, "参数类型不能为空", request.getRequestURI(), null);
		}
		if (StringUtils.isEmpty(dMap.get("parmKey"))) {
			return ApiResult.of(reqId, ApiStatus.STATUS_800, "参数key不能为空", request.getRequestURI(), null);
		}
		if (StringUtils.isEmpty(dMap.get("parmValue"))) {
			return ApiResult.of(reqId, ApiStatus.STATUS_800, "参数值不能为空", request.getRequestURI(), null);
		}

		parmService.addParm(dMap);
		return ApiResult.of(reqId, ApiStatus.STATUS_200, "新增成功", request.getRequestURI(), null);
	}

	@ApiOperation("编辑参数")
	@PutMapping
	@Function(module = "uums", code = "uums.parm.edit", description = "编辑参数")
	public ApiResult editParm(HttpServletRequest request) {
		String reqId = getReqId(request);
		Map<String, Object> dMap = parseParm(request);
		if (StringUtils.isEmpty(dMap.get("parmId"))) {
			return ApiResult.of(reqId, ApiStatus.STATUS_800, "参数Id不能为空", request.getRequestURI(), null);
		}
		if (StringUtils.isEmpty(dMap.get("parmType"))) {
			return ApiResult.of(reqId, ApiStatus.STATUS_800, "参数类型不能为空", request.getRequestURI(), null);
		}
		if (StringUtils.isEmpty(dMap.get("parmKey"))) {
			return ApiResult.of(reqId, ApiStatus.STATUS_800, "参数key不能为空", request.getRequestURI(), null);
		}
		if (StringUtils.isEmpty(dMap.get("parmValue"))) {
			return ApiResult.of(reqId, ApiStatus.STATUS_800, "参数值不能为空", request.getRequestURI(), null);
		}

		parmService.editParm(dMap);
		return ApiResult.of(reqId, ApiStatus.STATUS_200, "修改成功", request.getRequestURI(), null);
	}

}
