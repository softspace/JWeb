package com.hm.uums.controller;

import java.math.BigInteger;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hm.common.annotation.Function;
import com.hm.common.api.ApiResult;
import com.hm.common.api.ApiStatus;
import com.hm.common.utils.StringUtils;
import com.hm.uums.service.OrgService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;

/**
 * @author hwg
 *
 *         2020年12月18日
 */
@Api(tags = "4.部门接口")
@RestController
@RequestMapping("/org")
public class OrgController extends BaseController {
	Logger logger = LoggerFactory.getLogger(OrgController.class);

	@Autowired
	private OrgService orgService;

	@ApiOperation("部门查询")
	@ApiImplicitParams({@ApiImplicitParam(name = "name", value = "部门名称", required = false, dataType = "String", paramType = "query"), @ApiImplicitParam(name = "status", value = "状态", required = false, dataType = "Integer", paramType = "query")})
	@GetMapping("/list")
	@Function(module = "uums", code = "uums.org.view", description = "部门查询")
	public ApiResult list(HttpServletRequest request) {
		String reqId = getReqId(request);
		Map<String, Object> params = getEmptyParam(request);
		params.put("name", request.getParameter("name"));
		params.put("status", request.getParameter("status"));

		List<Map<String, Object>> dataList = orgService.list(params);
		Map<String, Object> mData = toMapResult(dataList, dataList.size());
		return ApiResult.of(reqId, ApiStatus.STATUS_200, "查询成功", request.getRequestURI(), mData);
	}

	@ApiOperation("设置部门状态")
	@ApiImplicitParams({@ApiImplicitParam(name = "userId", value = "部门Id", required = true, dataType = "BigInteger", paramType = "query"), @ApiImplicitParam(name = "status", value = "部门状态", required = true, dataType = "Integer", paramType = "query")})
	@PutMapping("/status")
	@Function(module = "uums", code = "uums.org.edit", description = "设置部门状态")
	public ApiResult editOrgStatus(HttpServletRequest request) {
		String reqId = getReqId(request);

		String orgId = request.getParameter("orgId");
		String status = request.getParameter("status");

		if (StringUtils.isEmpty(orgId)) {
			return ApiResult.of(reqId, ApiStatus.STATUS_800, "部门Id不能为空", request.getRequestURI(), null);
		}

		if (StringUtils.isEmpty(status)) {
			return ApiResult.of(reqId, ApiStatus.STATUS_800, "部门状态（status不能为空", request.getRequestURI(), null);
		}

		int rsCode = orgService.editOrgStatus(new BigInteger(orgId), Integer.parseInt(status));
		if (rsCode == 1) {
			return ApiResult.of(reqId, ApiStatus.STATUS_1100, request.getRequestURI());
		}
		return ApiResult.of(reqId, ApiStatus.STATUS_200, "设置成功", request.getRequestURI(), null);
	}

	@ApiOperation("删除部门")
	@ApiImplicitParams({@ApiImplicitParam(name = "orgId", value = "部门Id", required = true, dataType = "Long", paramType = "path")})
	@DeleteMapping("/{orgId}")
	@Function(module = "uums", code = "uums.org.del", description = "删除部门")
	public ApiResult delOrg(HttpServletRequest request, @PathVariable(required = false) BigInteger orgId) {
		String reqId = getReqId(request);
		if (StringUtils.isEmpty(orgId)) {
			return ApiResult.of(reqId, ApiStatus.STATUS_800, "部门Id不能为空", request.getRequestURI(), null);
		}

		int rsCode = orgService.delOrg(orgId);
		if (rsCode == 1) {
			return ApiResult.of(reqId, ApiStatus.STATUS_1101, request.getRequestURI());
		}
		return ApiResult.of(reqId, ApiStatus.STATUS_200, "删除成功", request.getRequestURI(), null);
	}

	@ApiOperation("获取部门")
	@GetMapping("/{orgId}")
	@Function(module = "uums", code = "uums.org.view", description = "获取部门")
	public ApiResult getOrg(HttpServletRequest request, @PathVariable(required = false) BigInteger orgId) {
		String reqId = getReqId(request);
		if (StringUtils.isEmpty(orgId)) {
			return ApiResult.of(reqId, ApiStatus.STATUS_800, "部门Id不能为空", request.getRequestURI(), null);
		}

		Map<String, Object> dMap = orgService.getOrg(orgId);
		if (StringUtils.isNotEmpty(dMap.get("orgPId"))) {//如果有上级部门，则获取上级部门的名字
			Map<String, Object> pOrgMap = orgService.getOrg((BigInteger) dMap.get("orgPId"));
			dMap.put("orgPName", pOrgMap.get("name"));
		}
		return ApiResult.of(reqId, ApiStatus.STATUS_200, "成功", request.getRequestURI(), dMap);
	}

	private Map<String, Object> parseOrg(HttpServletRequest request) {
		String orgId = request.getParameter("orgId");
		String orgPId = request.getParameter("orgPId");
		String name = request.getParameter("name");
		String desc = request.getParameter("desc");
		String order = request.getParameter("order");

		Map<String, Object> dMap = new HashMap<String, Object>();
		dMap.put("orgId", StringUtils.isEmpty(orgId) ? null : new BigInteger(orgId));
		dMap.put("orgPId", StringUtils.isEmpty(orgPId) ? null : new BigInteger(orgPId));
		dMap.put("order", StringUtils.isEmpty(order) ? 0 : Integer.parseInt(order));
		dMap.put("name", name);
		dMap.put("desc", desc);
		dMap.put("fullPId", null);
		return dMap;
	}

	@ApiOperation("新增部门")
	@PostMapping
	@Function(module = "uums", code = "uums.org.add", description = "新增部门")
	public ApiResult addOrg(HttpServletRequest request) {
		String reqId = getReqId(request);

		Map<String, Object> dMap = parseOrg(request);

		if (StringUtils.isEmpty(dMap.get("name"))) {
			return ApiResult.of(reqId, ApiStatus.STATUS_800, "部门名称不能为空", request.getRequestURI(), null);
		}

		orgService.addOrg(dMap);
		return ApiResult.of(reqId, ApiStatus.STATUS_200, "新增成功", request.getRequestURI(), null);
	}

	@ApiOperation("编辑部门")
	@PutMapping
	@Function(module = "uums", code = "uums.org.edit", description = "新增部门")
	public ApiResult editOrg(HttpServletRequest request) {
		String reqId = getReqId(request);
		Map<String, Object> dMap = parseOrg(request);
		if (StringUtils.isEmpty(dMap.get("orgId"))) {
			return ApiResult.of(reqId, ApiStatus.STATUS_800, "部门Id不能为空", request.getRequestURI(), null);
		}
		if (StringUtils.isEmpty(dMap.get("name"))) {
			return ApiResult.of(reqId, ApiStatus.STATUS_800, "部门名称不能为空", request.getRequestURI(), null);
		}

		if (StringUtils.isNotEmpty(dMap.get("orgPId"))) {
			Map<String, Object> pOrgMap = orgService.getOrg((BigInteger) dMap.get("orgPId"));
			dMap.put("fullPId", StringUtils.ifEmptyReturn(pOrgMap.get("fullPId"), "") + "-" + dMap.get("orgPId"));
		}

		orgService.editOrg(dMap);
		return ApiResult.of(reqId, ApiStatus.STATUS_200, "修改成功", request.getRequestURI(), null);
	}

}
