package com.hm.uums.controller;

import java.math.BigInteger;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.hm.common.Constant;
import com.hm.common.utils.PageUtils;

/**
 * @author hwg
 *
 *         2020年12月18日 公共Controller, 封装一些Controller中一些常用的处理逻辑
 */
public class BaseController {
	/**
	 * 获取请求消息头Id，此Id由是网关中的serverWebExchange.getRequest().getId()
	 * 
	 * @param request
	 * @return
	 */
	protected String getReqId(HttpServletRequest request) {
		return request.getHeader(Constant.REQUEST_ID);
	}

	/**
	 * 获取请求消息头token
	 * 
	 * @param request
	 * @return
	 */
	protected String getToken(HttpServletRequest request) {
		return request.getHeader(Constant.token);
	}

	/**
	 * 获取请求消息头userId，此userId是网关处验证token有效侯，获取对应用户Id添加到请求消息头的
	 * 
	 * @param request
	 * @return
	 */
	protected BigInteger getUserId(HttpServletRequest request) {
		return new BigInteger(request.getHeader(Constant.USER_ID));
	}

	/**
	 * 得到一个Map，无数据
	 * 
	 * @param request
	 * @return
	 */
	protected Map<String, Object> getEmptyParam(HttpServletRequest request) {
		return new HashMap<String, Object>();
	}

	/**
	 * 解析请求中的通用参数，主要分页中的参数
	 * 
	 * @param request
	 * @return
	 */
	protected Map<String, Object> getPageParam(HttpServletRequest request) {
		String cPage = request.getParameter("pageNo");//当前页
		String pSize = request.getParameter("pageSize");//每页大小
		return new HashMap<String, Object>() {
			{
				put("pageSize", PageUtils.getPageSize(pSize));//校验并转换为int
				put("offset", PageUtils.getOffset(cPage, pSize));//计算分页（limit offset， pageSize）中的offset
				put("sortField", request.getParameter("sortField"));//排序字段
				put("sortType", request.getParameter("sortType"));//排序类型asc或desc
			}
		};
	}

	/**
	 * 用于分页查询返回结果封装，把结果封装到Map对象中
	 * 
	 * @param dataList
	 * @param count
	 * @return
	 */
	protected Map<String, Object> toMapResult(List<Map<String, Object>> dataList, long count) {
		Map<String, Object> dMap = toMapResult(dataList);
		dMap.put("count", count);
		return dMap;
	}

	/**
	 * 用于分页查询返回结果封装，把结果封装到Map对象中
	 * 
	 * @param dataList
	 * @return
	 */
	protected Map<String, Object> toMapResult(List<Map<String, Object>> dataList) {
		return new HashMap<String, Object>() {
			{
				put("dataList", dataList);
			}
		};
	}

}
