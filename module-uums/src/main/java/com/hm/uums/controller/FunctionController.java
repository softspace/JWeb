package com.hm.uums.controller;

import java.math.BigInteger;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hm.common.annotation.Function;
import com.hm.common.api.ApiResult;
import com.hm.common.api.ApiStatus;
import com.hm.common.utils.StringUtils;
import com.hm.uums.service.FunctionService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;

/**
 * @author hwg
 *
 *         2020年12月18日
 */
@Api(tags = "5.功能权限接口")
@RestController
@RequestMapping("/function")
public class FunctionController extends BaseController {
	Logger logger = LoggerFactory.getLogger(FunctionController.class);

	@Autowired
	private FunctionService functionService;

	@ApiOperation("功能权限查询")
	@ApiImplicitParams({@ApiImplicitParam(name = "name", value = "功能权限名称", required = false, dataType = "String", paramType = "query"), @ApiImplicitParam(name = "status", value = "状态", required = false, dataType = "Integer", paramType = "query")})
	@GetMapping("/list")
	@Function(module = "uums", code = "uums.func.view", description = "功能权限查询")
	public ApiResult list(HttpServletRequest request) {
		String reqId = getReqId(request);
		Map<String, Object> params = getEmptyParam(request);
		params.put("name", request.getParameter("name"));
		params.put("status", request.getParameter("status"));

		List<Map<String, Object>> dataList = functionService.list(params);
		Map<String, Object> mData = toMapResult(dataList, dataList.size());
		return ApiResult.of(reqId, ApiStatus.STATUS_200, "查询成功", request.getRequestURI(), mData);
	}

	@ApiOperation("设置功能权限状态")
	@ApiImplicitParams({@ApiImplicitParam(name = "userId", value = "功能权限Id", required = true, dataType = "BigInteger", paramType = "query"), @ApiImplicitParam(name = "status", value = "功能权限状态", required = true, dataType = "Integer", paramType = "query")})
	@PutMapping("/status")
	@Function(module = "uums", code = "uums.func.edit", description = "设置功能权限状态")
	public ApiResult editFunctionStatus(HttpServletRequest request) {
		String reqId = getReqId(request);

		String functionId = request.getParameter("functionId");
		String status = request.getParameter("status");

		if (StringUtils.isEmpty(functionId)) {
			return ApiResult.of(reqId, ApiStatus.STATUS_800, "功能权限Id不能为空", request.getRequestURI(), null);
		}

		if (StringUtils.isEmpty(status)) {
			return ApiResult.of(reqId, ApiStatus.STATUS_800, "功能权限状态（status不能为空", request.getRequestURI(), null);
		}

		int rsCode = functionService.editFunctionStatus(new BigInteger(functionId), Integer.parseInt(status));
		if (rsCode == 1) {
			return ApiResult.of(reqId, ApiStatus.STATUS_1120, request.getRequestURI());
		}
		return ApiResult.of(reqId, ApiStatus.STATUS_200, "设置成功", request.getRequestURI(), null);
	}

	@ApiOperation("删除功能权限")
	@ApiImplicitParams({@ApiImplicitParam(name = "functionId", value = "功能权限Id", required = true, dataType = "Long", paramType = "path")})
	@DeleteMapping("/{functionId}")
	@Function(module = "uums", code = "uums.func.del", description = "删除功能权限")
	public ApiResult delFunction(HttpServletRequest request, @PathVariable(required = false) BigInteger functionId) {
		String reqId = getReqId(request);
		if (StringUtils.isEmpty(functionId)) {
			return ApiResult.of(reqId, ApiStatus.STATUS_800, "功能权限Id不能为空", request.getRequestURI(), null);
		}

		int rsCode = functionService.delFunction(functionId);
		if (rsCode == 1) {
			return ApiResult.of(reqId, ApiStatus.STATUS_1121, request.getRequestURI());
		}
		return ApiResult.of(reqId, ApiStatus.STATUS_200, "删除成功", request.getRequestURI(), null);
	}

	@ApiOperation("获取功能权限")
	@GetMapping("/{functionId}")
	@Function(module = "uums", code = "uums.func.view", description = "获取功能权限")
	public ApiResult getFunction(HttpServletRequest request, @PathVariable(required = false) BigInteger functionId) {
		String reqId = getReqId(request);
		if (StringUtils.isEmpty(functionId)) {
			return ApiResult.of(reqId, ApiStatus.STATUS_800, "功能权限Id不能为空", request.getRequestURI(), null);
		}

		Map<String, Object> dMap = functionService.getFunction(functionId);
		if ((BigInteger) dMap.get("functionPId") != null) {//如果有上级功能权限，则获取上级功能权限的名字
			Map<String, Object> pFunctionMap = functionService.getFunction((BigInteger) dMap.get("functionPId"));
			dMap.put("functionPName", pFunctionMap.get("name"));
		}
		return ApiResult.of(reqId, ApiStatus.STATUS_200, "成功", request.getRequestURI(), dMap);
	}

	private Map<String, Object> parseFunction(HttpServletRequest request) {
		String functionId = request.getParameter("functionId");
		String functionPId = request.getParameter("functionPId");
		String name = request.getParameter("name");
		String type = request.getParameter("type");
		String code = request.getParameter("code");
		String url = request.getParameter("url");
		String icon = request.getParameter("icon");
		String desc = request.getParameter("desc");
		String order = request.getParameter("order");

		Map<String, Object> dMap = new HashMap<String, Object>();
		dMap.put("functionId", StringUtils.isEmpty(functionId) ? null : new BigInteger(functionId));
		dMap.put("functionPId", StringUtils.isEmpty(functionPId) ? null : new BigInteger(functionPId));
		dMap.put("order", StringUtils.isEmpty(order) ? 0 : Integer.parseInt(order));
		dMap.put("type", Integer.parseInt(type));
		dMap.put("name", name);
		dMap.put("code", code);
		dMap.put("url", url);
		dMap.put("icon", icon);
		dMap.put("desc", desc);
		dMap.put("fullPId", null);
		return dMap;
	}

	@ApiOperation("新增功能权限")
	@PostMapping
	@Function(module = "uums", code = "uums.func.add", description = "新增功能权限")
	public ApiResult addFunction(HttpServletRequest request) {
		String reqId = getReqId(request);

		Map<String, Object> dMap = parseFunction(request);

		if (StringUtils.isEmpty(dMap.get("name"))) {
			return ApiResult.of(reqId, ApiStatus.STATUS_800, "功能权限名称不能为空", request.getRequestURI(), null);
		}

		functionService.addFunction(dMap);
		return ApiResult.of(reqId, ApiStatus.STATUS_200, "新增成功", request.getRequestURI(), null);
	}

	@ApiOperation("编辑功能权限")
	@PutMapping
	@Function(module = "uums", code = "uums.func.edit", description = "编辑功能权限")
	public ApiResult editFunction(HttpServletRequest request) {
		String reqId = getReqId(request);
		Map<String, Object> dMap = parseFunction(request);
		if (StringUtils.isEmpty(dMap.get("functionId"))) {
			return ApiResult.of(reqId, ApiStatus.STATUS_800, "功能权限Id不能为空", request.getRequestURI(), null);
		}
		if (StringUtils.isEmpty(dMap.get("name"))) {
			return ApiResult.of(reqId, ApiStatus.STATUS_800, "功能权限名称不能为空", request.getRequestURI(), null);
		}

		functionService.editFunction(dMap);
		return ApiResult.of(reqId, ApiStatus.STATUS_200, "修改成功", request.getRequestURI(), null);
	}

}
