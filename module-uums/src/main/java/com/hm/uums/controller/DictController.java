package com.hm.uums.controller;

import java.math.BigInteger;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hm.common.annotation.Function;
import com.hm.common.api.ApiResult;
import com.hm.common.api.ApiStatus;
import com.hm.common.utils.StringUtils;
import com.hm.uums.service.DictService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;

/**
 * @author hwg
 *
 *         2020年12月18日
 */
@Api(tags = "6.字典接口")
@RestController
@RequestMapping("/dict")
public class DictController extends BaseController {

	Logger logger = LoggerFactory.getLogger(DictController.class);

	@Autowired
	private DictService dictService;

	@ApiOperation("字典查询")
	@ApiImplicitParams({@ApiImplicitParam(name = "name", value = "字典名称", required = false, dataType = "String", paramType = "query"), @ApiImplicitParam(name = "status", value = "状态", required = false, dataType = "Integer", paramType = "query")})
	@GetMapping("/list")
	@Function(module = "uums", code = "uums.dict.view", description = "字典查询")
	public ApiResult list(HttpServletRequest request) {
		String reqId = getReqId(request);
		Map<String, Object> params = getPageParam(request);
		params.put("dictName", request.getParameter("dictName"));
		params.put("dictCode", request.getParameter("dictCode"));

		List<Map<String, Object>> dataList = dictService.list(params);
		Map<String, Object> mData = toMapResult(dataList, dataList.size());
		return ApiResult.of(reqId, ApiStatus.STATUS_200, "查询成功", request.getRequestURI(), mData);
	}

	@ApiOperation("删除字典")
	@ApiImplicitParams({@ApiImplicitParam(name = "dictId", value = "字典Id", required = true, dataType = "Long", paramType = "path")})
	@DeleteMapping("/{dictId}")
	@Function(module = "uums", code = "uums.dict.del", description = "删除字典")
	public ApiResult delDict(HttpServletRequest request, @PathVariable(required = false) BigInteger dictId) {
		String reqId = getReqId(request);
		if (StringUtils.isEmpty(dictId)) {
			return ApiResult.of(reqId, ApiStatus.STATUS_800, "字典Id不能为空", request.getRequestURI(), null);
		}

		dictService.delDict(dictId);
		return ApiResult.of(reqId, ApiStatus.STATUS_200, "删除成功", request.getRequestURI(), null);
	}

	@ApiOperation("获取字典")
	@GetMapping("/{dictId}")
	@Function(module = "uums", code = "uums.dict.view", description = "获取字典")
	public ApiResult getDict(HttpServletRequest request, @PathVariable(required = false) BigInteger dictId) {
		String reqId = getReqId(request);
		if (StringUtils.isEmpty(dictId)) {
			return ApiResult.of(reqId, ApiStatus.STATUS_800, "字典Id不能为空", request.getRequestURI(), null);
		}

		Map<String, Object> dMap = dictService.getDict(dictId);
		return ApiResult.of(reqId, ApiStatus.STATUS_200, "成功", request.getRequestURI(), dMap);
	}

	@ApiOperation("获取字典")
	@GetMapping("/by/code/{code}")
	@Function(module = "uums", code = "uums.dict.view", description = "获取字典")
	public ApiResult getDictByCode(HttpServletRequest request, @PathVariable(required = false) String code) {
		String reqId = getReqId(request);
		if (StringUtils.isEmpty(code)) {
			return ApiResult.of(reqId, ApiStatus.STATUS_800, "字典编号不能为空", request.getRequestURI(), null);
		}

		Map<String, Object> dMap = dictService.getDictByCode(code);
		return ApiResult.of(reqId, ApiStatus.STATUS_200, "成功", request.getRequestURI(), dMap);
	}

	private Map<String, Object> parseDict(HttpServletRequest request) {
		String dictId = request.getParameter("dictId");
		String dictName = request.getParameter("dictName");
		String dictCode = request.getParameter("dictCode");

		Map<String, Object> dMap = new HashMap<String, Object>();
		dMap.put("dictId", StringUtils.isEmpty(dictId) ? null : new BigInteger(dictId));
		dMap.put("dictName", dictName);
		dMap.put("dictCode", dictCode);
		return dMap;
	}

	@ApiOperation("新增字典")
	@PostMapping
	@Function(module = "uums", code = "uums.dict.add", description = "新增字典")
	public ApiResult addDict(HttpServletRequest request) {
		String reqId = getReqId(request);

		Map<String, Object> dMap = parseDict(request);

		if (StringUtils.isEmpty(dMap.get("dictName"))) {
			return ApiResult.of(reqId, ApiStatus.STATUS_800, "字典名称不能为空", request.getRequestURI(), null);
		}
		if (StringUtils.isEmpty(dMap.get("dictCode"))) {
			return ApiResult.of(reqId, ApiStatus.STATUS_800, "字典编号不能为空", request.getRequestURI(), null);
		}

		int rsCode = dictService.addDict(dMap);
		if (rsCode == 1) {
			return ApiResult.of(reqId, ApiStatus.STATUS_999, "字典编号已经存在", request.getRequestURI(), null);
		}
		return ApiResult.of(reqId, ApiStatus.STATUS_200, "新增成功", request.getRequestURI(), null);
	}

	@ApiOperation("编辑字典")
	@PutMapping
	@Function(module = "uums", code = "uums.dict.edit", description = "编辑字典")
	public ApiResult editDict(HttpServletRequest request) {
		String reqId = getReqId(request);

		Map<String, Object> dMap = parseDict(request);

		if (StringUtils.isEmpty(dMap.get("dictId"))) {
			return ApiResult.of(reqId, ApiStatus.STATUS_800, "字典Id不能为空", request.getRequestURI(), null);
		}
		if (StringUtils.isEmpty(dMap.get("dictName"))) {
			return ApiResult.of(reqId, ApiStatus.STATUS_800, "字典名称不能为空", request.getRequestURI(), null);
		}
		if (StringUtils.isEmpty(dMap.get("dictCode"))) {
			return ApiResult.of(reqId, ApiStatus.STATUS_800, "字典编号不能为空", request.getRequestURI(), null);
		}

		dictService.editDict(dMap);
		return ApiResult.of(reqId, ApiStatus.STATUS_200, "修改成功", request.getRequestURI(), null);
	}

	///////////////////////////////////////////////////////////////////////
	@ApiOperation("字典值查询")
	@ApiImplicitParams({@ApiImplicitParam(name = "name", value = "字典值名称", required = false, dataType = "String", paramType = "query"), @ApiImplicitParam(name = "status", value = "状态", required = false, dataType = "Integer", paramType = "query")})
	@GetMapping("/item/list")
	@Function(module = "uums", code = "uums.dict.view", description = "字典值查询")
	public ApiResult itemList(HttpServletRequest request) {
		String reqId = getReqId(request);
		Map<String, Object> params = getEmptyParam(request);
		params.put("dictId", request.getParameter("dictId"));

		List<Map<String, Object>> dataList = dictService.itemlist(params);
		Map<String, Object> mData = toMapResult(dataList, dataList.size());
		return ApiResult.of(reqId, ApiStatus.STATUS_200, "查询成功", request.getRequestURI(), mData);
	}

	@ApiOperation("删除字典值")
	@ApiImplicitParams({@ApiImplicitParam(name = "dictId", value = "字典Id", required = true, dataType = "Long", paramType = "path")})
	@DeleteMapping("/item/{dictItemId}")
	@Function(module = "uums", code = "uums.dict.del", description = "删除字典值")
	public ApiResult delDictItem(HttpServletRequest request, @PathVariable(required = false) BigInteger dictItemId) {
		String reqId = getReqId(request);
		if (StringUtils.isEmpty(dictItemId)) {
			return ApiResult.of(reqId, ApiStatus.STATUS_800, "字典值Id不能为空", request.getRequestURI(), null);
		}

		dictService.delDictItem(dictItemId);
		return ApiResult.of(reqId, ApiStatus.STATUS_200, "删除成功", request.getRequestURI(), null);
	}

	@ApiOperation("获取字典值")
	@GetMapping("/item/{dictItemId}")
	@Function(module = "uums", code = "uums.dict.view", description = "获取字典值")
	public ApiResult getDictItem(HttpServletRequest request, @PathVariable(required = false) BigInteger dictItemId) {
		String reqId = getReqId(request);
		if (StringUtils.isEmpty(dictItemId)) {
			return ApiResult.of(reqId, ApiStatus.STATUS_800, "字典值Id不能为空", request.getRequestURI(), null);
		}

		Map<String, Object> dMap = dictService.getDictItem(dictItemId);
		return ApiResult.of(reqId, ApiStatus.STATUS_200, "成功", request.getRequestURI(), dMap);
	}

	private Map<String, Object> parseDictItem(HttpServletRequest request) {
		String dictItemId = request.getParameter("dictItemId");
		String dictId = request.getParameter("dictId");
		String dictItemCode = request.getParameter("dictItemCode");
		String dictItemName = request.getParameter("dictItemName");
		String dictItemDesc = request.getParameter("dictItemDesc");
		String order = request.getParameter("order");

		Map<String, Object> dMap = new HashMap<String, Object>();
		dMap.put("dictItemId", StringUtils.isEmpty(dictItemId) ? null : new BigInteger(dictItemId));
		dMap.put("order", StringUtils.isEmpty(order) ? 0 : Integer.parseInt(order));
		dMap.put("dictId", StringUtils.isEmpty(dictId) ? null : new BigInteger(dictId));
		dMap.put("dictItemCode", dictItemCode);
		dMap.put("dictItemName", dictItemName);
		dMap.put("dictItemDesc", dictItemDesc);
		return dMap;
	}

	@ApiOperation("新增字典值")
	@PostMapping("item")
	@Function(module = "uums", code = "uums.dict.add", description = "新增字典值")
	public ApiResult addDictItem(HttpServletRequest request) {
		String reqId = getReqId(request);

		Map<String, Object> dMap = parseDictItem(request);

		if (StringUtils.isEmpty(dMap.get("dictItemName"))) {
			return ApiResult.of(reqId, ApiStatus.STATUS_800, "字典值名称不能为空", request.getRequestURI(), null);
		}
		if (StringUtils.isEmpty(dMap.get("dictItemCode"))) {
			return ApiResult.of(reqId, ApiStatus.STATUS_800, "字典值不能为空", request.getRequestURI(), null);
		}
		if (StringUtils.isEmpty(dMap.get("dictId"))) {
			return ApiResult.of(reqId, ApiStatus.STATUS_800, "字典Id不能为空", request.getRequestURI(), null);
		}

		dictService.addDictItem(dMap);;
		return ApiResult.of(reqId, ApiStatus.STATUS_200, "新增成功", request.getRequestURI(), null);
	}

	@ApiOperation("编辑字典值")
	@PutMapping("item")
	@Function(module = "uums", code = "uums.dict.edit", description = "编辑字典值")
	public ApiResult editDictItem(HttpServletRequest request) {
		String reqId = getReqId(request);

		Map<String, Object> dMap = parseDictItem(request);

		if (StringUtils.isEmpty(dMap.get("dictItemId"))) {
			return ApiResult.of(reqId, ApiStatus.STATUS_800, "字典值Id不能为空", request.getRequestURI(), null);
		}
		if (StringUtils.isEmpty(dMap.get("dictItemName"))) {
			return ApiResult.of(reqId, ApiStatus.STATUS_800, "字典值名称不能为空", request.getRequestURI(), null);
		}
		if (StringUtils.isEmpty(dMap.get("dictItemCode"))) {
			return ApiResult.of(reqId, ApiStatus.STATUS_800, "字典值不能为空", request.getRequestURI(), null);
		}
		dictService.editDictItem(dMap);
		return ApiResult.of(reqId, ApiStatus.STATUS_200, "修改成功", request.getRequestURI(), null);
	}
}
