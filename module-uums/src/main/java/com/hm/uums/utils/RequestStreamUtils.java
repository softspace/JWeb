package com.hm.uums.utils;

import java.io.BufferedReader;
import java.io.InputStreamReader;

import javax.servlet.http.HttpServletRequest;

public class RequestStreamUtils {
	public static String readReqStream(HttpServletRequest request) throws Exception {
		BufferedReader reader = new BufferedReader(new InputStreamReader(request.getInputStream(), "UTF-8"));
		StringBuffer jsonBuffer = new StringBuffer();
		String line = "";
		while ((line = reader.readLine()) != null) {
			jsonBuffer.append(line);
		}
		return jsonBuffer.toString();
	}
}
