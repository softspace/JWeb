package com.hm.uums;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

import springfox.documentation.oas.annotations.EnableOpenApi;

/**
 * @author hwg
 *
 *         2020年12月18日
 */
@EnableOpenApi
@SpringBootApplication
@EnableDiscoveryClient
@MapperScan("com.hm.uums.mapper")
public class UUMSApplication extends SpringBootServletInitializer {
	public static void main(String[] args) {
		SpringApplication.run(UUMSApplication.class, args);
	}

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
		return builder.sources(UUMSApplication.class);
	}
}
