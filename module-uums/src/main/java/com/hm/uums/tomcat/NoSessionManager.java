/**
 * 
 */
package com.hm.uums.tomcat;

import java.io.IOException;

import org.apache.catalina.Lifecycle;
import org.apache.catalina.LifecycleException;
import org.apache.catalina.LifecycleState;
import org.apache.catalina.Session;
import org.apache.catalina.session.ManagerBase;
import org.apache.catalina.session.StandardManager;
import org.apache.tomcat.util.ExceptionUtils;

/**
 * @author hwg
 *
 *         2021年3月3日
 */
public class NoSessionManager extends ManagerBase implements Lifecycle {
	
	@Override
	protected synchronized void startInternal() throws LifecycleException {
		super.startInternal();
		try {
			load();
			StandardManager X = null;
		} catch (Throwable t) {
			ExceptionUtils.handleThrowable(t);
			t.printStackTrace();
		}
		setState(LifecycleState.STARTING);
	}

	@Override
	protected synchronized void stopInternal() throws LifecycleException {
		setState(LifecycleState.STOPPING);
		try {
			unload();
		} catch (Throwable t) {
			ExceptionUtils.handleThrowable(t);
			t.printStackTrace();
		}
		super.stopInternal();
	}

	@Override
	public void load() throws ClassNotFoundException, IOException {
	}

	@Override
	public void unload() throws IOException {
	}

	@Override
	public Session createSession(String sessionId) {
		return null;
	}

	@Override
	public Session createEmptySession() {
		return null;
	}

	@Override
	public void add(Session session) {
	}
	
	@Override
	public Session findSession(String id) throws IOException {
		return null;
	}
	
	@Override
	public Session[] findSessions() {
		return null;
	}
	
	@Override
	public void processExpires() {
	}
}