/**
 * 
 */
package com.hm.uums.tomcat;

import org.apache.catalina.Context;
import org.springframework.boot.web.embedded.tomcat.TomcatServletWebServerFactory;
import org.springframework.stereotype.Component;

/**
 * @author hwg
 *
 *         2021年3月3日
 */
@Component
public class CustomTomcatServletWebServerFactory extends TomcatServletWebServerFactory {
	@Override
	protected void postProcessContext(Context context) {
		context.setManager(new NoSessionManager());
	}
}
