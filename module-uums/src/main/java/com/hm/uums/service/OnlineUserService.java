package com.hm.uums.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hm.uums.mapper.OnlineUserMapper;

/**
 * @author hwg
 *
 *         2020年12月18日
 */
@Service
public class OnlineUserService {

	@Autowired
	private OnlineUserMapper onlineUserMapper;

	public List<Map<String, Object>> list(Map<String, Object> params) {
		return onlineUserMapper.list(params);
	}

}
