package com.hm.uums.service;

import java.math.BigInteger;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.hm.common.utils.DateUtils;
import com.hm.common.utils.StringUtils;
import com.hm.uums.mapper.CommonMapper;

/**
 * @author hwg
 *
 *         2020年12月18日 
 *         公共业务Service
 */
@Service
public class CommonService {

	@Autowired
	private CommonMapper commonMapper;

	/**
	 * 根据用户名和密码查找用户
	 * 
	 * @param username
	 * @param password
	 * @return
	 */
	public Map<String, Object> findUsers(String username, String password) {
		return commonMapper.findUser(username, password);
	}

	//添加token，如果不允许同一个用户在多个客户端同时登录在线操作，则删除之前登录的token信息
	@Transactional
	public void addToken(Map<String, Object> dMap) {
		dMap.put("cdate", DateUtils.fnow());//当前日期
		dMap.put("refreshTime", DateUtils.now().getTime());//当前时间

		int allowMultipleClientOnline = (int) dMap.get("allowMultipleClientOnline");

		//同一个用户是否允许多个客户端同时登录在线操作，0：不允许，1：允许！未配置或配置错误（非0和1），则默认为0
		if (allowMultipleClientOnline == 0) {
			//不允许多个客户端同时在线，删除同一个账号之前登录的信息
			BigInteger userId = (BigInteger) dMap.get("userId");
			commonMapper.backupSameUserTokenToHis(userId);
			commonMapper.delSameUserToken(userId);
		}

		//添加当前登录的用户信息
		commonMapper.addToken(dMap);
	}

	/**
	 * 删除token，删除之前把token备份到历史表中
	 * 
	 * @param token
	 */
	@Transactional
	public void delToken(String token) {
		commonMapper.backupTokenToHis(token);
		commonMapper.delToken(token);
	}

	/**
	 * 获取系统信息，系统名称、版本号等
	 * 
	 * @param sysCode
	 * @return
	 */
	public Map<String, Object> getSysInfo(int sysCode) {
		return commonMapper.getSysInfo(sysCode);
	}

	/**
	 * 验证token
	 * 
	 * @param token
	 */
	public boolean validateToken(String token) {
		Map<String, Object> tokenMap = commonMapper.findToken(token);
		return StringUtils.isNotEmpty(tokenMap);
	}
}
