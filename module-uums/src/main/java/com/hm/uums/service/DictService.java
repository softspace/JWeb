package com.hm.uums.service;

import java.math.BigInteger;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.hm.common.utils.DateUtils;
import com.hm.common.utils.StringUtils;
import com.hm.uums.mapper.DictMapper;

/**
 * @author hwg
 *
 *         2020年12月18日
 */
@Service
public class DictService {

	@Autowired
	private DictMapper dictMapper;

	public Map<String, Object> getDict(BigInteger dictId) {
		return dictMapper.getDict(dictId);
	}

	@Transactional
	public int addDict(Map<String, Object> dMap) {
		String dictCode = (String) dMap.get("dictCode");

		Map<String, Object> m = dictMapper.getDictByCode(dictCode);
		if (StringUtils.isNotEmpty(m)) {
			return 1;
		}

		dMap.put("createtime", DateUtils.fnow());
		dMap.put("updatetime", DateUtils.fnow());
		dictMapper.addDict(dMap);
		return 0;
	}

	@Transactional
	public void editDict(Map<String, Object> dMap) {
		dMap.put("updatetime", DateUtils.fnow());
		dictMapper.editDict(dMap);
	}

	@Transactional
	public void delDict(BigInteger dictId) {
		dictMapper.delDictItemByDictId(dictId);
		dictMapper.delDict(dictId);
	}

	public List<Map<String, Object>> list(Map<String, Object> params) {
		return dictMapper.list(params);
	}

	public Map<String, Object> getDictByCode(String key) {
		Map<String, Object> dMap = dictMapper.getDictByCode(key);
		List<Map<String, Object>> itemList = dictMapper.itemlist(new HashMap<String, Object>() {
			{
				put("dictId", dMap.get("dictId"));
			}
		});
		dMap.put("itemList", itemList);
		return dMap;
	}

	@Transactional
	public void addDictItem(Map<String, Object> dMap) {
		dMap.put("createtime", DateUtils.fnow());
		dMap.put("updatetime", DateUtils.fnow());
		dictMapper.addDictItem(dMap);
	}

	@Transactional
	public void editDictItem(Map<String, Object> dMap) {
		dMap.put("updatetime", DateUtils.fnow());
		dictMapper.editDictItem(dMap);
	}

	public Map<String, Object> getDictItem(BigInteger dictItemId) {
		return dictMapper.getDictItem(dictItemId);
	}

	public List<Map<String, Object>> itemlist(Map<String, Object> params) {
		return dictMapper.itemlist(params);
	}

	@Transactional
	public void delDictItem(BigInteger dictItemId) {
		dictMapper.delDictItem(dictItemId);
	}

}
