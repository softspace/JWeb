package com.hm.uums.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hm.uums.mapper.LogMapper;

/**
 * @author hwg
 *
 *         2020年12月18日
 */
@Service
public class LogService {

	@Autowired
	private LogMapper logMapper;

	public List<Map<String, Object>> list(Map<String, Object> params) {
		return logMapper.list(params);
	}
	
	public long count(Map<String, Object> params) {
		return logMapper.count(params);
	}

}
