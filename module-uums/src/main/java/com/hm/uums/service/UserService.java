package com.hm.uums.service;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.hm.common.utils.DateUtils;
import com.hm.common.utils.StringUtils;
import com.hm.uums.mapper.UserMapper;

/**
 * @author hwg
 *
 *         2020年12月18日
 */
@Service
public class UserService {

	@Autowired
	private UserMapper userMapper;

	@Transactional
	public void addUser(Map<String, Object> dMap) {
		dMap.put("createtime", DateUtils.fnow());
		dMap.put("updatetime", DateUtils.fnow());
		userMapper.addUser(dMap);

		BigInteger userId = (BigInteger) dMap.get("userId");

		userMapper.ClearUserOrg(userId);
		userMapper.ClearUserRole(userId);

		userMapper.editUserOrg(userId, (Set<BigInteger>) dMap.get("orgSet"));
		userMapper.editUserRole(userId, (Set<BigInteger>) dMap.get("roleSet"));
	}

	@Transactional
	public void editUser(Map<String, Object> dMap) {
		dMap.put("updatetime", DateUtils.fnow());
		userMapper.editUser(dMap);

		BigInteger userId = (BigInteger) dMap.get("userId");

		userMapper.ClearUserOrg(userId);
		userMapper.ClearUserRole(userId);

		userMapper.editUserOrg(userId, (Set<BigInteger>) dMap.get("orgSet"));
		userMapper.editUserRole(userId, (Set<BigInteger>) dMap.get("roleSet"));
	}

	@Transactional
	public void editUserStatus(BigInteger userId, int status) {
		userMapper.editUserStatus(userId, status, DateUtils.fnow());
	}

	@Transactional
	public void delUser(BigInteger userId) {
		userMapper.delUser(userId);
	}

	public List<Map<String, Object>> list(Map<String, Object> params) {
		return userMapper.list(params);
	}

	public long count(Map<String, Object> params) {
		return userMapper.count(params);
	}

	public Map<String, Object> getUser(BigInteger userId) {
		return userMapper.getUser(userId);
	}

	public List<Map<String, Object>> getOrgByUser(BigInteger userId) {
		return userMapper.getOrgByUser(userId);
	}

	public List<Map<String, Object>> getRoleByUser(BigInteger userId) {
		return userMapper.getRoleByUser(userId);
	}

	@Transactional
	public void modifyPassword(BigInteger userId, String password) {
		userMapper.modifyPassword(userId, password, DateUtils.fnow());
	}

	public boolean verifyPassword(BigInteger userId, String password) {
		String pwd = userMapper.getPassword(userId);
		return StringUtils.toString(pwd).equals(password);
	}

	public Map<String, Object> getUserInfo(BigInteger userId) {
		Map<String, Object> mMap = new HashMap<String, Object>();
		Map<String, Object> userMap = userMapper.getUser(userId);
		List<Map<String, Object>> orgList = userMapper.getOrgByUser(userId);
		List<Map<String, Object>> roleList = userMapper.getRoleByUser(userId);
		List<Map<String, Object>> functionList = userMapper.getFunctionByUser(userId);
		List<Map<String, Object>> allFunctionList = userMapper.getAllFunction();
		List<Map<String, Object>> functions = new ArrayList<Map<String, Object>>();

		String fullPId = "";
		for (Map<String, Object> func0 : allFunctionList) {
			for (Map<String, Object> func1 : functionList) {
				if (((BigInteger) func0.get("functionId")).compareTo((BigInteger) func1.get("functionId")) == 0) {
					functions.add(func0);
					break;
				}
				fullPId = StringUtils.toString(func1.get("fullPId"));

				if (fullPId.contains("-" + func0.get("functionId"))) {
					functions.add(func0);
					break;
				}
			}
		}

		mMap.put("user", userMap);
		mMap.put("orgList", orgList);
		mMap.put("roleList", roleList);
		mMap.put("functionList", functions);
		return mMap;
	}

	public Set<String> getFunctionCodeByUser(BigInteger userId) {
		List<Map<String, Object>> functionList = userMapper.getFunctionCodeByUser(userId);
		Set<String> functionSet = new HashSet<String>();
		for (Map<String, Object> func : functionList) {
			functionSet.add(StringUtils.toString(func.get("code")).toLowerCase());
		}
		return functionSet;
	}
}
