package com.hm.uums.service;

import java.math.BigInteger;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.hm.common.utils.DateUtils;
import com.hm.common.utils.StringUtils;
import com.hm.uums.mapper.ParmMapper;

/**
 * @author hwg
 *
 *         2020年12月18日
 */
@Service
public class ParmService {

	@Autowired
	private ParmMapper parmMapper;

	public Map<String, Object> getParm(BigInteger parmId) {
		return parmMapper.getParm(parmId);
	}

	@Transactional
	public void addParm(Map<String, Object> dMap) {
		dMap.put("createtime", DateUtils.fnow());
		dMap.put("updatetime", DateUtils.fnow());
		parmMapper.addParm(dMap);
	}

	@Transactional
	public void editParm(Map<String, Object> dMap) {
		dMap.put("updatetime", DateUtils.fnow());
		parmMapper.editParm(dMap);
	}

	@Transactional
	public void delParm(BigInteger parmId) {
		parmMapper.delParm(parmId);
	}

	public List<Map<String, Object>> list(Map<String, Object> params) {
		return parmMapper.list(params);
	}

	public Map<String, Object> getParmByKey(String key) {
		return parmMapper.getParmByKey(key);
	}

	public String getParmValueByKey(String key) {
		Map<String, Object> m = parmMapper.getParmByKey(key);
		return m == null ? "" : StringUtils.toString(m.get("parmValue"));
	}

}
