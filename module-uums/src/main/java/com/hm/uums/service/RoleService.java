package com.hm.uums.service;

import java.math.BigInteger;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.hm.common.utils.DateUtils;
import com.hm.common.utils.StringUtils;
import com.hm.uums.mapper.RoleMapper;

/**
 * @author hwg
 *
 *         2020年12月18日
 */
@Service
public class RoleService {

	@Autowired
	private RoleMapper roleMapper;

	public Map<String, Object> getRole(BigInteger roleId) {
		return roleMapper.getRole(roleId);
	}

	@Transactional
	public void addRole(Map<String, Object> dMap) {
		dMap.put("createtime", DateUtils.fnow());
		dMap.put("updatetime", DateUtils.fnow());
		roleMapper.addRole(dMap);
	}

	@Transactional
	public void editRole(Map<String, Object> dMap) {
		dMap.put("updatetime", DateUtils.fnow());
		roleMapper.editRole(dMap);
	}

	@Transactional
	public void editRoleStatus(BigInteger roleId, int status) {
		roleMapper.editRoleStatus(roleId, status, DateUtils.fnow());
	}

	@Transactional
	public void delRole(BigInteger roleId) {
		roleMapper.delRole(roleId);
	}

	public List<Map<String, Object>> list(Map<String, Object> params) {
		return roleMapper.list(params);
	}

	@Transactional
	public void editRoleFunction(BigInteger roleId, List<BigInteger> functions) {
		roleMapper.ClearRoleFunction(roleId);
		if (StringUtils.isEmpty(functions)) {
			return;
		}
		roleMapper.editRoleFunction(roleId, functions);
	}

	public List<Map<String, Object>> functionList(BigInteger roleId) {
		return roleMapper.functionList(roleId);
	}

}
