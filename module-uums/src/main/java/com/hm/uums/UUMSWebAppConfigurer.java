/**
 * 
 */
package com.hm.uums;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import com.hm.uums.interceptor.AccessInterceptor;

/**
 * @author hwg
 *
 *         2021年3月2日
 */
@Configuration
public class UUMSWebAppConfigurer implements WebMvcConfigurer {
	@Bean
	public AccessInterceptor accessInterceptor() {
		return new AccessInterceptor();
	}

	@Override
	public void addInterceptors(InterceptorRegistry registry) {
		registry.addInterceptor(accessInterceptor()).addPathPatterns("/**");//拦截所有请求 
	}

}
