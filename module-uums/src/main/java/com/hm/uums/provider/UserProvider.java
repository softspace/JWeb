/**
 * 
 */
package com.hm.uums.provider;

import java.math.BigInteger;
import java.util.HashMap;
import java.util.Map;

import org.apache.ibatis.jdbc.SQL;

import com.hm.common.utils.StringUtils;

/**
 * @author hwg
 *
 *         2020年12月30日
 */
public class UserProvider {
	public String list(Map<String, Object> params) {
		SQL sql = new SQL();
		sql.SELECT("u.`id` as userId");
		sql.SELECT("u.`username` as username");
		sql.SELECT("u.`full_name` as fullname");
		sql.SELECT("u.`email`");
		sql.SELECT("u.`mobile_number` as mobilenumber");
		sql.SELECT("u.`office_phone` as officephone");
		sql.SELECT("u.`status`");
		sql.SELECT("u.`desc`");
		sql.SELECT("u.`create_time` as createtime");
		sql.SELECT("u.`update_time` as updatetime");
		sql.SELECT("org.orgnames");
		sql.SELECT("role.rolenames");
		
		sql.FROM("module_uums_user as u");
		sql.FROM("(select uo.`user_id`, group_concat(org.`name` order by org.`id` asc) as orgnames from module_uums_user_org as uo, module_uums_org as org where uo.`org_id` = org.`id` group by uo.`user_id`) as org");
		sql.FROM("(select ur.`user_id`, group_concat(role.`name`order by role.`id` asc) as rolenames from module_uums_user_role as ur, module_uums_role as role where ur.`role_id` = role.`id` group by ur.`user_id`) as role");
		sql.WHERE("u.`id` = org.`user_id`");
		sql.WHERE("u.`id` = role.`user_id`");

		if (StringUtils.isNotEmpty(params.get("username"))) {
			sql.WHERE("u.`username` like concat('%',#{username},'%')");
		}

		if (StringUtils.isNotEmpty(params.get("fullname"))) {
			sql.WHERE("u.`full_name` like concat('%',#{fullname},'%')");
		}

		if (StringUtils.isNotEmpty(params.get("status"))) {
			sql.WHERE("u.`status` = #{status}");
		}
		
		if (StringUtils.isNotEmpty(params.get("orgId"))) {
			sql.WHERE("exists(select * from module_uums_user_org as uo where uo.`org_id` = #{orgId} and u.`id` = uo.`user_id`)");
		}

		String sortField = StringUtils.toString(params.get("sortField"));
		String sortType = StringUtils.toString(params.get("sortType"));

		sql.GROUP_BY("u.`id`");
		
		if (StringUtils.isNotEmpty(sortField) && StringUtils.isNotEmpty(sortType)) {
			 sql.ORDER_BY("${sortField} ${sortType}");
		}
		
		sql.LIMIT("#{offset}, #{pageSize}");
		return sql.toString();
	}

	public String count(Map<String, Object> params) {
		SQL sql = new SQL();
		sql.SELECT("count(u.`id`)");
		sql.FROM("module_uums_user as u");
		sql.WHERE("1=1");
		if (StringUtils.isNotEmpty(params.get("username"))) {
			sql.WHERE("u.`username` like concat('%',#{username},'%')");
		}

		if (StringUtils.isNotEmpty(params.get("fullname"))) {
			sql.WHERE("u.`full_name` like concat('%',#{fullname},'%')");
		}

		if (StringUtils.isNotEmpty(params.get("status"))) {
			sql.WHERE("u.`status` = #{status}");
		}
		return sql.toString();
	}
	
	public String getFunctionByUser(BigInteger userId) {
		SQL sql = new SQL();
		sql.SELECT("fct.`id` as functionId");
		sql.SELECT("fct.`p_id` as functionPId");
		sql.SELECT("fct.`full_p_id` as fullPId");
		sql.SELECT("fct.`name`");
		sql.SELECT("fct.`status`");
		sql.SELECT("fct.`order`");
		sql.SELECT("fct.`type`");
		sql.SELECT("fct.`code`");
		sql.SELECT("fct.`url`");
		sql.SELECT("fct.`icon`");
		sql.SELECT("fct.`create_time` as createtime");
		sql.SELECT("fct.`update_time` as updatetime");
		sql.FROM("module_uums_function as fct");
		sql.WHERE("fct.`status` = 0");
		sql.WHERE("exists(select 1 from module_uums_role_function as rf, module_uums_user_role as ur,module_uums_role as r where fct.id = rf.function_id and ur.role_id = rf.role_id and ur.role_id = r.id and r.`status` = 0 and ur.user_id = #{userId} )");
		sql.ORDER_BY("fct.p_id asc, fct.`order` asc");
		return sql.toString();
	}
	
	public String getAllFunction() {
		SQL sql = new SQL();
		sql.SELECT("fct.`id` as functionId");
		sql.SELECT("fct.`p_id` as functionPId");
		sql.SELECT("fct.`full_p_id` as fullPId");
		sql.SELECT("fct.`name`");
		sql.SELECT("fct.`status`");
		sql.SELECT("fct.`order`");
		sql.SELECT("fct.`type`");
		sql.SELECT("fct.`code`");
		sql.SELECT("fct.`url`");
		sql.SELECT("fct.`code`");
		sql.SELECT("fct.`icon`");
		sql.SELECT("fct.`create_time` as createtime");
		sql.SELECT("fct.`update_time` as updatetime");
		sql.FROM("module_uums_function as fct");
		sql.WHERE("fct.`status` = 0");
		sql.ORDER_BY("fct.p_id asc, fct.`order` asc");
		return sql.toString();
	}

	public String getFunctionCodeByUser(BigInteger userId) {
		SQL sql = new SQL();
		sql.SELECT("fct.`code`");
		sql.FROM("module_uums_function as fct");
		sql.WHERE("fct.`status` = 0");
		sql.WHERE("exists(select 1 from module_uums_role_function as rf, module_uums_user_role as ur,module_uums_role as r where fct.id = rf.function_id and ur.role_id = rf.role_id and ur.role_id = r.id and r.`status` = 0 and ur.user_id = #{userId} )");
		return sql.toString();
	}
}
