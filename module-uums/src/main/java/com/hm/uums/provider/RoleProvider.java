/**
 * 
 */
package com.hm.uums.provider;

import java.math.BigInteger;
import java.util.Map;

import org.apache.ibatis.jdbc.SQL;

import com.hm.common.utils.StringUtils;

/**
 * @author hwg
 *
 *         2020年12月30日
 */
public class RoleProvider {
	public String list(Map<String, Object> params) {
		SQL sql = new SQL();
		sql.SELECT("role.`id` as roleId");
		sql.SELECT("role.`name`");
		sql.SELECT("role.`code`");
		sql.SELECT("role.`status`");
		sql.SELECT("role.`desc`");
		sql.SELECT("role.`order`");
		sql.SELECT("role.`create_time` as createtime");
		sql.SELECT("role.`update_time` as updatetime");
		sql.FROM("module_uums_role as role");
		sql.WHERE("1=1");

		if (StringUtils.isNotEmpty(params.get("name"))) {
			sql.WHERE("role.`name` like concat('%',#{name},'%')");
		}

		if (StringUtils.isNotEmpty(params.get("status"))) {
			sql.WHERE("role.`status` = #{status}");
		}
		
		sql.ORDER_BY("role.`order` asc");
		return sql.toString();
	}
	
	public String functionList(BigInteger roleId) {
		SQL sql = new SQL();
		sql.SELECT("fct.`id` as functionId");
		sql.SELECT("rfm.`role_id` as roleId");
		sql.SELECT("fct.`p_id` as functionPId");
		sql.SELECT("fct.`full_p_id` as fullPId");
		sql.SELECT("fct.`name`");
		sql.SELECT("fct.`status`");
		sql.SELECT("fct.`order`");
		sql.SELECT("fct.`type`");
		sql.SELECT("fct.`code`");
		sql.SELECT("fct.`url`");
		sql.SELECT("fct.`code`");
		sql.SELECT("fct.`icon`");
		sql.SELECT("fct.`create_time` as createtime");
		sql.SELECT("fct.`update_time` as updatetime");
		sql.FROM("module_uums_function as fct");
		sql.LEFT_OUTER_JOIN("module_uums_role_function as rfm on fct.`id` = rfm.`function_id` and rfm.`role_id` = #{roleId} ");
		sql.WHERE("fct.`status` = 0 ");
		sql.ORDER_BY("fct.`order` asc");
		return sql.toString();
	}
}
