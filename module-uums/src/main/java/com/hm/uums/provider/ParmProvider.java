/**
 * 
 */
package com.hm.uums.provider;

import java.math.BigInteger;
import java.util.Map;

import org.apache.ibatis.jdbc.SQL;

import com.hm.common.utils.StringUtils;

/**
 * @author hwg
 *
 *         2020年12月30日
 */
public class ParmProvider {
	public String list(Map<String, Object> params) {
		SQL sql = new SQL();
		sql.SELECT("parm.`id` as parmId");
		sql.SELECT("parm.`parm_type` as parmType");
		sql.SELECT("parm.`parm_key` as parmKey");
		sql.SELECT("parm.`parm_value` as parmValue");
		sql.SELECT("parm.`parm_desc` as parmDesc");
		sql.SELECT("parm.`create_time` as createtime");
		sql.SELECT("parm.`update_time` as updatetime");
		sql.FROM("module_uums_parm as parm");
		sql.WHERE("1=1");

		if (StringUtils.isNotEmpty(params.get("parmKey"))) {
			sql.WHERE("parm.`parm_key` like concat('%',#{parmKey},'%')");
		}
		
		sql.LIMIT("#{offset}, #{pageSize}");
		return sql.toString();
	}
	
}
