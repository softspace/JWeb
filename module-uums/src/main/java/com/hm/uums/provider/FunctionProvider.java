/**
 * 
 */
package com.hm.uums.provider;

import java.util.Map;

import org.apache.ibatis.jdbc.SQL;

import com.hm.common.utils.StringUtils;

/**
 * @author hwg
 *
 *         2020年12月30日
 */
public class FunctionProvider {
	public String list(Map<String, Object> params) {
		SQL sql = new SQL();
		sql.SELECT("fct.`id` as functionId");
		sql.SELECT("fct.`p_id` as functionPId");
		sql.SELECT("fct.`full_p_id` as fullPId");
		sql.SELECT("fct.`name`");
		sql.SELECT("fct.`status`");
		sql.SELECT("fct.`order`");
		sql.SELECT("fct.`type`");
		sql.SELECT("fct.`code`");
		sql.SELECT("fct.`url`");
		sql.SELECT("fct.`code`");
		sql.SELECT("fct.`icon`");
		sql.SELECT("fct.`create_time` as createtime");
		sql.SELECT("fct.`update_time` as updatetime");
		sql.FROM("module_uums_function as fct");
		sql.WHERE("1=1");

		if (StringUtils.isNotEmpty(params.get("name"))) {
			sql.WHERE("fct.`name` like concat('%',#{name},'%')");
		}
		
		if (StringUtils.isNotEmpty(params.get("type"))) {
			sql.WHERE("fct.`type` = #{type}");
		}

		if (StringUtils.isNotEmpty(params.get("status"))) {
			sql.WHERE("fct.`status` = #{status}");
		}
		
		sql.ORDER_BY("fct.`order` asc");
		return sql.toString();
	}
}
