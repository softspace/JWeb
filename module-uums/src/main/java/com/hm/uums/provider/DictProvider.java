/**
 * 
 */
package com.hm.uums.provider;

import java.math.BigInteger;
import java.util.Map;

import org.apache.ibatis.jdbc.SQL;

import com.hm.common.utils.StringUtils;

/**
 * @author hwg
 *
 *         2020年12月30日
 */
public class DictProvider {
	public String list(Map<String, Object> params) {
		SQL sql = new SQL();
		sql.SELECT("dict.`id` as dictId");
		sql.SELECT("dict.`dict_code` as dictCode");
		sql.SELECT("dict.`dict_name` as dictName");
		sql.SELECT("dict.`dict_desc` as dictDesc");
		sql.SELECT("dict.`create_time` as createtime");
		sql.SELECT("dict.`update_time` as updatetime");
		sql.FROM("module_uums_dict as dict");
		sql.WHERE("1=1");

		if (StringUtils.isNotEmpty(params.get("dictName"))) {
			sql.WHERE("dict.`dict_name` like concat('%',#{dictName},'%')");
		}
		
		if (StringUtils.isNotEmpty(params.get("dictCode"))) {
			sql.WHERE("dict.`dict_code` like concat('%',#{dictCode},'%')");
		}
		
		sql.LIMIT("#{offset}, #{pageSize}");
		return sql.toString();
	}
	
	public String itemlist(Map<String, Object> params) {
		SQL sql = new SQL();
		sql.SELECT("dict.`id` as dictItemId");
		sql.SELECT("dict.`dict_id` as dictId");
		sql.SELECT("dict.`dict_item_code` as dictItemCode");
		sql.SELECT("dict.`dict_item_name` as dictItemName");
		sql.SELECT("dict.`dict_item_desc` as dictItemDesc");
		sql.SELECT("dict.`order`");
		sql.SELECT("dict.`create_time` as createtime");
		sql.SELECT("dict.`update_time` as updatetime");
		sql.FROM("module_uums_dict_item as dict");
		sql.WHERE("dict.`dict_id` = #{dictId}");
		sql.ORDER_BY("dict.`order` asc");
		return sql.toString();
	}
	
}
