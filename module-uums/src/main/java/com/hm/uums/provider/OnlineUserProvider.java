/**
 * 
 */
package com.hm.uums.provider;

import java.math.BigInteger;
import java.util.Map;

import org.apache.ibatis.jdbc.SQL;

import com.hm.common.utils.StringUtils;

/**
 * @author hwg
 *
 *         2020年12月30日
 */
public class OnlineUserProvider {
	public String list(Map<String, Object> params) {
		SQL sql = new SQL();
		sql.SELECT("u.`id` as userId");
		sql.SELECT("u.`username` as username");
		sql.SELECT("u.`full_name` as fullname");
		sql.SELECT("t.`create_time` as loginTime");
		sql.SELECT("t.`refresh_time` as refreshTime");
		sql.SELECT("t.`token_timeout_minutes` as tokenTimeoutMinutes");
		sql.SELECT("t.`client_ip` as clientIp");
		sql.SELECT("t.`client_name` as clientName");

		sql.FROM("module_uums_user as u, `module_uums_token` as t");

		sql.WHERE("u.`id` = t.`user_id` ");
		
		if (StringUtils.isNotEmpty(params.get("username"))) {
			sql.WHERE("u.`username` like concat('%',#{username},'%')");
		}

		if (StringUtils.isNotEmpty(params.get("fullname"))) {
			sql.WHERE("u.`full_name` like concat('%',#{fullname},'%')");
		}

		String sortField = StringUtils.toString(params.get("sortField"));
		String sortType = StringUtils.toString(params.get("sortType"));

		if (StringUtils.isNotEmpty(sortField) && StringUtils.isNotEmpty(sortType)) {
			 sql.ORDER_BY("${sortField} ${sortType}");
		}
		
		sql.LIMIT("#{offset}, #{pageSize}");
		return sql.toString();
	}
	
}
