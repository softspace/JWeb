/**
 * 
 */
package com.hm.uums.provider;

import java.util.Map;

import org.apache.ibatis.jdbc.SQL;

import com.hm.common.utils.StringUtils;

/**
 * @author hwg
 *
 *         2020年12月30日
 */
public class OrgProvider {
	public String list(Map<String, Object> params) {
		SQL sql = new SQL();
		sql.SELECT("org.`id` as orgId");
		sql.SELECT("org.`p_id` as orgPId");
		sql.SELECT("org.`full_p_id` as fullPId");
		sql.SELECT("org.`name`");
		sql.SELECT("org.`status`");
		sql.SELECT("org.`desc`");
		sql.SELECT("org.`order`");
		sql.SELECT("org.`create_time` as createtime");
		sql.SELECT("org.`update_time` as updatetime");
		sql.FROM("module_uums_org as org");
		sql.WHERE("1=1");

		if (StringUtils.isNotEmpty(params.get("name"))) {
			sql.WHERE("org.`name` like concat('%',#{name},'%')");
		}

		if (StringUtils.isNotEmpty(params.get("status"))) {
			sql.WHERE("org.`status` = #{status}");
		}
		
		sql.ORDER_BY("org.`order` asc");
		
		return sql.toString();
	}
}
