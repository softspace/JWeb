package com.hm.uums.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.ResultType;
import org.apache.ibatis.annotations.SelectProvider;

import com.hm.uums.provider.OnlineUserProvider;

/**
 * @author hwg
 *
 *         2020年12月18日
 */
public interface OnlineUserMapper {
	@SelectProvider(type = OnlineUserProvider.class, method = "list")
	@ResultType(value = List.class)
	public List<Map<String, Object>> list(Map<String, Object> params);
}
