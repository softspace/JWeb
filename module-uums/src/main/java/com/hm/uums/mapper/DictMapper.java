package com.hm.uums.mapper;

import java.math.BigInteger;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.ResultType;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.SelectProvider;
import org.apache.ibatis.annotations.Update;

import com.hm.uums.provider.DictProvider;

/**
 * @author hwg
 *
 *         2020年12月18日
 */
public interface DictMapper {
	@Insert("insert into module_uums_dict(`dict_code`,`dict_name`,`dict_desc`,`create_time`,`update_time`) values (#{dictCode},#{dictName},#{dictDesc},#{createtime},#{updatetime}) ")
	public void addDict(Map<String, Object> dMap);

	@Update("update module_uums_dict set `dict_code` = #{dictCode},`dict_name` = #{dictName},`dict_desc`=#{dictDesc},`update_time`= #{updatetime} where `id` = #{dictId} ")
	public void editDict(Map<String, Object> dMap);

	@Select("select `id` as dictId, `dict_code` as dictCode, `dict_name` as dictName, `dict_desc` as dictDesc from module_uums_dict where `id` = #{dictId}")
	@ResultType(value = Map.class)
	public Map<String, Object> getDict(BigInteger dictId);

	@SelectProvider(type = DictProvider.class, method = "list")
	@ResultType(value = List.class)
	public List<Map<String, Object>> list(Map<String, Object> params);

	@Delete("delete from module_uums_dict where `id` = #{dictId} ")
	public void delDict(BigInteger dictId);
	
	@Select("select `id` as dictId, `dict_code` as dictCode, `dict_name` as dictName, `dict_desc` as dictDesc from module_uums_dict where `dict_code` = #{dictCode} ")
	@ResultType(value = Map.class)
	public Map<String, Object> getDictByCode(String dictCode);
	
	////////////////////////////////////////////////////////////////
	@Insert("insert into module_uums_dict_item(`dict_id`,`dict_item_code`,`dict_item_name`,`dict_item_desc`,`order`,`create_time`,`update_time`) values (#{dictId},#{dictItemCode},#{dictItemName},#{dictItemDesc},#{order},#{createtime},#{updatetime}) ")
	public void addDictItem(Map<String, Object> dMap);

	@Update("update module_uums_dict_item set `dict_item_code`=#{dictItemCode}, `dict_item_name` = #{dictItemName},`dict_item_desc`=#{dictItemDesc},`order`= #{order},`update_time`= #{updatetime} where `id` = #{dictItemId} ")
	public void editDictItem(Map<String, Object> dMap);

	@Select("select `id` as dictItemId, `dict_id` as dictId, `dict_item_code` as dictItemCode,`dict_item_name` as dictItemName,`dict_item_desc` as dictItemDesc,`order` from module_uums_dict_item where `id` = #{dictItemId}")
	@ResultType(value = Map.class)
	public Map<String, Object> getDictItem(BigInteger dictItemId);

	@SelectProvider(type = DictProvider.class, method = "itemlist")
	@ResultType(value = List.class)
	public List<Map<String, Object>> itemlist(Map<String, Object> params);

	@Delete("delete from module_uums_dict_item where `id` = #{dictItemId} ")
	public void delDictItem(BigInteger dictItemId);
	
	@Delete("delete from module_uums_dict_item where `dict_id` = #{dictId} ")
	public void delDictItemByDictId(BigInteger dictId);
}
