package com.hm.uums.mapper;

import java.math.BigInteger;
import java.util.Map;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.ResultType;
import org.apache.ibatis.annotations.Select;

/**
 * @author hwg
 *
 *         2020年12月18日 公共业务Mapper
 */
public interface CommonMapper {
	/**
	 * 根据用户名和密码查找用户
	 * 
	 * @param username
	 * @param password
	 * @return
	 */
	@Select("select `id`, `status` from module_uums_user where username = #{username} and password = #{password} ")
	@ResultType(value = Map.class)
	public Map<String, Object> findUser(String username, String password);

	/**
	 * 添加token
	 * 
	 * @param dMap
	 */
	@Insert("insert into module_uums_token(`token`, `user_id`, `token_timeout_minutes`, `create_time`, `refresh_time`, `client_ip`, `client_name`) values (#{token}, #{userId}, #{tokenTimeoutMinutes}, #{cdate}, #{refreshTime}, #{clientIp}, #{clientName})")
	public void addToken(Map<String, Object> dMap);

	/**
	 * 备份指定token到历史表
	 * 
	 * @param token
	 */
	@Delete("insert into module_uums_token_his(`token`,`user_id`,`create_time`,`refresh_time`,`delete_time`,`token_timeout_minutes`,`client_ip`,`client_name`,`del_type`) SELECT `token`,`user_id`,`create_time`,`refresh_time`, date_format(now(),'%Y-%m-%d %H:%i:%s') as `delete_time`,`token_timeout_minutes`,`client_ip`,`client_name`,0 as `del_type`from module_uums_token where `token` = #{token}")
	public void backupTokenToHis(String token);

	/**
	 * 删除指定token
	 * 
	 * @param token
	 */
	@Delete("delete from module_uums_token where `token` = #{token}")
	public void delToken(String token);

	/**
	 * 备份指定用户token到历史表
	 * 
	 * @param token
	 */
	@Delete("insert into module_uums_token_his(`token`,`user_id`,`create_time`,`refresh_time`,`delete_time`,`token_timeout_minutes`,`client_ip`,`client_name`,`del_type`) SELECT `token`,`user_id`,`create_time`,`refresh_time`, date_format(now(),'%Y-%m-%d %H:%i:%s') as `delete_time`,`token_timeout_minutes`,`client_ip`,`client_name`,2 as `del_type`from module_uums_token where `user_id` = #{userId}")
	public void backupSameUserTokenToHis(BigInteger userId);

	/**
	 * 删除指定用户token
	 * 
	 * @param token
	 */
	@Delete("delete from module_uums_token where `user_id` = #{userId}")
	public void delSameUserToken(BigInteger userId);

	/**
	 * 获取系统信息
	 * 
	 * @param sysCode
	 * @return
	 */
	@Select("select `sys_code` as sysCode, `sys_name` as sysName, `sys_version` as sysVersion, `sys_publish_date` as sysPublishDate, `sys_authorization_code` as authCode, `sys_manager_moblie_phone` as sysManagerMobliePhone, `sys_manager_email` as sysManagerEmail from module_sys_info where `sys_code` = #{sysCode} ")
	@ResultType(value = Map.class)
	public Map<String, Object> getSysInfo(int sysCode);

	/**
	 * 查找指定token
	 * 
	 * @param token
	 * @return
	 */
	@Select("select `user_id` as userId, `refresh_time` as refreshTime, `create_time` as createTime from module_uums_token where token = #{token}")
	@ResultType(value = Map.class)
	public Map<String, Object> findToken(String token);

}
