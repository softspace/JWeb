package com.hm.uums.mapper;

import java.math.BigInteger;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.ResultType;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.SelectProvider;
import org.apache.ibatis.annotations.Update;

import com.hm.uums.provider.UserProvider;

/**
 * @author hwg
 *
 *         2020年12月18日
 */
public interface UserMapper {
	@Insert("insert into module_uums_user(`username`,`password`,`full_name`,`email`,`mobile_number`,`office_phone`,`desc`,`create_time`,`update_time`) values (#{username},#{password},#{fullname},#{email},#{mobilenumber},#{officephone},#{desc},#{createtime},#{updatetime}) ")
	@Options(useGeneratedKeys = true, keyProperty = "userId", keyColumn = "id")
	public void addUser(Map<String, Object> dMap);

	@Update("update module_uums_user set `username` = #{username}, `full_name` = #{fullname}, `email` = #{email}, `mobile_number` = #{mobilenumber}, `office_phone` = #{officephone}, `desc` = #{desc}, `update_time` = #{updatetime} where `id` = #{userId} ")
	public void editUser(Map<String, Object> dMap);

	@Update("update module_uums_user set `status` = #{status}, `update_time` = #{updatetime} where `id` = #{userId} ")
	public void editUserStatus(BigInteger userId, int status, String updatetime);

	@Select("select `id` as userId,`username`,`full_name` as fullname,`email`,`mobile_number` as mobilenumber,`office_phone` as officephone,`status`,`desc`,`create_time` as createtime,`update_time` as updatetime from module_uums_user where `id` = #{userId}")
	@ResultType(value = Map.class)
	public Map<String, Object> getUser(BigInteger userId);

	@SelectProvider(type = UserProvider.class, method = "list")
	@ResultType(value = List.class)
	public List<Map<String, Object>> list(Map<String, Object> params);

	@SelectProvider(type = UserProvider.class, method = "count")
	@ResultType(value = Long.class)
	public long count(Map<String, Object> params);

	@Delete("delete from module_uums_user where `id` = #{userId};delete from module_uums_user_org where `user_id` = #{userId};delete from module_uums_user_role where `user_id` = #{userId} ")
	public void delUser(BigInteger userId);

	@Delete("delete from module_uums_user_org where `user_id` = #{userId} ")
	public void ClearUserOrg(BigInteger userId);

	@Insert({"<script>", //
			"insert into module_uums_user_org(`user_id`, `org_id`) values ", //
			"<foreach collection='orgs' item='orgId' index='index' separator=','>", //
			"(#{userId}, #{orgId})", //
			"</foreach>", //
			"</script>"})
	public void editUserOrg(BigInteger userId, Set<BigInteger> orgs);

	@Delete("delete from module_uums_user_role where `user_id` = #{userId} ")
	public void ClearUserRole(BigInteger userId);

	@Insert({"<script>", //
			"insert into module_uums_user_role(`user_id`, `role_id`) values ", //
			"<foreach collection='roles' item='roleId' index='index' separator=','>", //
			"(#{userId}, #{roleId})", //
			"</foreach>", //
			"</script>"})
	public void editUserRole(BigInteger userId, Set<BigInteger> roles);
	
	@Select("select org.`id` as orgId,org.`p_id` as orgPId, org.`full_p_id` as fullPId, org.`name`,org.`status`,org.`order`,org.`desc` from module_uums_user_org as uo, module_uums_org as org where org.`status` = 0 and uo.`org_id` = org.`id` and uo.`user_id` = #{userId}")
	@ResultType(value = List.class)
	public List<Map<String, Object>> getOrgByUser(BigInteger userId);
	
	@Select("select role.`id` as roleId,role.`name`,role.`code`,role.`desc`,role.`order` from module_uums_user_role as ur, module_uums_role as role where role.`status` = 0 and ur.`role_id` = role.`id` and ur.`user_id` = #{userId}")
	@ResultType(value = List.class)
	public List<Map<String, Object>> getRoleByUser(BigInteger userId);

	@Update("update module_uums_user set `password` = #{password}, `update_time` = #{updatetime} where `id` = #{userId} ")
	public void modifyPassword(BigInteger userId, String password, String updatetime);
	
	@Select("select `password` from module_uums_user where `id` = #{userId}")
	@ResultType(value = String.class)
	public String getPassword(BigInteger userId);
	
	@SelectProvider(type = UserProvider.class, method = "getFunctionByUser")
	@ResultType(value = List.class)
	public List<Map<String, Object>> getFunctionByUser(BigInteger userId);
	
	@SelectProvider(type = UserProvider.class, method = "getAllFunction")
	@ResultType(value = List.class)
	public List<Map<String, Object>> getAllFunction();
	
	@SelectProvider(type = UserProvider.class, method = "getFunctionCodeByUser")
	@ResultType(value = List.class)
	public List<Map<String, Object>> getFunctionCodeByUser(BigInteger userId);
}
