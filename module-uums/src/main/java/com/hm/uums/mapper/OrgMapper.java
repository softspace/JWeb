package com.hm.uums.mapper;

import java.math.BigInteger;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.ResultType;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.SelectProvider;
import org.apache.ibatis.annotations.Update;

import com.hm.uums.provider.OrgProvider;

/**
 * @author hwg
 *
 *         2020年12月18日
 */
public interface OrgMapper {
	@Insert("insert into module_uums_org(`p_id`,`full_p_id`,`name`,`desc`,`order`,`create_time`,`update_time`) values (#{orgPId},#{fullPId},#{name},#{desc},#{order},#{createtime},#{updatetime}) ")
	public void addOrg(Map<String, Object> dMap);
	
	@Update("update module_uums_org set `p_id` = #{orgPId},`full_p_id`=#{fullPId}, `name` = #{name},`desc` = #{desc},`order`= #{order}, `update_time` = #{updatetime} where `id` = #{orgId} ")
	public void editOrg(Map<String, Object> dMap);
	
	@Update("update module_uums_org set `full_p_id`=replace(`full_p_id`, #{oldFullPId}, #{newFullPId}), `update_time` = #{updatetime} where `full_p_id` like concat(#{oldFullPId},'%') ")
	public void editSubFullPId(String newFullPId, String oldFullPId, String updatetime);

	@Update("update module_uums_org set `status` = #{status}, `update_time` = #{updatetime} where `id` = #{orgId} ")
	public void editOrgStatus(BigInteger orgId, int status, String updatetime);
	
	@Select("select `id` as orgId,`p_id` as orgPId, `full_p_id` as fullPId, `name`,`status`,`order`,`desc` from module_uums_org where `id` = #{orgId}")
	@ResultType(value = Map.class)
	public Map<String, Object> getOrg(BigInteger orgId);
	
	@SelectProvider(type = OrgProvider.class, method = "list")
	@ResultType(value = List.class)
	public List<Map<String, Object>> list(Map<String, Object> params);
	
	@Select("select count(1) from module_uums_org where `p_id` = #{orgPId} and `status` = 0 ")
	public long hasEnableSubOrg(BigInteger orgPId);
	
	@Select("select count(1) from module_uums_org where `p_id` = #{orgPId} ")
	public long hasSubOrg(BigInteger orgPId);

	@Delete("delete from module_uums_org where `id` = #{orgId} ")
	public void delOrg(BigInteger orgId);
}
