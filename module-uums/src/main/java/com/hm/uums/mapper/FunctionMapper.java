package com.hm.uums.mapper;

import java.math.BigInteger;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.ResultType;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.SelectProvider;
import org.apache.ibatis.annotations.Update;

import com.hm.uums.provider.FunctionProvider;

/**
 * @author hwg
 *
 *         2020年12月18日
 */
public interface FunctionMapper {
	@Insert("insert into module_uums_function(`p_id`,`full_p_id`,`name`,`desc`,`order`,`type`,`code`,`url`,`icon`,`create_time`,`update_time`) values (#{functionPId},#{fullPId},#{name},#{desc},#{order},#{type},#{code},#{url},#{icon},#{createtime},#{updatetime}) ")
	public void addFunction(Map<String, Object> dMap);

	@Update("update module_uums_function set `p_id` = #{functionPId},`full_p_id`=#{fullPId},`name` = #{name},`desc` = #{desc},`order`= #{order},`type`=#{type},`code`=#{code},`url`=#{url},`icon`=#{icon},`update_time`= #{updatetime} where `id` = #{functionId} ")
	public void editFunction(Map<String, Object> dMap);

	@Update("update module_uums_function set `full_p_id`=replace(`full_p_id`, #{oldFullPId}, #{newFullPId}), `update_time` = #{updatetime} where `full_p_id` like concat(#{oldFullPId},'%') ")
	public void editSubFullPId(String newFullPId, String oldFullPId, String updatetime);

	@Update("update module_uums_function set `status` = #{status}, `update_time` = #{updatetime} where `id` = #{functionId} ")
	public void editFunctionStatus(BigInteger functionId, int status, String updatetime);

	@Select("select `id` as functionId,`p_id` as functionPId,`full_p_id` as fullPId,`name`,`status`,`type`,`code`,`url`,`order`,`icon`,`desc` from module_uums_function where `id` = #{functionId}")
	@ResultType(value = Map.class)
	public Map<String, Object> getFunction(BigInteger functionId);

	@SelectProvider(type = FunctionProvider.class, method = "list")
	@ResultType(value = List.class)
	public List<Map<String, Object>> list(Map<String, Object> params);

	@Select("select count(1) from module_uums_function where `p_id` = #{functionPId} and `status` = 0 ")
	public long hasEnableSubFunction(BigInteger functionPId);

	@Select("select count(1) from module_uums_function where `p_id` = #{functionPId} ")
	public long hasSubFunction(BigInteger functionPId);

	@Delete("delete from module_uums_function where `id` = #{functionId} ")
	public void delFunction(BigInteger functionId);
}
