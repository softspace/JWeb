package com.hm.uums.mapper;

import java.math.BigInteger;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.ResultType;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.SelectProvider;
import org.apache.ibatis.annotations.Update;

import com.hm.uums.provider.ParmProvider;

/**
 * @author hwg
 *
 *         2020年12月18日
 */
public interface ParmMapper {
	@Insert("insert into module_uums_parm(`parm_type`,`parm_key`,`parm_value`,`parm_desc`,`create_time`,`update_time`) values (#{parmType},#{parmKey},#{parmValue},#{parmDesc},#{createtime},#{updatetime}) ")
	public void addParm(Map<String, Object> dMap);

	@Update("update module_uums_parm set `parm_type` = #{parmType},`parm_key`=#{parmKey},`parm_value`= #{parmValue},`parm_desc`=#{parmDesc},`update_time`= #{updatetime} where `id` = #{parmId} ")
	public void editParm(Map<String, Object> dMap);

	@Select("select `id` as parmId, `parm_type` as parmType, `parm_key` as parmKey, `parm_value` as parmValue, `parm_desc` as parmDesc from module_uums_parm where `id` = #{parmId}")
	@ResultType(value = Map.class)
	public Map<String, Object> getParm(BigInteger parmId);

	@SelectProvider(type = ParmProvider.class, method = "list")
	@ResultType(value = List.class)
	public List<Map<String, Object>> list(Map<String, Object> params);

	@Delete("delete from module_uums_parm where `id` = #{parmId} ")
	public void delParm(BigInteger parmId);
	
	@Select("select `id` as parmId, `parm_type` as parmType, `parm_key` as parmKey, `parm_value` as parmValue, `parm_desc` as parmDesc from module_uums_parm where `parm_key` = #{parmKey} ")
	@ResultType(value = Map.class)
	public Map<String, Object> getParmByKey(String parmKey);
}
