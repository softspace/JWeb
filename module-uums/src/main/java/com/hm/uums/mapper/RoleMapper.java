package com.hm.uums.mapper;

import java.math.BigInteger;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.ResultType;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.SelectProvider;
import org.apache.ibatis.annotations.Update;

import com.hm.uums.provider.RoleProvider;

/**
 * @author hwg
 *
 *         2020年12月18日
 */
public interface RoleMapper {
	@Insert("insert into module_uums_role(`name`,`code`,`desc`,`order`,`create_time`,`update_time`) values (#{name},#{code},#{desc},#{order},#{createtime},#{updatetime}) ")
	public void addRole(Map<String, Object> dMap);

	@Update("update module_uums_role set `name` = #{name},`code`=#{code},`order`= #{order},`desc`=#{desc},`update_time`= #{updatetime} where `id` = #{roleId} ")
	public void editRole(Map<String, Object> dMap);

	@Update("update module_uums_role set `status` = #{status}, `update_time` = #{updatetime} where `id` = #{roleId} ")
	public void editRoleStatus(BigInteger roleId, int status, String updatetime);

	@Select("select `id` as roleId,`name`,`code`,`desc`,`order` from module_uums_role where `id` = #{roleId}")
	@ResultType(value = Map.class)
	public Map<String, Object> getRole(BigInteger roleId);

	@SelectProvider(type = RoleProvider.class, method = "list")
	@ResultType(value = List.class)
	public List<Map<String, Object>> list(Map<String, Object> params);

	@Delete("delete from module_uums_role where `id` = #{roleId} ")
	public void delRole(BigInteger roleId);

	@Delete("delete from module_uums_role_function where `role_id` = #{roleId} ")
	public void ClearRoleFunction(BigInteger roleId);

	@Insert({"<script>", //
			"insert into module_uums_role_function(`role_id`, `function_id`) values ", //
			"<foreach collection='functions' item='function' index='index' separator=','>", //
			"(#{roleId}, #{function})", //
			"</foreach>", //
			"</script>"})
	public void editRoleFunction(BigInteger roleId, List<BigInteger> functions);

	@SelectProvider(type = RoleProvider.class, method = "functionList")
	@ResultType(value = List.class)
	public List<Map<String, Object>> functionList(BigInteger roleId);
}
