package com.hm.uums.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.ResultType;
import org.apache.ibatis.annotations.SelectProvider;

import com.hm.uums.provider.LogProvider;
import com.hm.uums.provider.UserProvider;

/**
 * @author hwg
 *
 *         2020年12月18日
 */
public interface LogMapper {
	@SelectProvider(type = LogProvider.class, method = "list")
	@ResultType(value = List.class)
	public List<Map<String, Object>> list(Map<String, Object> params);
	
	@SelectProvider(type = LogProvider.class, method = "count")
	@ResultType(value = Long.class)
	public long count(Map<String, Object> params);
}
