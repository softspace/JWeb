import cutils from '../../js/common/core-utils.js';
import '../../js/common/table-col-utils.js';

$(function() {
	layui.laydate.render({
		elem: '#q-start-req-date'
	});

	layui.laydate.render({
		elem: '#q-end-req-date'
	});

	initTable();

	$(document).keydown(function() {
		if (event.keyCode == 13) {
			doSearch();
		}
	});

	cutils.doAuthSign();
});

self.initTable = function() {
	$('#data-table').datagrid({
		url: "/" + cutils.webApp + "/log/list?_time=" + cutils.now(),
		method: 'get',
		animate: true,
		fit: true,
		fitColumns: false,
		rownumbers: false,// 行号
		loadMsg: false,
		resizeHandle: 'right',
		singleSelect: false,
		checkOnSelect: false,
		selectOnCheck: false,
		nowrap: false,
		emptyMsg: '无数据',
		pagination: true,
		pageSize: 50,
		pageList: [20, 50, 100, 200, 500],
		onBeforeLoad: function(param) {
			param["pageNo"] = param.page;
			param["pageSize"] = param.rows;
			param["sortType"] = param.order;
			param["sortField"] = param.sort;

			if (param["sortType"] == null || param["sortType"] == "") {
				param["sortType"] = "desc";
			}

			if (param["sortField"] == null || param["sortField"] == "") {
				param["sortField"] = "reqTime";
			}
		},
		onLoadSuccess: function(data) {
			$(".datagrid-pager td:nth-last-child(3)").hide();
			layui.form.render();
			cutils.doAuthSign();
		},
		onLoadError: function() {
			$(".datagrid-body").html("<div style='width: 100%; height: 50px; line-height: 50px; text-align: center;font-size: 14px;'>数据加载出错</div>");
		},
		loadFilter: function(data, parentId) {
			if (cutils.notEquals(data.status, 200)) {
				layer.msg(data.message, { icon: 3 });
				return;
			}

			return {
				total: 1000000000,//日志查询去掉计算总数功能，这里把总数设置为1000000000，视情况而定
				rows: data.data.dataList
			};
		},
		columns: [[{
			field: 'fullname',
			title: '操作用户',
			width: 100
		}, {
			field: 'reqUrl',
			title: '请求路径',
			width: 180
		}, {
			field: 'functionDesc',
			title: '日志标题',
			width: 150
		}, {
			field: 'method',
			title: 'http方法',
			width: 100,
			align: 'center'
		}, {
			field: 'clientIp',
			title: '客户端IP',
			width: 120
		}, {
			field: 'reqTime',
			title: '请求时间',
			width: 180,
			align: 'center',
			sortable: true
		}, {
			field: 'statusCode',
			title: '状态码',
			width: 100,
			align: 'center',
			sortable: true
		}, {
			field: 'respDuration',
			title: '响应时长',
			width: 100,
			align: 'center',
			sortable: true,
			formatter: function(value, row, index) {
				return value + "毫秒";
			}
		}, {
			field: 'clientName',
			title: '客户端设备名称',
			width: 500
		}]]
	});

	$('#data-table').datagrid('getPager').pagination({
		beforePageText: '第',
		afterPageText: '页',
		displayMsg: '显示 {from}到{to}'
	});
}

self.doSearch = function() {
	var queryParams = {};
	queryParams["userId"] = $("#q-user-id").val();
	queryParams["reqStartDate"] = $("#q-start-req-date").val();
	queryParams["reqEndDate"] = $("#q-end-req-date").val();
	queryParams["clientIp"] = $("#q-client-ip").val();
	$('#data-table').datagrid('load', queryParams);
}

self.doSelectUser = function() {
	top.layer.open({
		type: 2,
		title: '选择用户',
		area: ['1000px', '450px'],
		shade: 0.01,
		maxmin: true,
		shadeClose: true,
		content: "/" + cutils.webApp + "/views/user/select.html?singleSelect=true&userId=" + $("#q-user-id").val() + "&_time=" + cutils.now(),
		btn: ['<i class="fa fa-check" style="color: inherit;"></i>确定', '<i class="fa fa-eraser" style="color: inherit;"></i>清除', '<i class="fa fa-remove" style="color: inherit;"></i>取消'],
		yes: function(index, layero) {
			top.layer.close(index);

			var iframeWin = layero.find('iframe')[0];
			var data = iframeWin.contentWindow.getChecked();
			$("#q-user-id").val(data.userId);
			$("#q-username").val(data.fullname);

			doSearch();
		},
		btn2: function(index, layero) {
			top.layer.close(index);

			$("#q-user-id").val("");
			$("#q-username").val("");

			doSearch();
		}, btn3: function(index, layero) {
			top.layer.close(index);
		},
		cancel: function(index) {
			top.layer.close(index);
		}
	});
}