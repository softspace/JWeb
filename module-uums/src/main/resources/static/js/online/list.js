import cutils from '../../js/common/core-utils.js';
import '../../js/common/table-col-utils.js';

$(function() {
	initTable();

	$(document).keydown(function() {
		if (event.keyCode == 13) {
			doSearch();
		}
	});

	cutils.doAuthSign();
});

self.initTable = function() {
	$('#data-table').datagrid({
		url: "/" + cutils.webApp + "/online/user/list?_time=" + cutils.now(),
		method: 'get',
		animate: true,
		fit: true,
		fitColumns: false,
		rownumbers: false,// 行号
		loadMsg: false,
		resizeHandle: 'right',
		singleSelect: false,
		checkOnSelect: false,
		selectOnCheck: false,
		nowrap: false,
		emptyMsg: '无数据',
		pagination: true,
		pageSize: 50,
		pageList: [20, 50, 100, 200, 500],
		onBeforeLoad: function(param) {
			param["pageNo"] = param.page;
			param["pageSize"] = param.rows;
			param["sortType"] = param.order;
			param["sortField"] = param.sort;
		},
		onLoadSuccess: function(data) {
			layui.form.render();
			cutils.doAuthSign();
		},
		onLoadError: function() {
			$(".datagrid-body").html("<div style='width: 100%; height: 50px; line-height: 50px; text-align: center;font-size: 14px;'>数据加载出错</div>");
		},
		loadFilter: function(data, parentId) {
			if (cutils.notEquals(data.status, 200)) {
				layer.msg(data.message, { icon: 3 });
				return;
			}

			return {
				total: data.data.count,
				rows: data.data.dataList
			};
		},
		columns: [[{
			field: 'username',
			title: '登录账号',
			width: 100
		}, {
			field: 'fullname',
			title: '姓名',
			width: 100
		}, {
			field: 'loginTime',
			title: '登录时间',
			width: 180,
			align: 'center',
			sortable: true
		}, {
			field: 'refreshTime',
			title: '最后访问时间',
			width: 180,
			align: 'center',
			sortable: true
		}, {
			field: 'tokenTimeoutMinutes',
			title: 'token时效',
			width: 100,
			align: 'center',
			sortable: true,
			formatter: function(value, row, index) {
				return value + "分钟";
			}
		}, {
			field: 'clientIp',
			title: '客户端IP',
			width: 160
		}, {
			field: 'clientName',
			title: '客户端设备名称',
			width: 250
		}]]
	});
}

self.doSearch = function() {
	var queryParams = {};
	queryParams["username"] = $("#q-username").val();
	queryParams["fullname"] = $("#q-fullname").val();
	$('#data-table').datagrid('load', queryParams);
}