import cutils from '../common/core-utils.js';

/**
	 * 1、获取系统信息，初始化系统名称，版本号等信息
	 * 2、初始化完成后，如果已经存在token，验证token是否有效
	 * 3、token有效，则直接跳转到主页面，token无效，则引导用户进行登录
	 * 其中，系统信息，用户登录返回的token都会保存到localStorage 
	 */
$(function() {
	$.ajax({
		type: "get",
		url: "/" + cutils.webApp + "/common/sys?_time=" + cutils.now(),
		data: {
			sysCode: "10000",
		},
		dataType: 'json',
		success: function(result) {
			if (cutils.notEquals(result.status, 200)) {
				layer.msg("初始化错误! " + result.message, {
					icon: 7
				});
				return;
			}

			cutils.setSysData(result.data);//存储系统信息数据
			var sysInfo = cutils.getSysData(); //获取系统信息数据

			document.title = sysInfo.sysName;//设置title
			$("#sys-name").html(sysInfo.sysName);//设置系统名称
			$("#sys-version").html(sysInfo.sysVersion); //设置系统版本

			//记住账号密码处理
			var rememberMe = top.localStorage.getItem("HM-REMEMBER-ME");
			var username = top.localStorage.getItem("HM-LOGIN-NAME");
			var password = top.localStorage.getItem("HM-LOGIN-PASSWORD");

			if (rememberMe == "on") {//记住账号密码的情况下，把记住账号密码框√上
				layui.form.val('remember-me-form', {
					"rememberMe": true
				});
			} else {
				username = "";
				password = "";
			}

			$("#q-username").val(username);
			$("#q-password").val(password);

			//验证token 
			validateToken();

			//初始化滑块
			$('#drag-validate-bar').drag();

			//绑定回车键，回车自动登录
			bindEnter();
		}
	});
});

self.login = function() {
	var username = $("#q-username").val();
	var password = $("#q-password").val();

	if (cutils.isEmpty(username)) {
		layer.tips('请输入账号', '#q-username', {
			tips: 1
		});
		return;
	}

	if (cutils.isEmpty(password)) {
		layer.tips('请输入密码', '#q-password', {
			tips: 1
		});
		return;
	}

	if ($(".handler_ok_bg").length == 0) {
		layer.tips('请移动滑块验证', '.handler', {
			tips: 1
		});
		return;
	}

	$("#login-btn").html("登录中...");

	$.ajax({
		type: "POST",
		url: "/" + cutils.webApp + "/common/login?_time=" + cutils.now(),
		data: {
			username: username,
			password: password
		},
		dataType: 'json',
		success: function(result) {
			if (cutils.notEquals(result.status, 200)) {
				$("#login-btn").html("立即登录");
				layer.msg(result.message, { icon: 7 });
				return;
			}

			var formData = layui.form.val('remember-me-form');

			if (formData.rememberMe == "on") {
				top.localStorage.setItem("HM-REMEMBER-ME", "on");
				top.localStorage.setItem("HM-LOGIN-NAME", username);
				top.localStorage.setItem("HM-LOGIN-PASSWORD", password);
			} else {
				top.localStorage.removeItem("HM-REMEMBER-ME");
				top.localStorage.removeItem("HM-LOGIN-NAME");
				top.localStorage.removeItem("HM-LOGIN-PASSWORD");
			}

			//存储token
			cutils.addToken(result.data.token);

			top.location.href = "/" + cutils.webApp + "/views/main.html";
		},
		error: function(e) {
			$("#login-btn").html("立即登录");
			alert(e.status + ", " + e.responseText);
		}
	});
}

self.validateToken = function() {
	var token = cutils.getToken();

	if (cutils.isEmpty(token)) {
		$(".login-container").show();
		return;
	}

	var layerLoadIndex = layer.load(1);

	$.ajax({
		type: "get",
		url: "/" + cutils.webApp + "/common/validateToken",
		dataType: 'json',
		success: function(result) {
			layer.close(layerLoadIndex);

			if (cutils.isEmpty(result)) {
				cutils.delToken();
				$(".login-container").show();
				return;
			}

			 if (cutils.equals(result.status, 200)) {
				top.location.href = "/" + cutils.webApp + "/views/main.html";
				return;
			}

			cutils.delToken();
			$(".login-container").show();
		},
		error: function(e) {
			layer.close(layerLoadIndex);

			cutils.delToken();
			$(".login-container").show();
		}
	});
}

self.bindEnter = function() {
	$(document).keydown(function() {
		if (event.keyCode == 13) {
			login()
		}
	});
}