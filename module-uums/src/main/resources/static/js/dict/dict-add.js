import cutils from '../../js/common/core-utils.js';

self.doSubmitHandler = function(layerFromWin, pageFromWindow) {
	var dictName = $("#q-dict-name").val();
	var dictCode = $("#q-dict-code").val();

	if (cutils.isEmpty(dictName)) {
		layer.tips('请输入字典名称', '#q-dict-name', {
			tips: 1
		});
		return;
	}

	if (cutils.isEmpty(dictCode)) {
		layer.tips('请输入字典编号', '#q-dict-code', {
			tips: 1
		});
		return;
	}

	$.ajax({
		type: "POST",
		url: "/" + cutils.webApp + "/dict?_time=" + cutils.now(),
		data: $('#q-data-form').serialize(),
		dataType: 'json',
		success: function(result) {
			 if (cutils.equals(result.status, 200)) {
				layerFromWin.layer.msg(result.message, { icon: 1, time: 1000 });
				var indexFrame = layerFromWin.layer.getFrameIndex(window.name);
				layerFromWin.layer.close(indexFrame);
				pageFromWindow.doSearch();
				return;
			}
			layerFromWin.layer.msg(result.message, { icon: 7 });
		}
	});
}