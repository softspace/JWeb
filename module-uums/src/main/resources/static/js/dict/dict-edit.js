import cutils from '../../js/common/core-utils.js';

$(function() {
	initData();
});

self.initData = function() {
	var vars = cutils.getUrlVars();
	var dictId = vars["dictId"];

	$.ajax({
		type: "get",
		url: "/" + cutils.webApp + "/dict/" + dictId + "?_time=" + cutils.now(),
		data: $('#q-data-form').serialize(),
		dataType: 'json',
		success: function(result) {
			if (cutils.notEquals(result.status, 200)) {
				layer.msg(result.message, { icon: 7 });
				return;
			}

			$("#q-dict-id").val(result.data.dictId);
			$("#q-dict-name").val(result.data.dictName);
			$("#q-dict-code").val(result.data.dictCode);
		}
	});
}

self.doSubmitHandler = function(layerFromWin, pageFromWindow) {
	var dictName = $("#q-dict-name").val();
	var dictCode = $("#q-dict-code").val();

	if (cutils.isEmpty(dictName)) {
		layer.tips('请输入字典名称', '#q-dict-name', {
			tips: 1
		});
		return;
	}

	if (cutils.isEmpty(dictCode)) {
		layer.tips('请输入字典编号', '#q-dict-code', {
			tips: 1
		});
		return;
	}

	var layerLoadIndex = layer.load(1, { shade: 0.3 });
	$.ajax({
		type: "put",
		url: "/" + cutils.webApp + "/dict?_time=" + cutils.now(),
		data: $('#q-data-form').serialize(),
		dataType: 'json',
		success: function(result) {
			layer.close(layerLoadIndex);
			if (cutils.equals(result.status, 200)) {
				layerFromWin.layer.msg(result.message, { icon: 1, time: 1000 });
				var indexFrame = layerFromWin.layer.getFrameIndex(window.name);
				layerFromWin.layer.close(indexFrame);
				pageFromWindow.doSearch();
				return;
			}
			layerFromWin.layer.msg(result.message, { icon: 7 });
		},
		error: function(e) {
			layer.close(layerLoadIndex);
			alert(e.status + ", " + e.responseText);
		}
	});
}