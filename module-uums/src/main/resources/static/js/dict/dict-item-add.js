import cutils from '../../js/common/core-utils.js';

$(function() {
	initData();
});

self.initData = function() {
	var vars = cutils.getUrlVars();
	var dictId = vars["dictId"];
	$("#q-dict-id").val(dictId);
}

self.doSubmitHandler = function(layerFromWin, pageFromWindow) {
	var dictItemName = $("#q-dict-item-name").val();
	var dictItemCode = $("#q-dict-item-code").val();
	var order = $("#q-dict-item-order").val();

	if (cutils.isEmpty(dictItemName)) {
		layer.tips('请输入参数名称', '#q-dict-item-name', {
			tips: 1
		});
		return;
	}

	if (cutils.isEmpty(dictItemCode)) {
		layer.tips('请输入参数编号', '#q-dict-item-code', {
			tips: 1
		});
		return;
	}

	if (cutils.isNotEmpty(order) && cutils.isNotPositiveInteger(order)) {
		layer.tips('序号只能是正整数', '#q-dict-item-order', {
			tips: 1
		});
		return;
	}

	$.ajax({
		type: "POST",
		url: "/" + cutils.webApp + "/dict/item?_time=" + cutils.now(),
		data: $('#q-data-form').serialize(),
		dataType: 'json',
		success: function(result) {
			if (cutils.equals(result.status, 200)) {
				layerFromWin.layer.msg(result.message, { icon: 1, time: 1000 });
				var indexFrame = layerFromWin.layer.getFrameIndex(window.name);
				layerFromWin.layer.close(indexFrame);
				pageFromWindow.doDictItemSearch();
				return;
			}
			layerFromWin.layer.msg(result.message, { icon: 7 });
		}
	});
}