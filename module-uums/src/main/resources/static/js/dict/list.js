import cutils from '../../js/common/core-utils.js';
import '../../js/common/table-col-utils.js';

$(function() {
	initTable();

	$(document).keydown(function() {
		if (event.keyCode == 13) {
			doSearch();
		}
	});

	cutils.doAuthSign();
});

self.initTable = function() {
	$('#dict-data-table').datagrid({
		url: "/" + cutils.webApp + "/dict/list?_time=" + cutils.now(),
		method: 'get',
		animate: true,
		fit: true,
		fitColumns: false,
		rownumbers: false,// 行号
		loadMsg: false,
		resizeHandle: 'right',
		singleSelect: true,
		checkOnSelect: false,
		selectOnCheck: false,
		nowrap: false,
		emptyMsg: '无数据',
		pagination: true,
		pageSize: 50,
		pageList: [20, 50, 100, 200, 500],
		onLoadSuccess: function(data) {
			cutils.doAuthSign();
			if (data.rows.length == 0) {
				return;
			}
			$('#dict-data-table').datagrid("selectRow", 0);
			initDictItemTable(data.rows[0].dictId);
		},
		onLoadError: function() {
			$(".datagrid-body").html("<div style='width: 100%; height: 50px; line-height: 50px; text-align: center;font-size: 14px;'>数据加载出错</div>");
		},
		loadFilter: function(data, parentId) {
			if (cutils.notEquals(data.status, 200)) {
				layer.msg(data.message, { icon: 7 });
				return;
			}
			return {
				total: data.data.count,
				rows: data.data.dataList
			};
		}, onClickRow: function(rowIndex, rowData) {
			$('#dict-item-data-table').datagrid("reload", {
				dictId: rowData.dictId
			});
		},
		columns: [[{
			field: 'dictName',
			title: '字典名称',
			width: 200
		}, {
			field: 'dictCode',
			title: '字典编号',
			width: 200,
			align: 'center'
		}, {
			field: 'opts',
			title: '操作',
			width: 200,
			align: 'center',
			formatter: function(value, row, index) {
				let html = '<div title="编辑字典" data-authcode="uums.dict.edit" class="hi-small-btn hi-small-normal-btn hi-transition hi-auth-sign" onclick="doEditDict(' + row.dictId + ');">';
				html += '<i class="fa fa-pencil-square-o" aria-hidden="true"></i>';
				html += '</div>';

				html += '<div title="删除字典" data-authcode="uums.dict.del" class="hi-small-btn hi-small-txt-red-btn hi-transition hi-auth-sign" onclick="doDelDict(' + row.dictId + ');">';
				html += '<i class="fa fa-trash-o" aria-hidden="true"></i>';
				html += '</div>';

				html += '<div title="新增字典值" data-authcode="uums.dict.add.item" class="hi-small-btn hi-small-normal-btn hi-transition hi-auth-sign" onclick="doAddDictItem(' + row.dictId + ');">';
				html += '<i class="fa fa-plus" aria-hidden="true"></i>';
				html += '<span>新增字典值</span>';
				html += '</div>';
				return html;
			}
		}]]
	});
}

self.initDictItemTable = function(dictId) {
	$('#dict-item-data-table').datagrid({
		url: "/" + cutils.webApp + "/dict/item/list?_time=" + cutils.now(),
		method: 'get',
		animate: true,
		fit: true,
		fitColumns: false,
		rownumbers: false,// 行号
		loadMsg: false,
		resizeHandle: 'right',
		singleSelect: true,
		checkOnSelect: false,
		selectOnCheck: false,
		nowrap: false,
		emptyMsg: '无数据',
		pagination: false,
		onLoadSuccess: function(data) {
			cutils.doAuthSign();
		},
		onLoadError: function() {
			$(".datagrid-body").html("<div style='width: 100%; height: 50px; line-height: 50px; text-align: center;font-size: 14px;'>数据加载出错</div>");
		},
		loadFilter: function(data, parentId) {
			if (cutils.notEquals(data.status, 200)) {
				layer.msg(data.message, { icon: 7 });
				return;
			}
			return {
				total: data.data.count,
				rows: data.data.dataList
			};
		},
		queryParams: {
			dictId: dictId
		},
		columns: [[{
			field: 'dictItemName',
			title: '字典值名称',
			width: 180,
			align: 'center'
		}, {
			field: 'dictItemCode',
			title: '字典值',
			width: 180,
			align: 'center'
		}, {
			field: 'opts',
			title: '操作',
			width: 100,
			align: 'center',
			formatter: function(value, row, index) {
				let html = '<div title="编辑字典值" data-authcode="uums.dict.edit" class="hi-small-btn hi-small-normal-btn hi-transition hi-auth-sign" onclick="doEditDictItem(' + row.dictItemId + ');">';
				html += '<i class="fa fa-pencil-square-o" aria-hidden="true"></i>';
				html += '</div>';

				html += '<div title="删除字典值" data-authcode="uums.dict.del" class="hi-small-btn hi-small-txt-red-btn hi-transition hi-auth-sign" onclick="doDelDictItem(' + row.dictItemId + ');">';
				html += '<i class="fa fa-trash-o" aria-hidden="true"></i>';
				html += '</div>';
				return html;
			}
		}]]
	});
}

self.doSearch = function() {
	var queryParams = {};
	queryParams["dictName"] = $("#q-dict-name").val();
	queryParams["dictCode"] = $("#q-dict-code").val();
	$('#dict-data-table').datagrid('load', queryParams);
}

self.doDelDict = function(dictId) {
	layer.confirm('确定删除吗?',
		{
			icon: 3,
			title: '温馨提示',
			btn: ['<i class="fa fa-check" style="color: inherit;"></i>确定', '<i class="fa fa-remove" style="color: inherit;"></i>取消'],
			yes: function(index) {
				layer.close(index);
				$.ajax({
					type: "delete",
					url: "/" + cutils.webApp + "/dict/" + dictId + "?_time=" + cutils.now(),
					dataType: 'json',
					success: function(result) {
						if (cutils.notEquals(result.status, 200)) {
							layer.msg(result.message, { icon: 7 });
							return;
						}
						layer.msg(result.message, { icon: 1, time: 1000 });
						doSearch();
					}
				});
			},
			cancel: function(index) {
				layer.close(index);
			}
		});
}

self.doAddDict = function() {
	top.layer.open({
		type: 2,
		title: '新增参数',
		area: ['600px', '300px'],
		shade: 0.3,
		maxmin: true,
		content: "/" + cutils.webApp + "/views/dict/dict-add.html?&_time=" + cutils.now(),
		btn: ['<i class="fa fa-check" style="color: inherit;"></i>保存', '<i class="fa fa-remove" style="color: inherit;"></i>取消'],
		yes: function(index, layero) {
			var iframeWin = layero.find('iframe')[0];
			iframeWin.contentWindow.doSubmitHandler(top, window);
		},
		cancel: function(index) {
			top.layer.close(index);
		}
	});
}

self.doEditDict = function(dictId) {
	top.layer.open({
		type: 2,
		title: '编辑参数',
		area: ['600px', '400px'],
		shade: 0.3,
		maxmin: true,
		content: "/" + cutils.webApp + "/views/dict/dict-edit.html?dictId=" + dictId + "&_time=" + cutils.now(),
		btn: ['<i class="fa fa-check" style="color: inherit;"></i>保存', '<i class="fa fa-remove" style="color: inherit;"></i>取消'],
		yes: function(index, layero) {
			var iframeWin = layero.find('iframe')[0];
			iframeWin.contentWindow.doSubmitHandler(top, window);
		},
		cancel: function(index) {
			top.layer.close(index);
		}
	});
}

/////////////////////////////////////////////////////////////////////
self.doDictItemSearch = function() {
	$('#dict-item-data-table').datagrid("reload");
}

self.doDelDictItem = function(dictItemId) {
	layer.confirm('确定删除吗?',
		{
			icon: 3,
			title: '温馨提示',
			btn: ['<i class="fa fa-check" style="color: inherit;"></i>确定', '<i class="fa fa-remove" style="color: inherit;"></i>取消'],
			yes: function(index) {
				layer.close(index);
				$.ajax({
					type: "delete",
					url: "/" + cutils.webApp + "/dict/item/" + dictItemId + "?_time=" + cutils.now(),
					dataType: 'json',
					success: function(result) {
						if (cutils.notEquals(result.status, 200)) {
							layer.msg(result.message, { icon: 7 });
							return;
						}
						layer.msg(result.message, { icon: 1, time: 1000 });
						doDictItemSearch();
					}
				});
			},
			cancel: function(index) {
				layer.close(index);
			}
		});
}

self.doAddDictItem = function(dictId) {
	top.layer.open({
		type: 2,
		title: '新增参数',
		area: ['600px', '300px'],
		shade: 0.3,
		maxmin: true,
		content: "/" + cutils.webApp + "/views/dict/dict-item-add.html?dictId=" + dictId + "&_time=" + cutils.now(),
		btn: ['<i class="fa fa-check" style="color: inherit;"></i>保存', '<i class="fa fa-remove" style="color: inherit;"></i>取消'],
		yes: function(index, layero) {
			var iframeWin = layero.find('iframe')[0];
			iframeWin.contentWindow.doSubmitHandler(top, window);
		},
		cancel: function(index) {
			top.layer.close(index);
		}
	});
}

self.doEditDictItem = function(dictItemId) {
	top.layer.open({
		type: 2,
		title: '编辑参数',
		area: ['600px', '400px'],
		shade: 0.3,
		maxmin: true,
		content: "/" + cutils.webApp + "/views/dict/dict-item-edit.html?dictItemId=" + dictItemId + "&_time=" + cutils.now(),
		btn: ['<i class="fa fa-check" style="color: inherit;"></i>保存', '<i class="fa fa-remove" style="color: inherit;"></i>取消'],
		yes: function(index, layero) {
			var iframeWin = layero.find('iframe')[0];
			iframeWin.contentWindow.doSubmitHandler(top, window);
		},
		cancel: function(index) {
			top.layer.close(index);
		}
	});
}