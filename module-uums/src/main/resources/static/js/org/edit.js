import cutils from '../../js/common/core-utils.js';

$(function() {
	initData();
});

self.initData = function() {
	var vars = cutils.getUrlVars();
	var orgId = vars["orgId"];

	$.ajax({
		type: "get",
		url: "/" + cutils.webApp + "/org/" + orgId + "?_time=" + cutils.now(),
		data: $('#q-data-form').serialize(),
		dataType: 'json',
		success: function(result) {
			if (cutils.notEquals(result.status, 200)) {
				layer.msg(result.message, { icon: 7 });
				return;
			}

			$("#q-id").val(result.data.orgId);
			$("#q-name").val(result.data.name);
			$("#q-p-id").val(result.data.orgPId);
			$("#q-order").val(result.data.order);

			if (result.data.orgPName != undefined) {
				$("#q-p-name").val(result.data.orgPName);
			}
			$("#q-desc").val(result.data.desc);
		}
	});
}

self.doSubmitHandler = function(layerFromWin, pageFromWindow) {
	var orgName = $("#q-name").val();
	var order = $("#q-order").val();

	if (cutils.isEmpty(orgName)) {
		layer.tips('请输入部门名称', '#q-name', {
			tips: 1
		});
		return;
	}

	if (cutils.isNotEmpty(order) && cutils.isNotPositiveInteger(order)) {
		layer.tips('序号只能是正整数', '#q-order', {
			tips: 1
		});
		return;
	}


	$.ajax({
		type: "put",
		url: "/" + cutils.webApp + "/org?_time=" + cutils.now(),
		data: $('#q-data-form').serialize(),
		dataType: 'json',
		success: function(result) {
			if (cutils.equals(result.status, 200)) {
				layerFromWin.layer.msg(result.message, { icon: 1, time: 1000 });
				var indexFrame = layerFromWin.layer.getFrameIndex(window.name);
				layerFromWin.layer.close(indexFrame);
				pageFromWindow.doSearch();
				return;
			}
			layerFromWin.layer.msg(result.message, { icon: 7 });
		}
	});
}

self.doSelectOrg = function() {
	top.layer.open({
		type: 2,
		title: '选择部门',
		area: ['320px', '450px'],
		shade: 0.01,
		maxmin: true,
		shadeClose: true,
		content: "/" + cutils.webApp + "/views/org/select.html?singleSelect=true&orgId=" + $("#q-p-id").val() + "?_time=" + cutils.now(),
		btn: ['<i class="fa fa-check" style="color: inherit;"></i>确定', '<i class="fa fa-eraser" style="color: inherit;"></i>清除', '<i class="fa fa-remove" style="color: inherit;"></i>取消'],
		yes: function(index, layero) {
			var iframeWin = layero.find('iframe')[0];
			var data = iframeWin.contentWindow.getChecked();

			if (cutils.isEmpty(data)) {
				iframeWin.contentWindow.layer.msg("请选择上级部门", { icon: 7 });
				return;
			}

			$("#q-p-id").val(data.orgId);
			$("#q-p-name").val(data.name);
			top.layer.close(index);
		},
		btn2: function(index, layero) {
			$("#q-p-id").val("");
			$("#q-p-name").val("");
			top.layer.close(index);

		}, btn3: function(index, layero) {
			top.layer.close(index);
		},
		cancel: function(index) {
			top.layer.close(index);
		}
	});
}