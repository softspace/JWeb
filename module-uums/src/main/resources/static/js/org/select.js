import cutils from '../../js/common/core-utils.js';

$(function() {
	$("#q-org-name").focus();

	var vars = cutils.getUrlVars();
	self.singleSelect = (cutils.getVars(vars, "singleSelect") == "true" ? true : false);

	initTable();
	$(document).keydown(function() {
		if (event.keyCode == 13) {
			doSearch();
		}
	});
});

self.initTable = function() {
	$('#data-table').treegrid({
		url: "/" + cutils.webApp + "/org/list?_time=" + cutils.now(),
		method: 'get',
		idField: "orgId",
		treeField: "name",
		animate: true,
		fit: true,
		fitColumns: true,
		pagination: false,// 分页控件
		rownumbers: false,// 行号
		loadMsg: false,
		singleSelect: self.singleSelect,
		emptyMsg: '无数据',
		onLoadSuccess: function(data) {
			if (self.singleSelect) {
				$(".datagrid-header-check input").hide();
				$(".datagrid-header-check label").hide();
			}

			var vars = cutils.getUrlVars();
			var orgId = cutils.getVars(vars, "orgId");
			if (cutils.isEmpty(orgId)) {
				return;
			}
			$.each(orgId.split(","), function(i, node) {
				$('#data-table').treegrid("select", node);
			});
		},
		onLoadError: function() {
			$(".datagrid-body").html("<div style='width: 100%; height: 50px; line-height: 50px; text-align: center;font-size: 14px;'>数据加载出错</div>");
		},
		loadFilter: function(data, parentId) {
			if (cutils.notEquals(data.status, 200)) {
				layer.msg(data.message, { icon: 3 });
				return;
			}
			return doOrgToTree(data.data.dataList);
		},
		queryParams: {
			"status": 0
		},
		columns: [[{
			field: 'ck',
			checkbox: true
		}, {
			field: 'orgId',
			hidden: true
		}, {
			field: 'name',
			title: '部门名称',
			width: 350
		}]]
	});
}

self.doOrgToTree = function(list) {
	$.each(list, function(i, item) {
		item.hasParent = false;
	});

	$.each(list, function(i, item) {
		item.children = [];
		$.each(list, function(i, subItem) {
			if (item.orgId == subItem.orgPId) {
				subItem.hasParent = true;
				item.children.push(subItem);
			}
		});
	});

	var treeList = [];
	$.each(list, function(i, item) {
		if (item.children.length == 0) {
			item.iconCls = "fa-dot-circle-o";
		} else {
			//item.iconCls = "fa-sitemap";
		}
		if (!item.hasParent) {
			treeList.push(item);
		}
	});
	return treeList;
}

self.doSearch = function() {
	var queryParams = {};
	queryParams["name"] = $("#q-org-name").val();
	queryParams["status"] = 0;
	$('#data-table').treegrid('load', queryParams);
}

self.getChecked = function() {
	var rows = $('#data-table').treegrid('getChecked');
	if (self.singleSelect) {
		return rows == null ? null : rows[0];
	}
	return rows;
}
