import cutils from '../../js/common/core-utils.js';
import '../../js/common/table-col-utils.js';

$(function() {
	initTable();

	$(document).keydown(function() {
		if (event.keyCode == 13) {
			doSearch();
		}
	});

	cutils.doAuthSign();
});

self.initTable = function() {
	$('#data-table').treegrid({
		url: "/" + cutils.webApp + "/org/list?_time=" + cutils.now(),
		method: 'get',
		idField: "orgId",
		treeField: "name",
		animate: true,
		fit: true,
		fitColumns: false,
		pagination: false,// 分页控件
		rownumbers: false,// 行号
		loadMsg: false,
		resizeHandle: 'right',
		singleSelect: false,
		checkOnSelect: false,
		selectOnCheck: false,
		emptyMsg: '无数据',
		onLoadSuccess: function(data) {
			layui.form.render();
			$('#data-table').treegrid('unselectAll');
			$('#data-table').treegrid('uncheckAll');
		    cutils.doAuthSign();
		},
		onLoadError: function() {
			$(".datagrid-body").html("<div style='width: 100%; height: 50px; line-height: 50px; text-align: center;font-size: 14px;'>数据加载出错</div>");
		},
		loadFilter: function(data, parentId) {
			if (cutils.notEquals(data.status, 200)) {
				layer.msg(data.message, { icon: 3 });
				return;
			}
			return doOrgToTree(data.data.dataList);
		},
		columns: [[{
			field: 'name',
			title: '部门名称',
			width: 350
		}, {
			field: 'desc',
			title: '备注信息',
			width: 200
		}, {
			field: 'order',
			title: '排序号',
			align: 'center',
			width: 80
		}, {
			field: 'updatetime',
			title: '更新时间',
			width: 150,
			align: 'center',
			formatter: function(value, row, index) {
				return value.length > 16 ? value.substring(0, 16) : value;
			}
		}, {
			field: 'status',
			title: '是否禁用',
			align: 'center',
			width: 100,
			formatter: function(value, row, index) {
				return '<span id="org-status-span-' + row.orgId + '" class="layui-form"><input tipId="#org-status-span-' + row.orgId + '" type="checkbox" name="org-status" orgId="' + row.orgId + '" title="禁用" lay-filter="org-status" ' + (row.status == 1 ? 'checked' : '') + '></span>';
			}
		}, {
			field: 'opts',
			title: '操作',
			width: 140,
			align: 'center',
			formatter: function(value, row, index) {
				let html = '<div title="编辑部门" data-authcode="uums.org.edit" class="hi-small-btn hi-small-normal-btn hi-transition hi-auth-sign" onclick="doEditOrg(' + row.orgId + ');">';
				html += '<i class="fa fa-pencil-square-o" aria-hidden="true"></i>';
				//html +='<span>编辑</span>';
				html += '</div>';

				html += '<div title="新增下级部门" data-authcode="uums.org.add" class="hi-small-btn hi-small-normal-btn hi-transition hi-auth-sign" onclick="doAddOrg(' + row.orgId + ');">';
				html += '<i class="fa fa-plus" aria-hidden="true"></i>';
				//html +='<span>新增</span>';
				html += '</div>';

				html += '<div title="删除部门" data-authcode="uums.org.del" class="hi-small-btn hi-small-txt-red-btn hi-transition hi-auth-sign" onclick="doDelOrg(' + row.orgId + ');">';
				html += '<i class="fa fa-trash-o" aria-hidden="true"></i>';
				// html +='<span>删除</span>';
				html += '</div>';
				return html;
			}
		}]]
	});

	layui.form.on('checkbox(org-status)', function(obj) {
		if (cutils.hasNotAuthCode("uums.org.edit")) {
			$(obj.elem).prop("checked", !obj.elem.checked);
			layui.form.render();
			layer.tips('没有操作权限', $(obj.elem).attr("tipId"), {
				tips: 1,
				time: 1000
			});
			return false;
		}

		doEidtOrgStatus(obj);
	});
}

self.doOrgToTree = function(list) {
	$.each(list, function(i, item) {
		item.hasParent = false;
	});

	$.each(list, function(i, item) {
		item.children = [];
		$.each(list, function(i, subItem) {
			if (item.orgId == subItem.orgPId) {
				subItem.hasParent = true;
				item.children.push(subItem);
			}
		});
	});

	var treeList = [];
	$.each(list, function(i, item) {
		if (item.children.length == 0) {
			item.iconCls = "fa-dot-circle-o";
		} else {
			//item.iconCls = "fa-sitemap";
		}
		if (!item.hasParent) {
			treeList.push(item);
		}
	});
	return treeList;
}

self.doSearch = function() {
	var unlock = $("#q-status-unlock").is(":checked");
	var lock = $("#q-status-lock").is(":checked");

	var queryParams = {};
	queryParams["name"] = $("#q-org-name").val();
	queryParams["status"] = unlock && lock ? "" : !unlock && !lock ? "" : unlock ? 0 : 1;
	$('#data-table').treegrid('load', queryParams);
}

self.doEidtOrgStatus = function(obj) {
	var params = {
		"orgId": $(obj.elem).attr("orgId"),
		"status": obj.elem.checked ? 1 : 0,
	};

	$.ajax({
		type: "put",
		url: "/" + cutils.webApp + "/org/status?_time=" + cutils.now(),
		data: params,
		dataType: 'json',
		sucess: function(result) {
			if (cutils.equals(result.status, 200)) {
				layer.msg(result.message, { icon: 1, time: 1000 });
				return;
			}

			$(obj.elem).prop("checked", !obj.elem.checked);
			layui.form.render();
			layer.msg(result.message, { icon: 7 });
		},
		nerror: function(e) {
			$(obj.elem).prop("checked", !obj.elem.checked);
			layui.form.render();
		}
	});
}

self.doDelOrg = function(orgId) {
	layer.confirm('确定删除吗?',{
			icon: 3,
			title: '温馨提示',
			btn: ['<i class="fa fa-check" style="color: inherit;"></i>确定', '<i class="fa fa-remove" style="color: inherit;"></i>取消'],
			yes: function(index) {
				layer.close(index);
				
				$.ajax({
					type: "delete",
					url: "/" + cutils.webApp + "/org/" + orgId + "?_time=" + cutils.now(),
					dataType: 'json',
					success: function(result) {
						if (cutils.notEquals(result.status, 200)) {
							layer.msg(result.message, { icon: 7 });
							return;
						}
						layer.msg(result.message, { icon: 1, time: 1000 });
						doSearch();
					}
				});
			},
			cancel: function(index) {
				layer.close(index);
			}
		});
}

self.doAddOrg = function(orgPId) {
	top.layer.open({
		type: 2,
		title: '新增部门',
		area: ['600px', '400px'],
		shade: 0.3,
		maxmin: true,
		content: "/" + cutils.webApp + "/views/org/add.html?orgPId=" + orgPId + "&_time=" + cutils.now(),
		btn: ['<i class="fa fa-check" style="color: inherit;"></i>保存', '<i class="fa fa-remove" style="color: inherit;"></i>取消'],
		yes: function(index, layero) {
			var iframeWin = layero.find('iframe')[0];
			iframeWin.contentWindow.doSubmitHandler(top, window);
		},
		cancel: function(index) {
			top.layer.close(index);
		}
	});
}

self.doEditOrg = function(orgId) {
	top.layer.open({
		type: 2,
		title: '编辑部门',
		area: ['600px', '400px'],
		shade: 0.3,
		maxmin: true,
		content: "/" + cutils.webApp + "/views/org/edit.html?orgId=" + orgId + "&_time=" + cutils.now(),
		btn: ['<i class="fa fa-check" style="color: inherit;"></i>保存', '<i class="fa fa-remove" style="color: inherit;"></i>取消'],
		yes: function(index, layero) {
			var iframeWin = layero.find('iframe')[0];
			iframeWin.contentWindow.doSubmitHandler(top, window);
		},
		cancel: function(index) {
			top.layer.close(index);
		}
	});
}