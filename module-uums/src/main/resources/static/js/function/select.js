import cutils from '../../js/common/core-utils.js';

$(function() {
	$("#q-function-name").focus();

	var vars = cutils.getUrlVars();
	self.singleSelect = (cutils.getVars(vars, "singleSelect") == "true" ? true : false);

	initTable();
	$(document).keydown(function() {
		if (event.keyCode == 13) {
			doSearch();
		}
	});
});

self.initTable = function() {
	$('#data-table').treegrid({
		url: "/" + cutils.webApp + "/function/list?_time=" + cutils.now(),
		method: 'get',
		idField: "functionId",
		treeField: "name",
		animate: true,
		fit: true,
		fitColumns: true,
		pagination: false,// 分页控件
		rownumbers: false,// 行号
		loadMsg: false,
		singleSelect: self.singleSelect,
		emptyMsg: '无数据',
		onLoadSuccess: function(data) {
			if (self.singleSelect) {
				$(".datagrid-header-check input").hide();
				$(".datagrid-header-check label").hide();
			}

			var vars = cutils.getUrlVars();
			var functionId = cutils.getVars(vars, "functionId");
			if (cutils.isEmpty(functionId)) {
				return;
			}

			$.each(functionId.split(","), function(i, node) {
				$('#data-table').treegrid("select", node);
			});

			var roots = $('#data-table').treegrid("getRoots");
			doCollapse(null, roots);
		},
		onLoadError: function() {
			$(".datagrid-body").html("<div style='width: 100%; height: 50px; line-height: 50px; text-align: center;font-size: 14px;'>数据加载出错</div>");
		},
		loadFilter: function(data, parentId) {
			if (cutils.notEquals(data.status, 200)) {
				layer.msg(data.message, { icon: 3 });
				return;
			}
			return doFunctionToTree(data.data.dataList);
		},
		queryParams: {
			"status": 0
		},
		columns: [[{
			field: 'ck',
			checkbox: true
		}, {
			field: 'functionId',
			hidden: true
		}, {
			field: 'name',
			title: '功能名称',
			width: 350
		}]]
	});
}

self.doFunctionToTree = function(list) {
	$.each(list, function(i, item) {
		item.hasParent = false;
	});

	$.each(list, function(i, item) {
		item.children = [];
		$.each(list, function(i, subItem) {
			if (item.functionId == subItem.functionPId) {
				subItem.hasParent = true;
				item.children.push(subItem);
			}
		});
	});

	var treeList = [];
	$.each(list, function(i, item) {
		item.iconCls = item.icon;
		if (item.icon == null || item.icon == "") {
			item.iconCls = "fa-paper-plane-o";
		}
		item.iconCls = "fa " + item.iconCls;

		if (item.type == 1) {//权限类型图标
			item.iconCls = "fa-dot-circle-o";
		}

		if (!item.hasParent) {
			treeList.push(item);
		}
	});
	return treeList;
}

self.doSearch = function() {
	var queryParams = {};
	queryParams["name"] = $("#q-function-name").val();
	queryParams["status"] = 0;
	$('#data-table').treegrid('load', queryParams);
}

self.getChecked = function() {
	var rows = $('#data-table').treegrid('getChecked');
	if (self.singleSelect) {
		return rows == null ? null : rows[0];
	}
	return rows;
}

self.doCollapse = function(node, childrens) {
	var flag = true;
	$.each(childrens, function(i, item) {
		if (item.type == 0) {
			flag = false;
			doCollapse(item, $('#data-table').treegrid("getChildren", item.functionId));
		}
	});
	if (node == null) {
		return;
	}
	if (flag) {
		$('#data-table').treegrid("collapse", node.functionId);
	}
}
