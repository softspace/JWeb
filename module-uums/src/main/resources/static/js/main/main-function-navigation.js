import cutils from '../common/core-utils.js';

self.initFunctionNavigation = function() {
	var userData = cutils.getUserData();

	var functionHtml = "";
	$.each(userData.functionTree, function(i, item) {
		if (item.children && item.children.length > 0) {
			functionHtml += '<div style="width: 100%; height: 30px; line-height: 30px; font-weight: bold;">';
			functionHtml += '<span style="padding-left: 10px; color: #0279B5;">' + item.name + '</span>';
			functionHtml += '</div>';
			functionHtml += '<div class="common-functions">';
			functionHtml += doSubFunctionNavigation(item.children);
			functionHtml += '</div>';
		}
	});
	$("#function-navigation-box-inner").append(functionHtml);
}

self.doSubFunctionNavigation = function(menuJson) {
	var functionHtml = "";
	$.each(menuJson, function(i, item) {
		if (item.children && item.children.length > 0) {
			functionHtml += doSubFunctionNavigation(item.children);
		} else {
			functionHtml += '<div class="common-functions-item" url="' + menuJson.url + '" onclick="triggerOpenMenu(\'' + 1 + '\')">';
			functionHtml += '<div class="common-functions-item-ico">';
			functionHtml += '<i class="fa ' + item.icon + '" aria-hidden="true"></i>';
			functionHtml += '</div>';
			functionHtml += '<div class="common-functions-item-label">';
			functionHtml += '<span>' + item.name + '</span>';
			functionHtml += '</div>';
			functionHtml += '</div>';
		}
	});
	return functionHtml;
}

let funcNavIsMouseOut = false;
let funcNavBoxTimeout = null;
let funcNavForceClose = false;
let funcNavIsShow = false;

self.functionNavigation = function() {
	if (funcNavIsShow) {
		return;
	}
	funcNavIsShow = true;

	var ww = $(window).width();
	var bw = $("#function-navigation-box").width();
	var left = $(".menu-more-func").offset().left;

	if ((left + bw) > ww) {
		left = ww - bw + 30;
	}

	$("#function-navigation-box").css("left", left - 50);
	$("#function-navigation-box").css("height", "400px");
	$("#function-navigation-box").css("border", "1px solid #ccc;");
	$("#function-navigation-box").css("padding", "10px 0px 10px 0px");

	$("#function-navigation-box").bind("mouseleave.funcNav", function(e) {
		funcNavIsMouseOut = true;
		dofuncNavBoxTimeout();
	});

	$(window).bind("mousemove.funcNav", function(e) {
		doHidefuncNavBox(e);
	});
}

self.doHidefuncNavBox = function(e) {
	var e = e || window.event;
	var elem = e.target;

	if (($(elem).is('.menu-more-func') || $(elem).is('.menu-more-func *'))) {
		funcNavIsMouseOut = false;
		return;
	}

	if (($(elem).is('#function-navigation-box') || $(elem).is('#function-navigation-box *'))) {
		funcNavIsMouseOut = false;
		return;
	}

	funcNavIsMouseOut = true;
	dofuncNavBoxTimeout();
}

self.dofuncNavBoxTimeout = function() {
	if (funcNavBoxTimeout != null) {
		return;
	}

	funcNavBoxTimeout = setTimeout(function() {
		funcNavBoxTimeout = null;
		if (funcNavIsMouseOut || funcNavForceClose) {
			$(window).unbind("mousemove.funcNav");
			$(window).unbind("click.funcNav");
			$("#function-navigation-box").unbind("mouseleave.funcNav");
			$("#function-navigation-box").css("height", "0px");
			$("#function-navigation-box").css("border", "0px");
			$("#function-navigation-box").css("padding", "0px");
			funcNavIsShow = false;
		}
	}, 500);
}