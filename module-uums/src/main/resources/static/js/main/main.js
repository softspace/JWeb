import cutils from '../common/core-utils.js';
import menuTree from './main-menu-tree.js';
import './main-user.js';
import './main-function-navigation.js';

$(function() {
	//检查token
	var token = cutils.getToken();

	if (cutils.isEmpty(token)) {
		$("#center-layout-0-container").show();
		var callback = function(index) {
			layer.close(index);
			top.location.href = "/" + cutils.webApp + "/views/login.html";
		}
		top.layer.alert("没有检测到登录信息，将跳转到登录页面", {
			icon: 7,
			title: '温馨提示',
			yes: callback,
			cancel: callback
		});
		return;
	}

	$("#full-screen").click(function() {
		triggerFullscreen();
	});

	$("#hi-menu-tree-slim-scroll").slimScroll({
		width: '100%',
		height: '100%',
		color: '#000',
		size: '3px',
		animate: true
	});

	$("#function-navigation-box-inner").slimScroll({
		width: '100%',
		height: '100%',
		color: '#000',
		size: '3px',
		animate: true
	});

	var sysInfo = cutils.getSysData();

	if (cutils.isNotEmpty(sysInfo)) {
		document.title = sysInfo.sysName;
		$("#sys-name").html(sysInfo.sysName);
	}

	$.ajax({
		type: "get",
		url: "/" + cutils.webApp + "/user/info?_time=" + cutils.now(),
		async: true,
		dataType: 'json',
		success: function(result) {
			if (cutils.notEquals(result.status, 200)) {
				layer.msg(result.message, { icon: 7 });
				return;
			}

			var fullname = result.data.user.fullname;
			var username = result.data.user.username;

			$("#user-name-label").html(cutils.isEmpty(fullname) ? username : fullname);

			result.data.functionTree = doFunctionToTree(result.data.functionList);

			cutils.setUserData(result.data);

			initSystemBar();

			fireBarMenu($("#sys-bar-inner-container").children("div:first-child"));

			initUserDropdown();

			initFunctionNavigation();
		}
	});
});

self.triggerToggler = function() {
	var rotateDeg = "0deg";

	if ($(".center-layout-1-right-container").hasClass("center-layout-1-right-max-width-container")) {
		$(".center-layout-1-right-container").removeClass("center-layout-1-right-max-width-container");
		$(".center-layout-1-left-container").removeClass("center-layout-1-left-min-width-container");
	} else {
		$(".center-layout-1-left-container").addClass("center-layout-1-left-min-width-container");
		$(".center-layout-1-right-container").addClass("center-layout-1-right-max-width-container");
		rotateDeg = "180deg";
	}

	$('.ui-layout-toggler i').css("transform", "rotate(" + rotateDeg + ")");
	$('.ui-layout-toggler i').css("-ms-transform", "rotate(" + rotateDeg + ")");
	$('.ui-layout-toggler i').css("-moz-transform", "rotate(" + rotateDeg + ")");
	$('.ui-layout-toggler i').css("-webkit-transform", "rotate(" + rotateDeg + ")");
	$('.ui-layout-toggler i').css("-o-transform", "rotate(" + rotateDeg + ")");
}

self.doFunctionToTree = function(list) {
	var listTemp = [];
	$.each(list, function(i, item) {
		item.hasParent = false;
		if (item.type == 0) {
			listTemp[listTemp.length] = item;
		}
	});

	$.each(listTemp, function(i, item) {
		item.children = [];
		$.each(listTemp, function(si, subItem) {
			if (item.functionId == subItem.functionPId) {
				subItem.hasParent = true;
				item.children.push(subItem);
			}
		});
	});

	var treeList = [];
	$.each(listTemp, function(i, item) {
		item.iconCls = item.icon;
		if (item.icon == null || item.icon == "") {
			item.iconCls = "fa-paper-plane-o";
		}
		item.iconCls = "fa " + item.iconCls;

		if (!item.hasParent) {
			treeList.push(item);
		}
	});
	return treeList;
}

self.initSystemBar = function() {
	var userData = cutils.getUserData();

	var menubar = '';
	$.each(userData.functionTree, function(i, item) {
		if (item.children && item.children.length > 0) {
			menubar += '<div class="menu menu-func" data-id="' + item.functionId + '" data-type="1">';
		} else {
			menubar += '<div class="menu menu-func" data-id="' + item.functionId + '" data-type="0" data-url="' + item.url + '">';
		}
		menubar += '<div class="menu-ico">';
		menubar += '<i class="fa ' + item.icon + '" aria-hidden="true"></i>';
		menubar += '</div>';
		menubar += '<div class="menu-label">' + item.name + '</div>';
		menubar += '</div>';
	});

	menubar += '<div class="menu menu-more-func" data-id="menu-function-navicon" data-type="2">';
	menubar += '<div class="menu-ico">';
	menubar += '<i class="fa fa-navicon" aria-hidden="true"></i>';
	menubar += '</div>';
	menubar += '<div class="menu-label">功能导航</div>';
	menubar += '</div>';

	$("#sys-bar-inner-container").html(menubar);

	//单击事件，若没有就打开，已经打开就不做处理
	$(".menu-func").bind("click", function() {
		fireBarMenu($(this));
	});

	//双击重新打开
	$(".menu-func").bind("dblclick", function() {
		if ($(this).hasClass("menu-active")) {
			$(this).removeClass("menu-active");
		}
		fireBarMenu($(this));
	});

	//单击事件，若没有就打开，已经打开就不做处理
	$(".menu-more-func").bind("click", function() {
		functionNavigation();
	});
}

self.fireBarMenu = function(_this) {
	if (_this.hasClass("menu-active")) {
		return;
	}

	if ($(".menu-active").length > 0) {
		$(".menu-active").removeClass("menu-active");
	}

	_this.addClass("menu-active");

	var type = _this.data("type");

	if (type == "0") {
		doLayout_0_(_this);
		return;
	}

	if (type == "1") {
		doLayout_1_(_this);
		return;
	}
}
self.doLayout_0_ = function(_this) {
	_this.addClass("menu-active");

	$(".center-layout-container").hide();
	$("#center-layout-0-container").show();
	var url = _this.data("url");

	$("#center-layout-0-container").empty();
	$("#center-layout-0-container").html('<iframe id="center-layout-0-iframe" class="iframe" src="' + url + '"></iframe>');
}

self.doLayout_1_ = function(_this) {
	_this.addClass("menu-active");

	$(".center-layout-container").hide();
	$("#center-layout-1-container").show();

	var functionId = _this.data("id");

	var userData = cutils.getUserData();

	$.each(userData.functionTree, function(i, item) {
		if (item.functionId == functionId) {
			menuTree.init({
				expendAll: false,
				openOneOlny: false,
				data: item.children
			});
		}
	});
}

self.logout = function() {
	layer.confirm('确定退出吗?',
		{
			icon: 3,
			title: '温馨提示',
			btn: ['<i class="fa fa-check" style="color: inherit;"></i>确定', '<i class="fa fa-remove" style="color: inherit;"></i>取消']
		},
		function(index) {
			layer.close(index);
			doLogout();
		});
}

self.doLogout = function() {
	$.ajax({
		type: "delete",
		url: "/" + cutils.webApp + "/common/logout?_time=" + cutils.now(),
		async: true,
		dataType: 'json',
		success: function(result) {
			 if (cutils.notEquals(result.status, 200)) {
				layer.msg(result.message, { icon: 3 });
				return;
			}

			cutils.delToken();
			top.location.href = "/" + cutils.webApp + "/views/login.html";
		}
	});
}

self.triggerFullscreen = function() {
	var fullscreen = parseInt($("#full-screen").data("fullscreen"));
	if (fullscreen == 0) {
		doFullScreen();
		$("#full-screen").data("fullscreen", 1);
	}
	if (fullscreen == 1) {
		doExitFullscreen();
		$("#full-screen").data("fullscreen", 0);
	}
}

