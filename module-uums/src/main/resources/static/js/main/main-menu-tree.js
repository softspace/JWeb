/** 树形菜单 */
class MenuTree {
	constructor() {
	}
	init(opt) {
		this.opt = opt;

		$("#center-layout-1-iframes").empty();
		$("#hi-menu-tree-slim-scroll").empty();

		$("#hi-menu-tree-slim-scroll").html(this.doHTree("", 0, this.opt.data));
		this.doActiveFristNodeMenu("", this.opt.data[0]);

		if (this.opt.expendAll) {
			$(".hi-menu-tree-children").show();
			let rotateDeg = "-90deg";
			$('.hi-menu-tree-right-ico i').css("transform", "rotate(" + rotateDeg + ")");
			$('.hi-menu-tree-right-ico i').css("-ms-transform", "rotate(" + rotateDeg + ")");
			$('.hi-menu-tree-right-ico i').css("-moz-transform", "rotate(" + rotateDeg + ")");
			$('.hi-menu-tree-right-ico i').css("-webkit-transform", "rotate(" + rotateDeg + ")");
			$('.hi-menu-tree-right-ico i').css("-o-transform", "rotate(" + rotateDeg + ")");
		}
	}


	triggerChildrens(i) {
		$("#hi-menu-tree-mask").show();

		setTimeout(function() {
			$("#hi-menu-tree-mask").hide();
		}, 500);

		if (this.opt.openOneOlny) {
			var id = 'hi-menu-tree-children-' + i;
			$(".hi-menu-tree-children").each(function() {
				if ($(this).is(":hidden")) {
					return true
				}
				if (id == $(this).attr("id")) {
					return true
				}
				this.doTriggerChildrens($(this).attr("id").substring(22));
			});
		}

		this.doTriggerChildrens(i);
	}

	doTriggerChildrens(i) {
		let rotateDeg = "0deg";
		if ($('#hi-menu-tree-children-' + i).is(":hidden")) {
			rotateDeg = "-90deg";
		}

		$('#hi-menu-tree-right-ico-' + i).css("transform", "rotate(" + rotateDeg + ")");
		$('#hi-menu-tree-right-ico-' + i).css("-ms-transform", "rotate(" + rotateDeg + ")");
		$('#hi-menu-tree-right-ico-' + i).css("-moz-transform", "rotate(" + rotateDeg + ")");
		$('#hi-menu-tree-right-ico-' + i).css("-webkit-transform", "rotate(" + rotateDeg + ")");
		$('#hi-menu-tree-right-ico-' + i).css("-o-transform", "rotate(" + rotateDeg + ")");

		$('#hi-menu-tree-children-' + i).slideToggle("slow");
	}

	doActiveFristNodeMenu(p, menuJson) {
		let flag = (menuJson.children == null || menuJson.children.length == 0);
		p = p + (p.length == 0 ? "" : "-") + "" + 0;

		$("#hi-menu-tree-item-" + p).trigger("click");
		if (flag) {
			return;
		}
		this.doActiveFristNodeMenu(p, menuJson.children[0]);
	}

	triggerOpenMenu(i) {
		let item = $("#hi-menu-tree-item-" + i);
		if (item.hasClass("hi-menu-tree-item-active")) {
			return;
		}

		if ($(".hi-menu-tree-item-active").length > 0) {
			$(".hi-menu-tree-item-active").removeClass("hi-menu-tree-item-active");
		}

		item.addClass("hi-menu-tree-item-active");

		let iframeId = "center-layout-1-iframe-" + i;

		//隐藏之前的iframe
		$(".center-layout-1-iframe").each(function() {
			if ($(this).is(":hidden")) {
				return true
			}
			if (iframeId == $(this).attr("id")) {
				return true
			}
			$(this).hide();
		});

		if ($("#" + iframeId).length == 0) {
			let iframe = '<iframe id="' + iframeId + '" class="iframe center-layout-1-iframe" src="' + item.attr("url") + '"></iframe>';
			$("#center-layout-1-iframes").append(iframe);
		}

		if ($("#" + iframeId).is(":hidden")) {
			$("#" + iframeId).fadeToggle(300);
		}
	}

	//重新加载页面
	triggerReOpenMenu(i) {
		let iframeId = "center-layout-1-iframe-" + i;
		if ($("#" + iframeId).length > 0) {
			$("#" + iframeId).remove();
		}

		let item = $("#hi-menu-tree-item-" + i);

		if (item.hasClass("hi-menu-tree-item-active")) {
			$(".hi-menu-tree-item-active").removeClass("hi-menu-tree-item-active");
		}

		this.triggerOpenMenu(i);
	}

	doHTree(p, l, menuJson) {
		let html = '';
		for (let i = 0; i < menuJson.length; i++) {
			html += this.doHTree0(p + (p.length == 0 ? "" : "-") + "" + i, l, menuJson[i]);
		}
		return html;
	}

	doHTree0(p, l, menuJson) {
		let flag = (menuJson.children == null || menuJson.children.length == 0);

		let html = '';
		html += '<div class="hi-menu-tree-item-warp" id="hi-menu-tree-item-warp-' + p + '">';
		if (flag) {
			html += '<div class="hi-menu-tree-item" id="hi-menu-tree-item-' + p + '" url="' + menuJson.url + '" ondblclick="triggerReOpenMenu(\'' + p + '\')" onclick="triggerOpenMenu(\'' + p + '\')"  style="' + this.boldStyle(l) + '">';
		} else {
			html += '<div class="hi-menu-tree-item" id="hi-menu-tree-item-' + p + '" onclick="triggerChildrens(\'' + p + '\')" style="' + this.boldStyle(l) + '">';
		}

		html += '<div class="hi-menu-tree-ico" style="margin-left: ' + (8 * l) + 'px;">';
		html += '<i class="fa ' + menuJson.icon + '" aria-hidden="true"></i>';
		html += '</div>';
		html += '<div class="hi-menu-tree-label">' + menuJson.name + '</div>';

		if (flag) {
			html += '</div>';
		} else {
			html += '<div class="hi-menu-tree-right-ico">';
			html += '<i id="hi-menu-tree-right-ico-' + p + '" class="fa fa-angle-left" aria-hidden="true"></i>';
			html += '</div>';
			html += '</div>';

			html += '<div class="hi-menu-tree-children" id="hi-menu-tree-children-' + p + '">';
			html += this.doHTree(p, l + 1, menuJson.children);
			html += '</div>';
		}

		html += '</div>';
		return html;
	}

	boldStyle(l) {
		if (l > 0) {
			return "";
		}
		return "font-weight: bold;";
	}
}

let menuTree = new MenuTree();

window.triggerReOpenMenu = function(i) {
	menuTree.triggerReOpenMenu(i);
}

window.triggerOpenMenu = function(i) {
	menuTree.triggerOpenMenu(i);
}

window.triggerChildrens = function(i) {
	menuTree.triggerChildrens(i);
}

export default menuTree;