import cutils from '../../js/common/core-utils.js';

$(function() {
	initData();
});

self.initData = function() {
	var vars = cutils.getUrlVars();
	var roleId = vars["roleId"];

	$.ajax({
		type: "get",
		url: "/" + cutils.webApp + "/role/" + roleId + "?_time=" + cutils.now(),
		data: $('#q-data-form').serialize(),
		dataType: 'json',
		success: function(result) {
			if (cutils.notEquals(result.status, 200)) {
				layer.msg(result.message, { icon: 7 });
				return;
			}

			$("#q-id").val(result.data.roleId);
			$("#q-name").val(result.data.name);
			$("#q-code").val(result.data.code);
			$("#q-order").val(result.data.order);
			$("#q-desc").val(result.data.desc);
		}
	});
}

self.doSubmitHandler = function(layerFromWin, pageFromWindow) {
	var roleName = $("#q-name").val();
	var roleCode = $("#q-code").val();
	var order = $("#q-order").val();

	if (cutils.isEmpty(roleName)) {
		layer.tips('请输入角色名称', '#q-name', {
			tips: 1
		});
		return;
	}

	if (cutils.isEmpty(roleCode)) {
		layer.tips('请输入角色编号', '#q-code', {
			tips: 1
		});
		return;
	}

	if (cutils.isNotEmpty(order) && cutils.isNotPositiveInteger(order)) {
		layer.tips('序号只能是正整数', '#q-order', {
			tips: 1
		});
		return;
	}

	$.ajax({
		type: "put",
		url: "/" + cutils.webApp + "/role?_time=" + cutils.now(),
		data: $('#q-data-form').serialize(),
		dataType: 'json',
		success: function(result) {
			if (cutils.equals(result.status, 200)) {
				layerFromWin.layer.msg(result.message, { icon: 1, time: 1000 });
				var indexFrame = layerFromWin.layer.getFrameIndex(window.name);
				layerFromWin.layer.close(indexFrame);
				pageFromWindow.doSearch();
				return;
			}
			layerFromWin.layer.msg(result.message, { icon: 7 });
		}
	});
}