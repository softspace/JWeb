import cutils from '../../js/common/core-utils.js';
import '../../js/common/table-col-utils.js';

$(function() {
	initTable();

	$(document).keydown(function() {
		if (event.keyCode == 13) {
			doSearch();
		}
	});

	cutils.doAuthSign();
});

self.initTable = function() {
	$('#data-table').datagrid({
		url: "/" + cutils.webApp + "/role/list?_time=" + cutils.now(),
		method: 'get',
		animate: true,
		fit: true,
		fitColumns: false,
		pagination: false,// 分页控件
		rownumbers: false,// 行号
		loadMsg: false,
		resizeHandle: 'right',
		singleSelect: false,
		checkOnSelect: false,
		selectOnCheck: false,
		nowrap: false,
		emptyMsg: '无数据',
		onLoadSuccess: function(data) {
			layui.form.render();
			cutils.doAuthSign();
		},
		onLoadError: function() {
			$(".datagrid-body").html("<div style='width: 100%; height: 50px; line-height: 50px; text-align: center;font-size: 14px;'>数据加载出错</div>");
		},
		loadFilter: function(data, parentId) {
			if (cutils.notEquals(data.status, 200)) {
				layer.msg(data.message, { icon: 3 });
				return;
			}

			return {
				total: data.data.count,
				rows: data.data.dataList
			};
		},
		columns: [[{
			field: 'name',
			title: '角色名称',
			width: 160
		}, {
			field: 'code',
			title: '角色编号',
			width: 150
		}, {
			field: 'desc',
			title: '备注信息',
			width: 200
		}, {
			field: 'updatetime',
			title: '更新时间',
			width: 150,
			align: 'center',
			formatter: function(value, row, index) {
				return value.length > 16 ? value.substring(0, 16) : value;
			}
		}, {
			field: 'order',
			title: '排序号',
			align: 'center',
			width: 60
		}, {
			field: 'status',
			title: '是否禁用',
			align: 'center',
			width: 85,
			formatter: function(value, row, index) {
				return '<span id="role-status-span-' + index + '" class="layui-form"><input tipId="#role-status-span-' + index + '" type="checkbox" name="role-status" roleId="' + row.roleId + '" title="禁用" lay-filter="role-status" ' + (row.status == 1 ? 'checked' : '') + '></span>';
			}
		}, {
			field: 'opts',
			title: '操作',
			width: 250,
			align: 'center',
			formatter: function(value, row, index) {
				let html = '<div title="编辑角色" data-authcode="uums.role.edit" class="hi-small-btn hi-small-normal-btn hi-transition hi-auth-sign" onclick="doEditRole(' + row.roleId + ');">';
				html += '<i class="fa fa-pencil-square-o" aria-hidden="true"></i>';
				html += '</div>';

				html += '<div title="删除角色" data-authcode="uums.role.del" class="hi-small-btn hi-small-txt-red-btn hi-transition hi-auth-sign" onclick="doDelRole(' + row.roleId + ');">';
				html += '<i class="fa fa-trash-o" aria-hidden="true"></i>';
				html += '</div>';

				html += '<div title="配置功能权限" data-authcode="uums.role.cfg" class="hi-small-btn hi-small-normal-btn hi-transition hi-auth-sign" onclick="doCfgFunction(' + row.roleId + ', \'' + row.code + '\', \'' + row.name + '\');">';
				html += '<i class="fa fa-cog" aria-hidden="true"></i>';
				html += '<span>配置功能权限</span>';
				html += '</div>';
				return html;
			}
		}]]
	});

	layui.form.on('checkbox(role-status)', function(obj) {
		if (cutils.hasNotAuthCode("uums.role.edit")) {
			$(obj.elem).prop("checked", !obj.elem.checked);
			layui.form.render();
			layer.tips('没有操作权限', $(obj.elem).attr("tipId"), {
				tips: 1,
				time: 1000
			});
			return false;
		}

		doEidtRoleStatus(obj);
	});
}

self.doSearch = function() {
	var unlock = $("#q-status-unlock").is(":checked");
	var lock = $("#q-status-lock").is(":checked");

	var queryParams = {};
	queryParams["name"] = $("#q-role-name").val();
	queryParams["status"] = unlock && lock ? "" : !unlock && !lock ? "" : unlock ? 0 : 1;
	$('#data-table').datagrid('load', queryParams);
}

self.doEidtRoleStatus = function(obj) {
	var params = {
		"roleId": $(obj.elem).attr("roleId"),
		"status": obj.elem.checked ? 1 : 0,
	};

	$.ajax({
		type: "put",
		url: "/" + cutils.webApp + "/role/status?_time=" + cutils.now(),
		data: params,
		dataType: 'json',
		success: function(result) {
			if (cutils.equals(result.status, 200)) {
				layer.msg(result.message, { icon: 1, time: 1000 });
				return;
			}

			$(obj.elem).prop("checked", !obj.elem.checked);
			layui.form.render();
			layer.msg(result.message, { icon: 7 });
		},
		nerror: function(e) {
			$(obj.elem).prop("checked", !obj.elem.checked);
			layui.form.render();
		}
	});
}

self.doDelRole = function(roleId) {
	layer.confirm('确定删除吗?',
		{
			icon: 3,
			title: '温馨提示',
			btn: ['<i class="fa fa-check" style="color: inherit;"></i>确定', '<i class="fa fa-remove" style="color: inherit;"></i>取消'],
			yes: function(index) {
				layer.close(index);
				var layerLoadIndex = layer.load(1, { shade: 0.3 });
				$.ajax({
					type: "delete",
					url: "/" + cutils.webApp + "/role/" + roleId + "?_time=" + cutils.now(),
					dataType: 'json',
					success: function(result) {
						layer.close(layerLoadIndex);
						if (cutils.notEquals(result.status, 200)) {
							layer.msg(result.message, { icon: 7 });
							return;
						}
						layer.msg(result.message, { icon: 1, time: 1000 });
						doSearch();
					},
					error: function(e) {
						layer.close(layerLoadIndex);
						alert(e.status + ", " + e.responseText);
					}
				});
			},
			cancel: function(index) {
				layer.close(index);
			}
		});
}

self.doAddRole = function() {
	top.layer.open({
		type: 2,
		title: '新增角色',
		area: ['600px', '400px'],
		shade: 0.3,
		maxmin: true,
		content: "/" + cutils.webApp + "/views/role/add.html?&_time=" + cutils.now(),
		btn: ['<i class="fa fa-check" style="color: inherit;"></i>保存', '<i class="fa fa-remove" style="color: inherit;"></i>取消'],
		yes: function(index, layero) {
			var iframeWin = layero.find('iframe')[0];
			iframeWin.contentWindow.doSubmitHandler(top, window);
		},
		cancel: function(index) {
			top.layer.close(index);
		}
	});
}

self.doEditRole = function(roleId) {
	top.layer.open({
		type: 2,
		title: '编辑角色',
		area: ['600px', '400px'],
		shade: 0.3,
		maxmin: true,
		content: "/" + cutils.webApp + "/views/role/edit.html?roleId=" + roleId + "&_time=" + cutils.now(),
		btn: ['<i class="fa fa-check" style="color: inherit;"></i>保存', '<i class="fa fa-remove" style="color: inherit;"></i>取消'],
		yes: function(index, layero) {
			var iframeWin = layero.find('iframe')[0];
			iframeWin.contentWindow.doSubmitHandler(top, window);
		},
		cancel: function(index) {
			top.layer.close(index);
		}
	});
}

self.doCfgFunction = function(roleId, roleCode, roleName) {
	top.layer.open({
		type: 2,
		title: '配置[' + roleName + ':' + roleCode + ']功能权限',
		area: ['800px', ($(top.document.body).height() - 100) + 'px'],
		shade: 0.3,
		maxmin: true,
		content: "/" + cutils.webApp + "/views/role/role-function.html?roleId=" + roleId + "&_time=" + cutils.now(),
		btn: ['<i class="fa fa-check" style="color: inherit;"></i>保存', '<i class="fa fa-remove" style="color: inherit;"></i>取消'],
		yes: function(index, layero) {
			var iframeWin = layero.find('iframe')[0];
			iframeWin.contentWindow.doSubmitHandler(top, window);
		},
		cancel: function(index) {
			top.layer.close(index);
		}
	});
}