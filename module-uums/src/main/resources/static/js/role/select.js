import cutils from '../../js/common/core-utils.js';

$(function() {
	$("#q-role-name").focus();

	var vars = cutils.getUrlVars();
	self.singleSelect = (cutils.getVars(vars, "singleSelect") == "true" ? true : false);

	initTable();
	$(document).keydown(function() {
		if (event.keyCode == 13) {
			doSearch();
		}
	});
});

self.initTable = function() {
	$('#data-table').datagrid({
		url: "/" + cutils.webApp + "/role/list?_time=" + cutils.now(),
		method: 'get',
		idField: "roleId",
		fit: true,
		fitColumns: true,
		pagination: false,// 分页控件
		rownumbers: false,// 行号
		loadMsg: false,
		emptyMsg: '无数据',
		singleSelect: self.singleSelect,
		onLoadSuccess: function(data) {
			if (self.singleSelect) {
				$(".datagrid-header-check input").hide();
				$(".datagrid-header-check label").hide();
			}

			var vars = cutils.getUrlVars();
			var roleId = cutils.getVars(vars, "roleId");
			if (cutils.isEmpty(roleId)) {
				return;
			}

			$.each(roleId.split(","), function(i, node) {
				$('#data-table').datagrid("checkRow", $('#data-table').datagrid("getRowIndex", node));
			});
		},
		onLoadError: function() {
			$(".datagrid-body").html("<div style='width: 100%; height: 50px; line-height: 50px; text-align: center;font-size: 14px;'>数据加载出错</div>");
		},
		loadFilter: function(data, parentId) {
			if (cutils.notEquals(data.status, 200)) {
				layer.msg(data.message, { icon: 3 });
				return;
			}

			return {
				total: data.data.count,
				rows: data.data.dataList
			};
		},
		queryParams: {
			"status": 0
		},
		columns: [[{
			field: 'ck',
			checkbox: true
		}, {
			field: 'roleId',
			hidden: true
		}, {
			field: 'name',
			title: '角色名称',
			width: 350
		}]]
	});
}

self.doSearch = function() {
	var queryParams = {};
	queryParams["name"] = $("#q-role-name").val();
	queryParams["status"] = 0;
	$('#data-table').datagrid('load', queryParams);
}

self.getChecked = function() {
	var rows = $('#data-table').datagrid('getChecked');
	if (self.singleSelect) {
		return rows == null ? null : rows[0];
	}
	return rows;
}
