import cutils from '../../js/common/core-utils.js';

self.doSubmitHandler = function(layerFromWin, pageFromWindow) {
	var roleName = $("#q-name").val();
	var roleCode = $("#q-code").val();
	var order = $("#q-order").val();

	if (cutils.isEmpty(roleName)) {
		layer.tips('请输入角色名称', '#q-name', {
			tips: 1
		});
		return;
	}

	if (cutils.isEmpty(roleCode)) {
		layer.tips('请输入角色编号', '#q-code', {
			tips: 1
		});
		return;
	}

	if (cutils.isNotEmpty(order) &&cutils.isNotPositiveInteger(order)) {
		layer.tips('序号只能是正整数', '#q-order', {
			tips: 1
		});
		return;
	}

	$.ajax({
		type: "POST",
		url: "/" + cutils.webApp + "/role?_time=" + cutils.now(),
		data: $('#q-data-form').serialize(),
		dataType: 'json',
		success: function(result) {
			 if (cutils.equals(result.status, 200)) {
				layerFromWin.layer.msg(result.message, { icon: 1, time: 1000 });
				var indexFrame = layerFromWin.layer.getFrameIndex(window.name);
				layerFromWin.layer.close(indexFrame);
				pageFromWindow.doSearch();
				return;
			}
			layerFromWin.layer.msg(result.message, { icon: 7 });
		}
	});
}