import cutils from '../../js/common/core-utils.js';

$(function() {//初始化
	var vars = cutils.getUrlVars();
	var userId = vars["userId"];
	$("#q-user-id").val(userId);
});

self.doSubmitHandler = function(layerFromWin, pageFromWindow) {
	var oldPassword = $("#q-oldPassword").val();
	var newPassword = $("#q-newPassword").val();
	var cNewPassword = $("#q-c-newPassword").val();

	if (cutils.isEmpty(oldPassword)) {
		layer.tips('请输入原密码', '#q-oldPassword', {
			tips: 1,
			time: 2000
		});
		return;
	}

	if (cutils.isEmpty(newPassword)) {
		layer.tips('请输入新密码', '#q-newPassword', {
			tips: 1,
			time: 2000
		});
		return;
	}

	if (cutils.isEmpty(cNewPassword)) {
		layer.tips('请再次输入新密码', '#q-c-newPassword', {
			tips: 1,
			time: 2000
		});
		return;
	}

	if (cutils.notEquals(newPassword, cNewPassword)) {
		layer.tips('两次输入的新密码不一致', '#q-c-newPassword', {
			tips: 1,
			time: 1000
		});
		return;
	}

	$.ajax({
		type: "put",
		url: "/" + cutils.webApp + "/user/modify/password?_time=" + cutils.now(),
		data: $('#q-data-form').serialize(),
		dataType: 'json',
		success: function(result) {
			 if (cutils.equals(result.status, 200)) {
				layerFromWin.layer.msg(result.message, { icon: 1, time: 1000 });
				var indexFrame = layerFromWin.layer.getFrameIndex(window.name);
				layerFromWin.layer.close(indexFrame);
				return;
			}
			layerFromWin.layer.msg(result.message, { icon: 7 });
		}
	});
}