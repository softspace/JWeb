import cutils from '../../js/common/core-utils.js';

/** 新增用户提交处理方法，参数检测，ajax提交表单 */
self.doSubmitHandler = function(layerFromWin, pageFromWindow) {
	var username = $("#q-username").val();

	if (cutils.isEmpty(username)) {
		layer.tips('请输入登录名', '#q-username', {
			tips: 1
		});
		return;
	}

	if ($("#q-org-ids input").length == 0) {
		layer.tips('请输选择部门', '#q-org-name', {
			tips: 1
		});
		return;
	}

	if ($("#q-role-ids input").length == 0) {
		layer.tips('请输选择角色', '#q-role-name', {
			tips: 1
		});
		return;
	}

	$.ajax({
		type: "post",
		url: "/" + cutils.webApp + "/user?_time=" + cutils.now(),
		data: $('#q-data-form').serialize(),
		dataType: 'json',
		success: function(result) {
			 if (cutils.equals(result.status, 200)) {//新增成功，提示成功信息，获取用户列表Iframe，关闭新增用户界面，刷新用户列表
				layer.msg(result.message, { icon: 1, time: 1000 });
				var indexFrame = layerFromWin.layer.getFrameIndex(window.name);
				layerFromWin.layer.close(indexFrame);
				pageFromWindow.doSearch();
				return;
			}
			//新增识别，提示错误信息
			layer.msg(result.message, { icon: 7 });
		}
	});
}

/**选择部门，可多选 */
self.doSelectOrg = function() {
	var orgIds = "";
	$("#q-org-ids input").each(function(i, item) {
		if (i > 0) {
			orgIds += ",";
		}
		orgIds += $(this).val();
	});

	top.layer.open({
		type: 2,
		title: '选择部门',
		area: ['320px', '450px'],
		shade: 0.01,
		maxmin: true,
		shadeClose: true,
		content: "/" + cutils.webApp + "/views/org/select.html?singleSelect=false&orgId=" + orgIds + "&_time=" + cutils.now(),
		btn: ['<i class="fa fa-check" style="color: inherit;"></i>确定', '<i class="fa fa-eraser" style="color: inherit;"></i>清除', '<i class="fa fa-remove" style="color: inherit;"></i>取消'],
		yes: function(index, layero) {
			var iframeWin = layero.find('iframe')[0];
			var data = iframeWin.contentWindow.getChecked();

			if (cutils.isEmpty(data)) {
				iframeWin.contentWindow.layer.msg("请选择部门", { icon: 7 });
				return;
			}

			$("#q-org-ids").empty();
			var orgNames = "";
			$.each(data, function(i, item) {
				$("#q-org-ids").append('<input type="hidden" name="orgId" value="' + item.orgId + '">');
				if (i > 0) {
					orgNames += "，";
				}
				orgNames += item.name;
			});
			$("#q-org-name").val(orgNames);

			top.layer.close(index);
		},
		btn2: function(index, layero) {
			$("#q-org-ids").empty();
			$("#q-org-name").val("");
			top.layer.close(index);

		}, btn3: function(index, layero) {
			top.layer.close(index);
		},
		cancel: function(index) {
			top.layer.close(index);
		}
	});
}

/** 选择角色，可多选 */
self.doSelectRole = function() {
	var roleIds = "";
	$("#q-role-ids input").each(function(i, item) {
		if (i > 0) {
			roleIds += ",";
		}
		roleIds += $(this).val();
	});

	top.layer.open({
		type: 2,
		title: '选择部门',
		area: ['320px', '450px'],
		shade: 0.01,
		maxmin: true,
		shadeClose: true,
		content: "/" + cutils.webApp + "/views/role/select.html?singleSelect=false&roleId=" + roleIds + "&_time=" + cutils.now(),
		btn: ['<i class="fa fa-check" style="color: inherit;"></i>确定', '<i class="fa fa-eraser" style="color: inherit;"></i>清除', '<i class="fa fa-remove" style="color: inherit;"></i>取消'],
		yes: function(index, layero) {
			var iframeWin = layero.find('iframe')[0];
			var data = iframeWin.contentWindow.getChecked();

			if (cutils.isEmpty(data)) {
				iframeWin.contentWindow.layer.msg("请选择部门", { icon: 7 });
				return;
			}

			$("#q-role-ids").empty();
			var roleNames = "";
			$.each(data, function(i, item) {
				$("#q-role-ids").append('<input type="hidden" name="roleId" value="' + item.roleId + '">');
				if (i > 0) {
					roleNames += "，";
				}
				roleNames += item.name;
			});
			$("#q-role-name").val(roleNames);

			top.layer.close(index);
		},
		btn2: function(index, layero) {
			$("#q-role-ids").empty();
			$("#q-role-name").val("");
			top.layer.close(index);

		}, btn3: function(index, layero) {
			top.layer.close(index);
		},
		cancel: function(index) {
			top.layer.close(index);
		}
	});
}