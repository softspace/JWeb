import cutils from '../../js/common/core-utils.js';

self.initUserData = function(data) {
	var username = data.user.username;
	var fullname = data.user.fullname;

	if (cutils.isNotEmpty(fullname)) {
		username += "（" + fullname + "）";
	}
	$("#user-name").html(username);

	$("#mobile-phone").html(data.user.mobilenumber);
	$("#office-phone").html(data.user.officephone);
	$("#email").html(data.user.email);

	var orgs = "";
	$.each(data.orgList, function(i, item) {
		if (i > 0) {
			orgs += "，";
		}
		orgs += item.name;
	});
	$("#org-names").html(orgs);

	var roles = "";
	$.each(data.roleList, function(i, item) {
		if (i > 0) {
			roles += "，";
		}
		roles += item.name;
	});
	$("#role-names").html(roles);
}