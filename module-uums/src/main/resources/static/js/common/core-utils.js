/** 工具类 */
class CoreUtils {
	constructor() {
		//项目名
		this.webApp = window.document.location.pathname.split('/')[1];
		let _this = this;
		//设置全局ajax
		$.ajaxSetup({
			beforeSend: function(xhr) {//使用时不应该覆盖此方法，若覆盖要处理token
				xhr.setRequestHeader('Hm-Token', _this.getToken());
				this.layerLoadIndex = layer.load(1, { shade: 0.3 });
			},
			cache: false,
			complete: function(xhr, status) {//使用时不应该覆盖此方法，若覆盖要处理token
				layer.close(this.layerLoadIndex);
			},
			nerror: function(xhr, status, error) {//
			},
			error: function(xhr, status, error) {//一般不用覆盖此方法，若需要异常后做些处理，则覆盖nerror方法即可
				try { this.nerror(xhr, status, error); } catch (e) { }
				if (this.dataType == null) {
					layer.msg("程序异常，status:" + status + ", error" + error, { icon: 7 });
					return;
				}
				if (this.dataType.toLowerCase() != "json") {
					layer.msg("程序异常，status:" + status + ", error" + error, { icon: 7 });
					return;
				}
				if (xhr.responseJSON.status == 401) {
					_this.delToken();
					var msg = "回话已过期，将跳转到登录页面";
					var callback = function(index) {
						layer.close(index);
						top.location.href = "/" + _this.webApp + "/views/login.html";
					}
					layer.alert(msg, { icon: 7, title: '温馨提示', yes: callback, cancel: callback });
					return;
				}
				if (xhr.responseJSON.status == 403) {
					layer.msg("没有操作权限", { icon: 7 });
					return;
				}
				if (xhr.responseJSON.status == 500) {
					layer.msg("程序异常", { icon: 7 });
					return;
				}
				layer.msg("程序异常，业务状态码：" + xhr.responseJSON.status + ", 异常信息：" + xhr.responseJSON.message, { icon: 7 });
			}
		});
	}

	//存储Token
	addToken(token) {
		top.localStorage.setItem("HM-TOKEN", token);
	}

	//获取token
	getToken() {
		var token = top.localStorage.getItem("HM-TOKEN");
		return this.isEmpty(token) ? "" : token;
	}

	//删除token
	delToken() {
		top.localStorage.removeItem("HM-TOKEN");
	}

	//存储系统数据
	setSysData(data) {
		top.localStorage.setItem("H-SYS-DATA", JSON.stringify(data));
	}

	//获取系统数据
	getSysData() {
		return JSON.parse(top.localStorage.getItem("H-SYS-DATA"));
	}

	//删除系统数据
	delSysData() {
		top.localStorage.removeItem("H-SYS-DATA");
	}

	//参数用户数据
	setUserData(data) {
		top.localStorage.setItem("H-USER-DATA", JSON.stringify(data));
	}

	//获取用户数据
	getUserData() {
		return JSON.parse(top.localStorage.getItem("H-USER-DATA"));
	}

	//删除用户数据
	delUserData() {
		top.localStorage.removeItem("H-USER-DATA");
	}

	//判断值是否为空，为空返回true
	isEmpty(v) {
		return v== undefined || v == null || v == "";
	}

	//判断值是否不为空，不为空返回true
	isNotEmpty(v) {
		return !this.isEmpty(v);
	}

	//判断两个值是否相等，等返回true
	equals(v1, v2) {
		return v1 == v2;
	}

	//判断两个值是否不相等，不相等返回true
	notEquals(v1, v2) {
		return !this.equals(v1, v2);
	}

	//判断值是否是正整数，是返回true
	isPositiveInteger(v) {
		return /(^[1-9]\d*$)/.test(v);
	}

	//判断值是否是正整数，不是返回true
	isNotPositiveInteger(v) {
		return !this.isPositiveInteger(v);
	}

	//获取url中的参数
	getUrlVars() {
		let vars = [], hash, path = window.location.href;

		if (path.indexOf("?") == -1) {
			return vars;
		}

		let hashes = path.slice(path.indexOf('?') + 1).split('&');
		for (let i = 0; i < hashes.length; i++) {
			hash = hashes[i].split('=');
			vars.push(hash[0]);
			vars[hash[0]] = hash[1];
		}
		return vars;
	}

	//根据参数名获取url中的参数值
	getVars(vars, name) {
		if (vars[name] == undefined) {
			return "";
		}
		return vars[name];
	}

	//当前时间
	now() {
		return new Date().getTime();
	}


	//调用各个浏览器提供的全屏方法
	doFullScreen() {
		let docElm = document.documentElement;
		if (docElm.requestFullscreen) {
			docElm.requestFullscreen();
		} else if (docElm.mozRequestFullScreen) {
			docElm.mozRequestFullScreen();
		} else if (docElm.webkitRequestFullScreen) {
			docElm.webkitRequestFullScreen();
		} else if (docElm.msRequestFullscreen) {
			docElm.msRequestFullscreen();
		} else {
			layer.msg("当前浏览器不支持全屏！", { icon: 3 });
		}
	};

	//调用各个浏览器提供的退出全屏方法
	doExitFullscreen() {
		let docElm = document.documentElement;
		//W3C 
		if (docElm.requestFullscreen) {
			docElm.requestFullscreen();
		}
		//FireFox 
		else if (docElm.mozRequestFullScreen) {
			docElm.mozRequestFullScreen();
		}
		//Chrome等 
		else if (docElm.webkitRequestFullScreen) {
			docElm.webkitRequestFullScreen();
		}
		//IE11 
		else if (docElm.msRequestFullscreen) {
			docElm.msRequestFullscreen();
		}
		if (document.exitFullscreen) {
			document.exitFullscreen();
		}
		else if (document.mozCancelFullScreen) {
			document.mozCancelFullScreen();
		}
		else if (document.webkitCancelFullScreen) {
			document.webkitCancelFullScreen();
		}
		else if (document.msExitFullscreen) {
			document.msExitFullscreen();
		}
	}

	//处理按钮权限，如果被标记为需要处理权限的按钮，有权限显示按钮，没有权限隐藏按钮
	doAuthSign() {
		let _this = this;
		$(".hi-auth-sign").each(function() {
			var that = this;
			var authCode = $(this).data("authcode");
			if (_this.hasNotAuthCode(authCode)) {
				$(this).addClass("hi-not-auth");
				$(this).hide();//可以不用隐藏，不用隐藏的话，按钮是灰色的，用户点了会提示没有操作权限，这里隐藏按钮
			}
		});

		$(".hi-not-auth").removeAttr("onclick");

		$(".hi-not-auth").click(function() {
			layer.tips('没有操作权限', this, {
				tips: 1,
				time: 1000
			});
		});
	}

	//是否有指定的权限code，没有返回true
	hasNotAuthCode(authCode) {
		return !this.hasAuthCode(authCode);
	}

	//是否有指定的权限code，有返回true
	hasAuthCode(authCode) {
		let userData = null;
		try {
			userData = this.getUserData();
		} catch (e) { };

		if (userData == null) {
			return false;
		}

		let hasAuthCode = false;
		$.each(userData.functionList, function(i, item) {
			if (item.type == 1) {
				if (authCode == item.code) {
					hasAuthCode = true;
					return false;
				}
			}
		});
		return hasAuthCode;
	}
}

export default new CoreUtils();