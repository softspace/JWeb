/** 列表显示列设置工具类 */
self.doOpenColsCfg = function(tableId, isTreegrid) {
	if ($("#hi-col-cfg-container").length > 0) {
		$("#hi-col-cfg-container").remove();
	}

	$("body").append('<div id="hi-col-cfg-container" class="hi-col-cfg-container layui-form" lay-filter="hi-col-cfg-form"></div>');

	layer.open({
		title: "显示隐藏列",
		type: 1,
		shade: 0.01,
		shadeClose: true,
		area: ['510px', '300px'],
		content: $('#hi-col-cfg-container'),
		btn: ['<i class="fa fa-check" style="color: inherit;"></i>确定', '<i class="fa fa-remove" style="color: inherit;"></i>取消'],
		yes: function(index, layero) {
			layer.close(index);
			self.showOrHideCol(tableId, isTreegrid);
		},
		success: function(layero, index) {
			var cols = self.getCols(tableId, isTreegrid);
			var colsHtml = '';
			$.each(cols, function(i1, item1) {
				$.each(item1, function(i2, item2) {
					colsHtml += '<div class="col-cfg-item-warp">';
					colsHtml += '<input type="checkbox" class="hi-col-cfg-ck" name="' + item2.field + '" ' + (item2.hidden ? '' : 'checked="checked""') + ' data-field="' + item2.field + '" title="' + (item2.title || item2.field) + '">';
					colsHtml += '</div>';
				});
			});
			layero.find("#hi-col-cfg-container").html("");
			layero.find("#hi-col-cfg-container").html(colsHtml);
			layui.form.render();
		}
	});
}

self.showOrHideCol = function(tableId, isTreegrid) {
	if ($(".hi-col-cfg-ck").length == 0) {
		return;
	}
	$(".hi-col-cfg-ck").each(function(i, item) {
		if (isTreegrid) {
			$(tableId).treegrid((item.checked ? 'showColumn' : 'hideColumn'), $(item).data('field'));
		} else {
			$(tableId).datagrid((item.checked ? 'showColumn' : 'hideColumn'), $(item).data('field'));
		}
	});
}

self.getCols = function(tableId, isTreegrid) {
	var options = isTreegrid ? $(tableId).treegrid('getOptions') : $(tableId).datagrid('getOptions');
	return options.columns;
}