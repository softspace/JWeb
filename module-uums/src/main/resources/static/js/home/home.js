import cutils from '../common/core-utils.js';

$(function() {
	var text = "欢迎您使用";
	for (var i = 0; i < text.length; i++) {
		$("#wellcome").append("<span class='hi-transition' id='wellcome-c-" + i + "'>" + text.charAt(i) + "</span>");
	}
	doWelcomeAnimation(text, 0);
});

window.doWelcomeAnimation = function(text, i) {
	if (i == text.length) {
		return;
	}
	$("#wellcome-c-" + i).css("color", "rgba(2, 121, 181, 1)");
	$("#wellcome-c-" + i).css("text-shadow", "0 1px 0 rgba(238, 238, 238, 1), 0 2px 0 rgba(238, 238, 238, 1), 0 3px 0 rgba(238, 238, 238, 1), 0 4px 0 rgba(238, 238, 238, 1), 0 0 5px rgba(238, 238, 238, 1), 0 1px 3px rgba(238, 238, 238, 1), 0 3px 5px rgba(238, 238, 238, 1)");
	setTimeout(function() {
		doWelcomeAnimation(text, ++i);
	}, 150);
}