import cutils from '../../js/common/core-utils.js';

$(function() {
	initData();
});

self.initData = function() {
	var vars = cutils.getUrlVars();
	var parmId = vars["parmId"];

	$.ajax({
		type: "get",
		url: "/" + cutils.webApp + "/parm/" + parmId + "?_time=" + cutils.now(),
		dataType: 'json',
		success: function(result) {
			if (cutils.notEquals(result.status, 200)) {
				layer.msg(result.message, { icon: 7 });
				return;
			}

			$("#q-parm-id").val(result.data.parmId);
			$("#q-parm-type").val(result.data.parmType);
			$("#q-parm-key").val(result.data.parmKey);
			$("#q-parm-value").val(result.data.parmValue);
			$("#q-parm-desc").val(result.data.parmDesc);

			var selected = "";
			$.each(result.data.itemList, function(i, item) {
				selected = "";
				if (item.dictItemCode == result.data.parmType) {
					selected = "selected";
				}
				$("#q-parm-type").append("<option value='" + item.dictItemCode + "' " + selected + ">" + item.dictItemName + "</option>");
			});

			layui.form.render();
		}
	});
}

self.doSubmitHandler = function(layerFromWin, pageFromWindow) {
	var parmType = $("#q-parm-type").val();
	var parmKey = $("#q-parm-key").val();
	var parmValue = $("#q-parm-value").val();

	if (cutils.isEmpty(parmType)) {
		layer.tips('请选择参数类型', '#q-parm-type', {
			tips: 1
		});
		return;
	}

	if (cutils.isEmpty(parmKey)) {
		layer.tips('请输入参数key', '#q-parm-key', {
			tips: 1
		});
		return;
	}

	if (cutils.isEmpty(parmValue)) {
		layer.tips('请输入参数值', '#q-parm-value', {
			tips: 1
		});
		return;
	}

	$.ajax({
		type: "put",
		url: "/" + cutils.webApp + "/parm?_time=" + cutils.now(),
		data: $('#q-data-form').serialize(),
		dataType: 'json',
		success: function(result) {
			if (cutils.equals(result.status, 200)) {
				layerFromWin.layer.msg(result.message, { icon: 1, time: 1000 });
				var indexFrame = layerFromWin.layer.getFrameIndex(window.name);
				layerFromWin.layer.close(indexFrame);
				pageFromWindow.doSearch();
				return;
			}
			layerFromWin.layer.msg(result.message, { icon: 7 });
		}
	});
}