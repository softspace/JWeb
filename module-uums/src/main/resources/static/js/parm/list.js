import cutils from '../../js/common/core-utils.js';
import '../../js/common/table-col-utils.js';

$(function() {
	initTable();

	$(document).keydown(function() {
		if (event.keyCode == 13) {
			doSearch();
		}
	});

	cutils.doAuthSign();
});

self.initTable = function() {
	$('#data-table').datagrid({
		url: "/" + cutils.webApp + "/parm/list?_time=" + cutils.now(),
		method: 'get',
		animate: true,
		fit: true,
		fitColumns: false,
		rownumbers: false,// 行号
		loadMsg: false,
		resizeHandle: 'right',
		singleSelect: false,
		checkOnSelect: false,
		selectOnCheck: false,
		nowrap: false,
		emptyMsg: '无数据',
		pagination: true,
		pageSize: 50,
		pageList: [20, 50, 100, 200, 500],
		onLoadSuccess: function(data) {
			layui.form.render();
			cutils.doAuthSign();
		},
		onLoadError: function() {
			$(".datagrid-body").html("<div style='width: 100%; height: 50px; line-height: 50px; text-align: center;font-size: 14px;'>数据加载出错</div>");
		},
		loadFilter: function(data, parentId) {
			if (cutils.notEquals(data.status, 200)) {
				layer.msg(data.message, { icon: 3 });
				return;
			}
			return {
				total: data.data.count,
				rows: data.data.dataList
			};
		},
		columns: [[{
			field: 'parmKey',
			title: '参数Key',
			width: 250
		}, {
			field: 'parmTypeCn',
			title: '参数类型',
			width: 100,
			align: 'center'
		}, {
			field: 'parmValue',
			title: '参数值',
			width: 250
		}, {
			field: 'parmDesc',
			title: '参数说明',
			width: 400
		}, {
			field: 'opts',
			title: '操作',
			width: 100,
			align: 'center',
			formatter: function(value, row, index) {
				let html = '<div title="编辑参数" data-authcode="uums.parm.edit" class="hi-small-btn hi-small-normal-btn hi-transition hi-auth-sign" onclick="doEditParm(' + row.parmId + ');">';
				html += '<i class="fa fa-pencil-square-o" aria-hidden="true"></i>';
				html += '</div>';

				html += '<div title="删除参数" data-authcode="uums.parm.del" class="hi-small-btn hi-small-txt-red-btn hi-transition hi-auth-sign" onclick="doDelParm(' + row.parmId + ');">';
				html += '<i class="fa fa-trash-o" aria-hidden="true"></i>';
				html += '</div>';
				return html;
			}
		}]]
	});
}

self.doSearch = function() {
	var unlock = $("#q-status-unlock").is(":checked");
	var lock = $("#q-status-lock").is(":checked");

	var queryParams = {};
	queryParams["name"] = $("#q-function-name").val();
	queryParams["status"] = unlock && lock ? "" : !unlock && !lock ? "" : unlock ? 0 : 1;
	$('#data-table').datagrid('load', queryParams);
}

self.doDelParm = function(parmId) {
	layer.confirm('确定删除吗?',
		{
			icon: 3,
			title: '温馨提示',
			btn: ['<i class="fa fa-check" style="color: inherit;"></i>确定', '<i class="fa fa-remove" style="color: inherit;"></i>取消'],
			yes: function(index) {
				layer.close(index);
				$.ajax({
					type: "delete",
					url: "/" + cutils.webApp + "/parm/" + parmId + "?_time=" + cutils.now(),
					dataType: 'json',
					success: function(result) {
						if (cutils.notEquals(result.status, 200)) {
							layer.msg(result.message, { icon: 7 });
							return;
						}
						layer.msg(result.message, { icon: 1, time: 1000 });
						doSearch();
					}
				});
			},
			cancel: function(index) {
				layer.close(index);
			}
		});
}

self.doAddParm = function() {
	top.layer.open({
		type: 2,
		title: '新增参数',
		area: ['600px', '400px'],
		shade: 0.3,
		maxmin: true,
		content: "/" + cutils.webApp + "/views/parm/add.html?&_time=" + cutils.now(),
		btn: ['<i class="fa fa-check" style="color: inherit;"></i>保存', '<i class="fa fa-remove" style="color: inherit;"></i>取消'],
		yes: function(index, layero) {
			var iframeWin = layero.find('iframe')[0];
			iframeWin.contentWindow.doSubmitHandler(top, window);
		},
		cancel: function(index) {
			top.layer.close(index);
		}
	});
}

self.doEditParm = function(parmId) {
	top.layer.open({
		type: 2,
		title: '编辑参数',
		area: ['600px', '400px'],
		shade: 0.3,
		maxmin: true,
		content: "/" + cutils.webApp + "/views/parm/edit.html?parmId=" + parmId + "&_time=" + cutils.now(),
		btn: ['<i class="fa fa-check" style="color: inherit;"></i>保存', '<i class="fa fa-remove" style="color: inherit;"></i>取消'],
		yes: function(index, layero) {
			var iframeWin = layero.find('iframe')[0];
			iframeWin.contentWindow.doSubmitHandler(top, window);
		},
		cancel: function(index) {
			top.layer.close(index);
		}
	});
}